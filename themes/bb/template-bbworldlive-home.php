<?php
/*
Template Name: BbWorld Live Home Template
*/
class BB_Bbworldlive_Home_Template {

    public $current_live_session;

    public function __construct() 
    {

        $this->current_live_session = get_field('current_live_session', 'option'); 

        $this->hooks();
        $this->content();
    }

    public function hooks()
    {
        add_action( 'wp_footer', array($this, 'footer_scripts') );
        add_action( 'wp_head', array($this, 'header_scripts') );
    }

    public function header_scripts()
    {
   
        ?>
        <script>
            var currenturl = window.location.pathname,
                ajaxdata = { redirect_to : currenturl },
                siteurl = "<?php echo site_url('/'); ?>";
        </script>

        <?php
    }

    public function footer_scripts()
    {
        ?>
        <script>

            var options = {
                ajaxdata : ajaxdata,
            }

            bbApp.auth.gate(options, function () {
                bbApp.auth.init();
                bbApp.auth.poll("<?php echo $this->current_live_session; ?>");
            });
           
        </script>
        <?php 
    }

    public function content()
    {
    	global $post, $bb_theme;

        $post_slug = $post->post_name;
        $post_ancestry = get_post_ancestors($post->ID);
        $poster = '';

        if (!empty($post_ancestry)) {
            $poster = get_post($post_ancestry[0]);
            $post_slug = $poster->post_name;
        }
        else {
            $poster = $post;
        }

        $bb_theme->header_html($post_slug);
        $bb_theme->top_header();
        $bb_theme->main_header($poster->post_name);
            	
    	if ( have_posts() ) {
    		while ( have_posts() ) {the_post(); ?>
                
                <?php $bb_theme->hero_content($post->ID); ?> 
                <?php echo $this->live_content(); ?>
                <?php $bb_theme->bottom_cta($post->ID); 
    		} 
    	} 

    	$bb_theme->footer_nav(); 
        $bb_theme->footer_scripts();
    }

    public function live_content ()
    {
        return '<section class="live-session-holder"></section>';
    }

}

$bb_bbworldlive_home_template = new BB_Bbworldlive_Home_Template;

?>