<?php
/*
Template Name: BbWorld Why Attend
*/

class BB_Attend_Template {

    private $fields;

    public function __construct() {

        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $this->visitor_check();
        $this->content();
    }

    private function visitor_check() {
        
        $cookiename = $_COOKIE["whyattend"];

        //check for reset flag
        if (isset($_REQUEST['reset']))  {
            return false;
        }  


    
        //check for url params
        if (isset($_REQUEST['role']) && isset($_REQUEST['market'])) {


            //set cookie
            setcookie( 'whyattend', $_REQUEST['role'].'|'.$_REQUEST['market'], time() + (20 * 365 * 24 * 60 * 60), '/' );

            $this->set_redirect($_REQUEST['role'].'|'.$_REQUEST['market']);

        }

        if (isset($cookiename) && !empty($cookiename)) {

            $this->set_redirect($cookiename);
            exit;

        }
       

    }


    public function content() {

    	global $post, $bb_theme;

        $post_slug = $post->post_name;
        $post_ancestry = get_post_ancestors($post->ID);
        $poster = '';

        if (!empty($post_ancestry)) {
            $poster = get_post($post_ancestry[0]);
            $post_slug = $poster->post_name;
        }
        else {
            $poster = $post;
        }

        $bb_theme->header_html($post_slug);
        $bb_theme->top_header();
        $bb_theme->main_header($poster->post_name);
            	
    	if ( have_posts() ) {
    		while ( have_posts() ) {the_post();
                
                $this->gate(); 
                    
    		} 
    	} 
        $bb_theme->footer_html();
    }

    public function gate()
    {
       global $bb_theme, $post;

        $background_image = $bb_theme->theme_dir.'images/whyattend-bg.jpg';
        $post_title = get_the_title($post->ID);
        
        ?>
        
        <section class="hero-section" style="background:url(<?php echo $background_image; ?>) #000 no-repeat center center; background-size:cover; height:100vh;">

            <div class="inner">
                <div class="row">
                    <div class="small-12 centered columns">
                        <h1 style="margin-top:50px;"><?php echo $post_title; ?></h1>
                        <p>BbWorld is a place where the innovators in education come together to exchange ideas, share best practices, and find solutions to challenges. Choose your role and industry below to find out how you can benefit from the BbWorld experience. </p>
                    </div>
                </div>

                <div class="row">  
                    <div class="small-12 medium-6 medium-centered centered columns"> 
                        <form class="attend" method="post" action="http://bbworld.com/bbworld/why-attend?rand=<?php echo rand(5, 3000); ?>"> 
                            <select name="role" required>
                                <option value="" disabled="disabled" selected>Choose a Role:</option>   
                                <option value="leadership">Leadership</option>
                                <option value="technology">Technology</option>
                                <option value="faculty">Faculty/Educator</option>
                                <option value="designer">Instructional Designer</option>
                                <option value="training">Training Professional</option>
                            </select>

                            <select name="market" required>
                                <option value="" disabled="disabled" selected>Choose a Market:</option>   
                                <option value="highered">Higher Education</option>
                                <option value="k12">K-12</option>
                                <option value="gov">Military/Government</option>
                                <option value="assc">Business/Association</option>
                            </select>

                         <input type="submit" value="Continue" class="button small attend-btn" />

                        </form>

                    </div>
                </div> 
               
            </div>
        
        </section> 

        <?php  
    }

    public function get_whyattend_fields () {

        $fields = array();

        if( have_rows('why_attend_settings', 'option') ) {

            $sinc = 0;
            // loop through the rows of data
            while ( have_rows('why_attend_settings', 'option') ) { 

                the_row();

                $fields[$sinc] = array(
                    'link' =>  get_sub_field('link'),
                    'roles' =>  get_sub_field('role'),
                    'markets' => get_sub_field('market')
                ); 

                $sinc++;        
            }
        }


        return $fields;

    }

    public function convert_to_slug($name) {

        $name = str_replace(' ', '_', strtolower($name));
        $name = str_replace('/', '_', $name);

        return $name;

    }

    private function set_redirect($cookie) {



        $fields = $this->get_whyattend_fields();

        $choices = explode("|", $cookie);
        $finals = array();

        foreach ($fields as $key => $link) {
            
            if (in_array($choices[0], $link['roles']) && in_array($choices[1], $link['markets'])) {

                header( 'Location: '.$link['link'] );
                exit;
            }
            
        }      

        wp_redirect( '/' );
        exit;  
       
    }

}

$bb_attend_template = new BB_Attend_Template;



?>