<?php 

//default timezone
date_default_timezone_set('America/Chicago');

// load the theme files
foreach ( glob( dirname( __FILE__ ) . '/inc/*.php' ) as $file ){
  require $file;
}

$bb_defaults = new BB_Defaults;
$bb_cpt = new BB_CPT;
$bb_taxonomies = new BB_Taxonomies;
$bb_lanyon = new BB_Lanyon;
$bb_shortcodes = new BB_Shortcodes;
$bb_theme = new BB_Theme;
$bb_auth = new BB_Auth;
$bb_chat = new BB_Chat;
$bb_sessions = new BB_Sessions;
$bb_pwreset = new BB_Pwreset;
$bb_login = new BB_Login;
$bb_email = new BB_Email;
$bb_vc = new BB_VC;

?>