<?php

/*
Template Name: Sessions Template
*/
class BB_Session_Template {

    public $theme_dir;
    public $footer_html;
    public $top_sections;

    public function __construct() {
        $this->hooks();
        $this->content();
    }

    public function hooks()
    {
        add_action( 'wp_footer', array($this, 'footer_scripts') );
        add_action( 'wp_head', array($this, 'header_scripts') );

    }

    public function header_scripts()
    {
        $nonce = wp_create_nonce( 'tnonce' );
        ?>
        <script>
            var tnonce = '<?php echo esc_attr($nonce); ?>'
                postholder = '.loading-holder',
                siteurl = "<?php echo site_url('/'); ?>",
                ajaxdata = { action : 'get_sessions', nonce : tnonce, posttype : 'session', pagenum : 1 }
        </script>

        <?php
    }

    public function footer_scripts()
    {
        ?>
        <script>
            var options = {
                ajaxdata : ajaxdata,
                holder : postholder,
                template : 'bbworld'
            }

            bbApp.ajax.init(options);
            
        </script>
        <?php 
    }

    public function content()
    {
        global $post, $bb_theme;

        $post_slug = $post->post_name;
        $post_ancestry = get_post_ancestors($post->ID);
        $poster = '';

        if (!empty($post_ancestry)) {
            $poster = get_post($post_ancestry[0]);
            $post_slug = $poster->posame;
        }
        else {
            $poster = $post;
        }

        $bb_theme->header_html($post_slug);
        $bb_theme->top_header();
        $bb_theme->main_header($poster->post_name);

        //terms
        $markets = get_terms ( 'markets', array ('order' => 'ASC') );
        $dates = get_terms ( 'dates' );
        $times = get_terms ( 'times' );
        $locations = get_terms ( 'locations' );
        $jobroles = get_terms ( 'jobroles' );
        $types = get_terms ( 'sessiontypes' );
        $bbworldtracks = get_terms ( 'bbworldtracks' );


        if ( have_posts() ) {
            while ( have_posts() ) {the_post(); ?>
                <?php $bb_theme->hero_content($post->ID); 

                ?>

                <section class="main-inner-content" style="padding-top:0px;">
                
                <?php 
            }
        }


        ?> 

            <div class="session-wrapper" style="margin-top:0px;">
                <div class="session-inner row">
                    <div class="medium-4 columns">

                        <div class="session-subtitle post-meta dark">
                            <p>Filter Sessions By</p>
                        </div>

                        <div class="session-filters">
                          
                            <form class="session-filter-form">
                                
                                <div class="session-filter-wrapper">
                                    <div class="session-filter-title">Audience
                                        <svg viewBox="0 0 100 100" class="icon shape-expand">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#shape-expand"></use>
                                        </svg>
                                    </div>
                                    <ul class="session-filter-options markets">
                                        <?php 

                                            foreach ($markets as $market) {

                                                $market->name = str_replace('&amp;', '&', $market->name);

                                                $checked = '';
                                                if (isset($_GET['market'])) {
                                                    if ($_GET['market'] == $market->name || (is_array($_GET['market']) && in_array($market->name, $_GET['market']))) {
                                                         $checked = 'checked';
                                                    }
                                                }

                                                echo '<li><div><input data-name="'.$market->name.'" value="'.$market->term_id.'" type="checkbox" '.$checked.' /><span>'.$market->name.'</span></div></li>';
                                            }
                                        ?>
                                    </ul>
                                </div>

                                <div class="session-filter-wrapper">
                                    <div class="session-filter-title">Date
                                        <svg viewBox="0 0 100 100" class="icon shape-expand">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#shape-expand"></use>
                                        </svg>
                                    </div>
                                    <ul class="session-filter-options dates">
                                        <?php 

                                            foreach ($dates as $date) {

                                                $checked = '';
                                                if (isset($_GET['date'])) {
                                                 
                                                    if ($_GET['date'] == $date->name || (is_array($_GET['date']) && in_array($date->name, $_GET['dates']))) {
                                                         $checked = 'checked';
                                                    }
                                                }
                                                
                                                echo '<li><div><input data-name="'.$date->name.'" value="'.$date->term_id.'" type="checkbox" '.$checked.' /><span>'.$date->name.'</span></div></li>';
                                            }

                                        ?>
                                    </ul>
                                </div>

                                <!--<div class="session-filter-wrapper">
                                    <div class="session-filter-title">Time
                                        <svg viewBox="0 0 100 100" class="icon shape-expand">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#shape-expand"></use>
                                        </svg>
                                    </div>
                                    <ul class="session-filter-options times">
                                        <?php 

                                            foreach ($times as $time) {

                                                $checked = '';
                                                if (isset($_GET['time'])) {
                                                 
                                                    if ($_GET['time'] == $time->name || (is_array($_GET['times']) && in_array($times->name, $_GET['dates']))) {
                                                         $checked = 'checked';
                                                    }
                                                }
                                                
                                                echo '<li><div><input data-name="'.$time->name.'" value="'.$time->term_id.'" type="checkbox" '.$checked.' /><span>'.$time->name.'</span></div></li>';
                                            }

                                        ?>
                                    </ul>
                                </div>

                                <div class="session-filter-wrapper">
                                    <div class="session-filter-title">Room Number
                                        <svg viewBox="0 0 100 100" class="icon shape-expand">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#shape-expand"></use>
                                        </svg>
                                    </div>
                                    <ul class="session-filter-options locations">
                                        <?php 

                                            foreach ($locations as $location) {

                                                $checked = '';
                                                if (isset($_GET['location'])) {
                                                 
                                                    if ($_GET['locations'] == $location->name || (is_array($_GET['locations']) && in_array($locations->name, $_GET['locations']))) {
                                                         $checked = 'checked';
                                                    }
                                                }
                                                
                                                echo '<li><div><input data-name="'.$location->name.'" value="'.$location->term_id.'" type="checkbox" '.$checked.' /><span>'.$location->name.'</span></div></li>';
                                            }

                                        ?>
                                    </ul>
                                </div> -->

                                <div class="session-filter-wrapper">
                                    <div class="session-filter-title">Themes
                                        <svg viewBox="0 0 100 100" class="icon shape-expand">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#shape-expand"></use>
                                        </svg>
                                    </div>
                                    <ul class="session-filter-options bbworldtracks">
                                        <?php 

                                            foreach ($bbworldtracks as $bbworldtrack) {

                                                $bbworldtrack->name = str_replace('&amp;', '&', $bbworldtrack->name);

                                                $checked = '';
                                                if (isset($_GET['bbworldtrack'])) {
                                                 
                                                    if ($_GET['bbworldtrack'] == $bbworldtrack->name || (is_array($_GET['bbworldtrack']) && in_array($bbworldtrack->name, $_GET['bbworldtrack']))) {
                                                         $checked = 'checked';
                                                    }
                                                }
                                                
                                                echo '<li><div><input data-name="'.$bbworldtrack->name.'" value="'.$bbworldtrack->term_id.'" type="checkbox" '.$checked.' /><span>'.$bbworldtrack->name.'</span></div></li>';
                                            }

                                        ?>
                                    </ul>
                                </div>


                                <div class="session-filter-wrapper">
                                    <div class="session-filter-title">Job Role
                                        <svg viewBox="0 0 100 100" class="icon shape-expand">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#shape-expand"></use>
                                        </svg>
                                    </div>
                                    <ul class="session-filter-options jobroles">
                                        <?php 

                                            foreach ($jobroles as $jobrole) {

                                                $checked = '';
                                                if (isset($_GET['job'])) {
                                                    if ($_GET['job'] == $jobrole->name || (is_array($_GET['job']) && in_array($jobrole->name, $_GET['job']))) {
                                                         $checked = 'checked';
                                                    }
                                                }

                                                echo '<li><div><input data-name="'.$jobrole->name.'" value="'.$jobrole->term_id.'" type="checkbox" '.$checked.' /><span>'.$jobrole->name.'</span></div></li>';
                                            }

                                        ?>
                                    </ul>
                                </div>

                                <div class="session-filter-wrapper">
                                    <div class="session-filter-title">Session Type 
                                        <svg viewBox="0 0 100 100" class="icon shape-expand">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#shape-expand"></use>
                                        </svg>
                                    </div>
                                    <ul class="session-filter-options types">
                                        <?php 

                                            foreach ($types as $type) {

                                                $type->name = str_replace('&amp;', '&', $type->name);

                                                $checked = '';
                                                if (isset($_GET['type'])) {
                                                    if ($_GET['type'] == $type->name || (is_array($_GET['type']) && in_array($type->name, $_GET['type']))) {
                                                         $checked = 'checked';
                                                    }
                                                }
                                                
                                                echo '<li><div><input data-name="'.$type->name.'" value="'.$type->term_id.'" type="checkbox" '.$checked.' /><span>'.$type->name.'</span></div></li>';
                                            }

                                        ?>
                                    </ul>
                                </div>


                            </form>
                        </div>
                    </div>
                    <div class="medium-8 columns">
                        
                        <div class="session-subtitle showing-subtitle post-meta dark">
                            <p>Showing All Sessions</p>
                        </div>
                        
                        <div class="loading-holder"></div>
                        
                    </div>

                </div>
            </div>
        </section>

        <?php 

        $bb_theme->footer_html();
    }

}

$bb_session_template = new BB_Session_Template;

	

?>