<?php

	global $post, $bb_theme;


    $post_slug = $post->post_name;
    $post_ancestry = get_post_ancestors($post->ID);

    //echo '<pre>';
    //print_r($post_ancestry);
    //exit;

    $poster = '';

    if (!empty($post_ancestry)) {
        $poster = get_post($post_ancestry[0]);
        $post_slug = $poster->post_name;
    }
    else {
        $poster = $post;
    }

    $bb_theme->header_html($post_slug);
    $bb_theme->top_header();
    $bb_theme->main_header($post->post_name);
	

    ?>
      

    <?php

	if ( have_posts() ) {
		while ( have_posts() ) {the_post(); ?>
            
            <?php 
                $bb_theme->hero_content($post->ID);
                if (is_front_page()) {
                    $bb_theme-> home_announcement(); 
                }     
            ?> 
                
            <div class="row">

                <?php the_content(); ?>
            </div>

            <?php $bb_theme->bottom_cta($post->ID); 
		} 
	} 

	$bb_theme->footer_html();

?>