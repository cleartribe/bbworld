<?php
/*
Template Name: BbWorld Live Login
*/
class BB_Login_Template {

    public function __construct() 
    {
        $this->hooks();
        $this->content();
    }

    public function hooks()
    {
        add_action( 'wp_footer', array($this, 'footer_scripts') );
        add_action( 'wp_head', array($this, 'header_scripts') );
    }

    public function header_scripts()
    {
        $nonce = wp_create_nonce( 'tnonce' );
        ?>
        <script>
            var tnonce = '<?php echo esc_attr($nonce); ?>'
                siteurl = "<?php echo site_url('/'); ?>",
                ajaxdata = { action : 'auth_user', nonce : tnonce, }

        </script>

        <?php
    }

    public function footer_scripts()
    {
        ?>
        <script>

            var options = {
                ajaxdata : ajaxdata,
            }

            bbApp.auth.init();
            bbApp.auth.login(options);
            
        </script>
        <?php 
    }

    public function content()
    {
        global $post, $bb_theme;

        $post_slug = $post->post_name;
        $post_ancestry = get_post_ancestors($post->ID);
        $poster = '';

        if (!empty($post_ancestry)) {
            $poster = get_post($post_ancestry[0]);
            $post_slug = $poster->post_name;
        }
        else {
            $poster = $post;
        }


        $bb_theme->header_html($post_slug);
        $bb_theme->top_header();
        $bb_theme->main_header($poster->post_name);


        if ( have_posts() ) {
            while ( have_posts() ) {the_post(); ?>
                
                <?php $bb_theme->hero_content($post->ID); ?> 

                <div class="row">
      
                    <div class="small-12 medium-7 medium-centered columns">

                        <div class="form-wrapper">

                            <?php 

                                if (isset($_GET['password']) && $_GET['password'] == 'changed') {
                                    echo '<h3>Your password has been updated. Please log in with your new information below.</h3>';
                                }

                                if (isset($_GET['registration'])) {
                                    echo '<h3>Thank you for registering. Check your inbox for a confirmation and email reminders about the event.</h3>';
                                }
                                
                            ?>

                            <form name="loginform" id="loginform">

                                <h3>Login and access all sessions by entering your username and password below.</h3><p><strong>Note: If you are a new user or a returning user from 2016, you will need to <a href="/bbworldlive/registration">register here</a> for a new username and password. </strong></p>

                                <span class="error-holder"></span>
                                        
                                <p class="login-username">
                                    <label for="user_login">Username</label>
                                    <input type="text" name="log" id="user_login" class="input" value="" size="20" required>
                                </p>
                                <p class="login-password">
                                    <label for="user_pass">Password</label>
                                    <input type="password" name="pwd" id="user_pass" class="input" value="" size="20" required>
                                </p>

                                <?php 

                                if (isset($_GET['redirect_to'])) {
                                    ?>
                                        <input type="hidden" name="redirect_to" value="<?php echo $_GET['redirect_to'] ?>">
                                    <?php 
                                }
                                   

                                ?>
                                
                                <p class="login-submit">
                                    <input type="submit" name="wp-submit" id="wp-submit" class="button dark button-primary" value="Log In">
                                    
                                </p>

                                 <a href="/lost-password/">Lost your username or password?</a>
                                
                            </form>

                         

                        </div>

                    </div>

                </div>

                <?php $bb_theme->bottom_cta($post->ID); 
            } 
        } 

        $bb_theme->footer_html(false);
    }

}

$bb_login_template = new BB_Login_Template;

?>