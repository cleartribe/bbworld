<?php
/*
Template Name: 404 Template
 */

$title = 'Oops Nothing Found';

global $post, $bb_theme, $wp_query;

$bb_theme->header_html('not-found');
$bb_theme->top_header();
$bb_theme->main_header('Not Found');
    	    
$bb_theme->hero_content(); ?>    
    
    <section class="main-inner-content">

        <div class="row">
            <div class="small-12 medium-8 medium-centered columns">
            
                
				<p>We are currently updating this site. Please check back soon.</p>
				<p>&nbsp;</p>
            </div>
        </div>

    </section>

<?php 

$bb_theme->footer_html();

?>