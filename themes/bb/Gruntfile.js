module.exports = function(grunt) {
 
    // load all grunt tasks
    grunt.loadNpmTasks('grunt-svgstore');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-autoprefixer');

    var gruntConfig = {

        svgstore: {
            options: {
                prefix : 'shape-', // This will prefix each <g> ID
            },
            default : {
                files: {
                    'images/svg-defs.svg': ['svg/*.svg'],
                }
            }
        },
        autoprefixer: {
            options: {
                browsers: ['last 1 version'] // more codenames at https://github.com/ai/autoprefixer#browsers
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: 'stylesheets/',
                    src: 'app.css',
                    dest: 'stylesheets/'
                }]
            }
        },
        watch: {
            all: {
                files: ['**/*.scss', 'js/*.js', '!js/app.js', '!js/app.min.js'],
                tasks: ['sass', 'autoprefixer', 'concat', 'uglify']                
            },
            css: {
                files: ['**/*.scss'],
                tasks: ['sass', 'autoprefixer']
            },
            js: {
                files: ['js/*.js', '!js/app.js', '!js/app.min.js'],
                tasks: ['concat', 'uglify']                
            }
        },
        sass: {
            dist: {                            // Target
                options: {                       // Target options
                    style: 'compressed'
                },
                files: {                         // Dictionary of files
                    'stylesheets/app.css': 'scss/app.scss'   // 'destination': 'source'
                }
            }
        },
        browserSync: {
            bsFiles: {
                src : [
                    'stylesheets/*.css',
                    '*.php',
                    'js/*.js'
                ]
            },
            options: {
                watchTask: true,
                proxy: "bbworld.dev"
            }
        },
        concat: {
            options: {
                separator: ''
            },
            dist: {
                src: [ // inputs, edit these files
                    'js/utilities.js',          // general functions that all JS files use
                    'js/header.js',             // open afApp()
                    'js/theme-menu.js',   
                    'js/ajax.js',
                    'js/auth.js',
                    'js/af-app-return.js',      // you shouldn't need to place any code under here
                    'js/footer.js'              // close afApp()
                ],
                dest: 'js/app.js' // output, do not edit
            }
        },
        uglify: {
            options: {
                mangle: true // must be true to reduce file size
            },
            dist: {
                files: {}
            }
        }
    };

    // must define dist.files here, as referencing concat.dist.dest from the above object was causing errors
    gruntConfig.uglify.dist.files['js/app.min.js'] = gruntConfig.concat.dist.dest;

    /**
     * I moved all of the config items out of grunt.initConfig()
     * this way we can add stuff dynamically,
     * like the above gruntConfig.uglify.dist.files
     * if we need to.
     */
    grunt.initConfig(gruntConfig); 

    //grunt tasks
    grunt.registerTask('svg', 'svgstore');
    grunt.registerTask('monitor', 'watch:all');
    grunt.registerTask('monitorjs', 'watch:js');
    grunt.registerTask('monitorcss', 'watch:css');
    grunt.registerTask('default', ['browserSync', 'watch:all']);

};