<?php

/*
Template Name: DevcCon Sessions Template
*/
class BB_Devcon_Session_Template {

    public $theme_dir;
    public $footer_html;
    public $top_sections;

    public function __construct() {
        $this->hooks();
        $this->content();
    }

    public function hooks()
    {
        add_action( 'wp_footer', array($this, 'footer_scripts') );
        add_action( 'wp_head', array($this, 'header_scripts') );

    }

    public function header_scripts()
    {
        $nonce = wp_create_nonce( 'tnonce' );
        ?>
        <script>
            var tnonce = '<?php echo esc_attr($nonce); ?>'
                postholder = '.loading-holder',
                siteurl = "<?php echo site_url('/'); ?>",
                ajaxdata = { action : 'get_sessions', nonce : tnonce, posttype : 'session', pagenum : 1 }
        </script>

        <?php
    }

    public function footer_scripts()
    {
        ?>
        <script>
            var options = {
                ajaxdata : ajaxdata,
                holder : postholder,
                template : 'devcon'
            }

            bbApp.ajax.init(options);
            
        </script>
        <?php 
    }

    public function content()
    {
        global $post, $bb_theme;

        $post_slug = $post->post_name;
        $post_ancestry = get_post_ancestors($post->ID);
        $poster = '';

        if (!empty($post_ancestry)) {
            $poster = get_post($post_ancestry[0]);
            $post_slug = $poster->post_name;
        }
        else {
            $poster = $post;
        }

        $bb_theme->header_html($post_slug);
        $bb_theme->top_header();
        $bb_theme->main_header($poster->post_name);

        //terms
        //$markets = get_terms ( 'markets' );
        //$jobroles = get_terms ( 'jobroles' );
        $types = get_terms ( 'sessiontypes' );
        $dates = get_terms ( 'dates' );
        $times = get_terms ( 'times' );
        $locations = get_terms ( 'locations' );
        $devcontracks = get_terms ( 'devcontracks' );
        $experience = get_terms ( 'experience' );

        if ( have_posts() ) {
            while ( have_posts() ) {the_post(); ?>
                <?php $bb_theme->hero_content($post->ID); 

                ?>

                <section class="main-inner-content" style="padding-top:0px;">
            
                <?php 
            }
        }


        ?> 

            <div class="session-wrapper" style="margin-top:0px;">
                <div class="session-inner row">
                    <div class="medium-4 columns">

                        <div class="session-subtitle post-meta dark">
                            <p>Filter Sessions By</p>
                        </div>

                        <div class="session-filters">
                          
                            <form class="session-filter-form">

                                <div class="session-filter-wrapper">
                                    <div class="session-filter-title">Tracks 
                                        <svg viewBox="0 0 100 100" class="icon shape-expand">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#shape-expand"></use>
                                        </svg>
                                    </div>
                                    <ul class="session-filter-options devcontracks">
                                        <?php 

                                            foreach ($devcontracks as $devcontrack) {

                                                $checked = '';
                                                if (isset($_GET['devcontrack'])) {
                                                    if ($_GET['devcontrack'] == $devcontrack->name || (is_array($_GET['devcontrack']) && in_array($devcontrack->name, $_GET['devcontrack']))) {
                                                         $checked = 'checked';
                                                    }
                                                }

                                                if ($devcontrack->name == 'Analytics')
                                                    $devcontrack->name = 'Learning Analytics';
                                                
                                                echo '<li><div><input data-name="'.$devcontrack->name.'" value="'.$devcontrack->term_id.'" type="checkbox" '.$checked.' /><span>'.$devcontrack->name.'</span></div></li>';
                                            }

                                        ?>
                                    </ul>
                                </div>

                                <div class="session-filter-wrapper">
                                    <div class="session-filter-title">Date
                                        <svg viewBox="0 0 100 100" class="icon shape-expand">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#shape-expand"></use>
                                        </svg>
                                    </div>
                                    <ul class="session-filter-options dates">
                                        <?php 

                                            foreach ($dates as $date) {


                                                $checked = '';
                                                if (isset($_GET['date'])) {
                                                    if ($_GET['date'] == $date->name || (is_array($_GET['date']) && in_array($date->name, $_GET['date']))) {
                                                         $checked = 'checked';
                                                    }
                                                }
                                                
                                                echo '<li><div><input data-name="'.$date->name.'" value="'.$date->term_id.'" type="checkbox" '.$checked.' /><span>'.$date->name.'</span></div></li>';
                                            }

                                        ?>
                                    </ul>
                                </div>

                                <!-- <div class="session-filter-wrapper">
                                    <div class="session-filter-title">Time
                                        <svg viewBox="0 0 100 100" class="icon shape-expand">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#shape-expand"></use>
                                        </svg>
                                    </div>
                                    <ul class="session-filter-options times hidden">
                                        <?php 

                                            foreach ($times as $time) {


                                                $checked = '';
                                                if (isset($_GET['time'])) {
                                                    if ($_GET['time'] == $time->name || (is_array($_GET['time']) && in_array($time->name, $_GET['time']))) {
                                                         $checked = 'checked';
                                                    }
                                                }
                                                
                                                echo '<li><div><input data-name="'.$time->name.'" value="'.$time->term_id.'" type="checkbox" '.$checked.' /><span>'.$time->name.'</span></div></li>';
                                            }

                                        ?>
                                    </ul>
                                </div>

                                <div class="session-filter-wrapper">
                                    <div class="session-filter-title">Room
                                        <svg viewBox="0 0 100 100" class="icon shape-expand">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#shape-expand"></use>
                                        </svg>
                                    </div>
                                    <ul class="session-filter-options locations">
                                        <?php 

                                            foreach ($locations as $location) {


                                                $checked = '';
                                                if (isset($_GET['location'])) {
                                                    if ($_GET['location'] == $location->name || (is_array($_GET['location']) && in_array($location->name, $_GET['location']))) {
                                                         $checked = 'checked';
                                                    }
                                                }
                                                
                                                echo '<li><div><input data-name="'.$location->name.'" value="'.$location->term_id.'" type="checkbox" '.$checked.' /><span>'.$location->name.'</span></div></li>';
                                            }

                                        ?>
                                    </ul>
                                </div> -->

                                
                                

                                <div class="session-filter-wrapper">
                                    <div class="session-filter-title">Session Type 
                                        <svg viewBox="0 0 100 100" class="icon shape-expand">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#shape-expand"></use>
                                        </svg>
                                    </div>
                                    <ul class="session-filter-options types">
                                        <?php 

                                            foreach ($types as $type) {

                                                if ($type->name == 'Rocket Session' || $type->name == 'Pre Conference')
                                                    continue;

                                                $checked = '';
                                                if (isset($_GET['type'])) {
                                                    if ($_GET['type'] == $type->name || (is_array($_GET['type']) && in_array($type->name, $_GET['type']))) {
                                                         $checked = 'checked';
                                                    }
                                                }
                                                
                                                echo '<li><div><input data-name="'.$type->name.'" value="'.$type->term_id.'" type="checkbox" '.$checked.' /><span>'.$type->name.'</span></div></li>';
                                            }

                                        ?>
                                    </ul>
                                </div>

                                <div class="session-filter-wrapper">
                                    <div class="session-filter-title">Experience Level 
                                        <svg viewBox="0 0 100 100" class="icon shape-expand">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#shape-expand"></use>
                                        </svg>
                                    </div>
                                    <ul class="session-filter-options experience">
                                        <?php 

                                            foreach ($experience as $exp) {


                                                $checked = '';
                                                if (isset($_GET['exp'])) {
                                                    if ($_GET['exp'] == $exp->name || (is_array($_GET['exp']) && in_array($exp->name, $_GET['exp']))) {
                                                         $checked = 'checked';
                                                    }
                                                }
                                                
                                                echo '<li><div><input data-name="'.$exp->name.'" value="'.$exp->term_id.'" type="checkbox" '.$checked.' /><span>'.$exp->name.'</span></div></li>';
                                            }

                                        ?>
                                    </ul>
                                </div>
   
                            </form>
                        </div>
                    </div>
                    <div class="medium-8 columns">
                        
                        <div class="session-subtitle showing-subtitle post-meta dark">
                            <p>Showing All Sessions</p>
                        </div>
                        
                        <div class="loading-holder"></div>
                        
                    </div>

                </div>
            </div>
        </section>

        <?php 

        $bb_theme->footer_html();
    }

}

$bb_devcon_session_template = new BB_Devcon_Session_Template;

	

?>