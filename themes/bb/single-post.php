<?php

	global $post, $bb_theme;

    $post_slug = $post->post_name;
    $post_ancestry = get_post_ancestors($post->ID);

    $bb_theme->header_html($post_slug);
    $bb_theme->top_header();
    $bb_theme->main_header($post->post_name);
        	
    	if ( have_posts() ) {
    		while ( have_posts() ) {the_post(); ?>
                
                <?php $bb_theme->hero_content($post->ID); ?>    
                    
                    <section id="main" class="main-inner-content single">

                        <div class="row">
                            <div class="small-12 medium-8 medium-centered columns">
                                <div class="post-meta">
                                    <p class="centered">Posted On <?php the_date( ); ?></p>
                                </div>
                                <h2 class="centered single-post-title"><?php the_title(); ?></h2>
                                <?php the_content();?>
                            </div>
                        </div>

                    </section>

                <?php 

                $bb_theme->bottom_cta($post->ID); 

    		} 
    	} 

	$bb_theme->footer_html();

?>