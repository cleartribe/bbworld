<?php
/*
Template Name: BbWorld Live Password Reset
*/
class BB_Reset_Template {

        public function __construct() 
        {
            $this->hooks();
            $this->content();
        }

        public function hooks()
        {
            add_action( 'wp_footer', array($this, 'footer_scripts') );
        }


        public function footer_scripts()
        {
            ?>
            <script>

                bbApp.auth.init();
        
            </script>
            <?php 
        }

        public function content()
        {

        	global $post, $bb_theme;

            $post_slug = $post->post_name;
            $post_ancestry = get_post_ancestors($post->ID);
            $poster = '';

            if (!empty($post_ancestry)) {
                $poster = get_post($post_ancestry[0]);
                $post_slug = $poster->post_name;
            }
            else {
                $poster = $post;
            }

            $bb_theme->header_html($post_slug);
            $bb_theme->top_header();
            $bb_theme->main_header($poster->post_name);
         

        	if ( have_posts() ) {
        		while ( have_posts() ) {the_post(); ?>
                    
                    <?php $bb_theme->hero_content($post->ID); ?> 

                    <div class="row">
          
                        <div class="small-12 medium-6 medium-centered columns">

                            <div class="form-wrapper">
                            <?php
                                // Parse shortcode attributes
                                $default_attributes = array( 'show_title' => false );
                             
                                if ( is_user_logged_in() ) {
                                    echo 'You are already signed in.';
                                } else {
                                    if ( isset( $_REQUEST['login'] ) && isset( $_REQUEST['key'] ) ) {
                                        $attributes['login'] = $_REQUEST['login'];
                                        $attributes['key'] = $_REQUEST['key'];
                             
                                        // Error messages
                                        $errors = array();
                                        if ( isset( $_REQUEST['error'] ) ) {
                                            $error_codes = explode( ',', $_REQUEST['error'] );
                             
                                            foreach ( $error_codes as $code ) {
                                                $errors [] = $bb_theme->get_error_message( $code );
                                            }
                                        }
                                        $attributes['errors'] = $errors;
                             
                                        ?>

                                        <div id="password-reset-form" class="widecolumn">
                                          
                                     
                                            <form name="resetpassform" id="resetpassform" action="<?php echo site_url( 'wp-login.php?action=resetpass' ); ?>" method="post" autocomplete="off">
                                                <input type="hidden" id="user_login" name="rp_login" value="<?php echo esc_attr( $attributes['login'] ); ?>" autocomplete="off" />
                                                <input type="hidden" name="rp_key" value="<?php echo esc_attr( $attributes['key'] ); ?>" />
                                                 
                                                <?php if ( count( $attributes['errors'] ) > 0 ) : ?>
                                                    <?php foreach ( $attributes['errors'] as $error ) : ?>
                                                        <p>
                                                            <?php echo $error; ?>
                                                        </p>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                         
                                                <p>
                                                    <label for="pass1"><?php _e( 'New password', 'personalize-login' ) ?></label>
                                                    <input type="password" name="pass1" id="pass1" class="input" size="20" value="" autocomplete="off" />
                                                </p>
                                                <p>
                                                    <label for="pass2"><?php _e( 'Repeat new password', 'personalize-login' ) ?></label>
                                                    <input type="password" name="pass2" id="pass2" class="input" size="20" value="" autocomplete="off" />
                                                </p>
                                                 
                                                <p class="description"><?php echo wp_get_password_hint(); ?></p>
                                                 
                                                <p class="resetpass-submit">
                                                    <input type="submit" name="submit" id="resetpass-button" class="button" value="<?php _e( 'Reset Password', 'personalize-login' ); ?>" />
                                                </p>
                                            </form>
                                        </div>

                                        <?php

                                    } else {
                                        echo 'Invalid password reset link.';
                                    }
                                }
                                ?>
                            </div>

                        </div>

                    </div>

                    <?php $bb_theme->bottom_cta($post->ID); 
        		} 
        	} 

            ?>
            

            <?php 

        	$bb_theme->footer_html(false);

        }
    }

    $bb_reset_template = new BB_Reset_Template;

?>