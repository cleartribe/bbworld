function ajaxClass()
{       
    var trigger = false;

    function make_ajax_request (options) {

        //ajaxdata, callback, holder
        var results = '';

        $(options.holder).append('<div class="loader">LOADING SESSIONS</div>');

        $.post(siteurl+'/wp-admin/admin-ajax.php', options.ajaxdata, function(data) {
            $('body '+options.holder+' .loader').remove();
            results = $.parseJSON(data);            

        }).fail(function() {

            console.log('failed');

        }).always(function() {

            if(results.html == '')
                results.html = 'No Sessions Found. Please Try Another Filter';
            
            $(options.holder).append(results.html);
            
            if (results.props.maxpages == options.ajaxdata.pagenum || results.props.maxpages == 0)
            return;

            trigger = false;
            monitor_scroll(results, options);

        });
    }

    function monitor_scroll(results, options) {
        
        var loading_div = $('body '+options.holder);
        loading_offset = loading_div.offset().top + loading_div.height();

        $( window ).scroll(function() {
            var bottom_screen = $(this).scrollTop() + $(this).height();
            if (bottom_screen >= loading_offset && !trigger) {
                trigger = true;
                options.ajaxdata.pagenum += 1;
                make_ajax_request(options);         
            }
        })
    }

    function set_filter_options(options, callback) {

        var types = new Array(),
            markets = new Array(),
            dates = new Array(),
            times = new Array(),
            locations = new Array(),
            jobroles = new Array(),
            devcontracks = new Array(),
            bbworldtracks = new Array(),
            experience = new Array(),
            types_string = '',
            markets_string = '',
            dates_string = '',
            times_string = '',
            locations_string = '',
            jobroles_string = '',
            devcontracks_string = '',
            bbworldtracks_string = '',
            experience_string = '',
            subtitle_string = 'Showing All ',
            state_query = '?';


        $('.session-filter-options.types input:checked').each(function (e) {
            types.push($(this).val());
            types_string += encodeURIComponent($(this).data('name')) + 's';

            state_query += 'type[]='+$(this).data('name')+'&';

            if ($('.session-filter-options.types input:checked').length == 1 || e == $('.session-filter-options.types input:checked').length - 1) {
                types_string += ' ';
            }
            else {
                 types_string += ' & ';
            }

        })
        $('.session-filter-options.markets input:checked').each(function (e) {
            markets.push($(this).val());
            markets_string += $(this).data('name');

            state_query += 'market[]='+encodeURIComponent($(this).data('name'))+'&';

            if ($('.session-filter-options.markets input:checked').length == 1 || e == $('.session-filter-options.markets input:checked').length - 1) {
                markets_string += ' ';
            }
            else {
                markets_string += ' & ';
            }
        })
        $('.session-filter-options.dates input:checked').each(function (e) {
            dates.push($(this).val());
            dates_string += $(this).data('name');

            state_query += 'date[]='+encodeURIComponent($(this).data('name'))+'&';

            if ($('.session-filter-options.dates input:checked').length == 1 || e == $('.session-filter-options.dates input:checked').length - 1) {
                dates_string += ' ';
            }
            else {
                dates_string += ' & ';
            }
        })
        $('.session-filter-options.times input:checked').each(function (e) {
            times.push($(this).val());
            times_string += $(this).data('name');

            state_query += 'date[]='+encodeURIComponent($(this).data('name'))+'&';

            if ($('.session-filter-options.times input:checked').length == 1 || e == $('.session-filter-options.times input:checked').length - 1) {
                times_string += ' ';
            }
            else {
                times_string += ' & ';
            }
        })

        $('.session-filter-options.locations input:checked').each(function (e) {
            locations.push($(this).val());
            locations_string += $(this).data('name');

            state_query += 'date[]='+encodeURIComponent($(this).data('name'))+'&';

            if ($('.session-filter-options.locations input:checked').length == 1 || e == $('.session-filter-options.locations input:checked').length - 1) {
                locations_string += ' ';
            }
            else {
                locations_string += ' & ';
            }
        })

        $('.session-filter-options.jobroles input:checked').each(function (e) {
            jobroles.push($(this).val());
            jobroles_string += $(this).data('name');

            state_query += 'job[]='+encodeURIComponent($(this).data('name'))+'&';

            if ($('.session-filter-options.jobroles input:checked').length == 1 || e == $('.session-filter-options.jobroles input:checked').length - 1) {
                jobroles_string += ' ';
            }
            else {
                jobroles_string += ' & ';
            }
        })
        $('.session-filter-options.devcontracks input:checked').each(function (e) {
            devcontracks.push($(this).val());
            devcontracks_string += $(this).data('name');

            state_query += 'devcontrack[]='+encodeURIComponent($(this).data('name'))+'&';

            if ($('.session-filter-options.devcontracks input:checked').length == 1 || e == $('.session-filter-options.devcontracks input:checked').length - 1) {
                devcontracks_string += ' ';
            }
            else {
                devcontracks_string += ' & ';
            }
        })
        $('.session-filter-options.bbworldtracks input:checked').each(function (e) {
            bbworldtracks.push($(this).val());
            bbworldtracks_string += $(this).data('name');

            state_query += 'bbworldtrack[]='+encodeURIComponent($(this).data('name'))+'&';

            if ($('.session-filter-options.bbworldtracks input:checked').length == 1 || e == $('.session-filter-options.bbworldtracks input:checked').length - 1) {
                bbworldtracks_string += ' ';
            }
            else {
                bbworldtracks_string += ' & ';
            }
        })
        $('.session-filter-options.experience input:checked').each(function (e) {
            experience.push($(this).val());
            experience_string += $(this).data('name');

            state_query += 'exp[]='+encodeURIComponent($(this).data('name'))+'&';

            if ($('.session-filter-options.experience input:checked').length == 1 || e == $('.session-filter-options.experience input:checked').length - 1) {
                experience_string += ' ';
            }
            else {
                experience_string += ' & ';
            }
        })


        if (types_string == '' && markets_string == '' && dates_string == '' && times_string == '' && locations_string == '' && jobroles_string == '' && bbworldtracks_string == '' && experience_string == '' && options.template == 'bbworld') {
            subtitle_string = 'Showing All Sessions';
        }

        else {
            if (types_string !== '') {
                subtitle_string += types_string;
            }
            if (markets_string !== '') {
                subtitle_string += 'In '+markets_string;
            }
            if (dates_string !== '') {
                subtitle_string += 'On '+dates_string;
            }
            if (times_string !== '') {
                subtitle_string += 'At '+times_string;
            }
            if (locations_string !== '') {
                subtitle_string += 'In Room '+locations_string;
            }
            if (jobroles_string !== '') {
                subtitle_string += 'For '+jobroles_string;
            }
            if (devcontracks_string !== '') {
                subtitle_string += 'In The '+devcontracks_string+' Track ';
            }
            if (bbworldtracks_string !== '') {
                subtitle_string += 'In The "'+bbworldtracks_string+'" Theme ';
            }
            if (experience_string !== '') {
                subtitle_string += 'With '+experience_string+' Level';
            }
        }

        $('.showing-subtitle p').html(decodeURIComponent(subtitle_string));

        var stateObj = {};

        if (state_query != '?')
            history.replaceState(stateObj, "", state_query);


        options.ajaxdata.pagenum = 1;
        options.ajaxdata.template = options.template;
        options.ajaxdata.types = types;
        options.ajaxdata.markets = markets;
        options.ajaxdata.dates = dates;
        options.ajaxdata.times = times;
        options.ajaxdata.locations = locations;
        options.ajaxdata.jobroles = jobroles;
        options.ajaxdata.devcontracks = devcontracks;
        options.ajaxdata.bbworldtracks = bbworldtracks;
        options.ajaxdata.experience = experience;

        $(options.holder).html('');
        make_ajax_request(options);

        if (typeof callback == 'function')
            callback();

    }


    function enable_filtering(options) {

        $('.session-filter-title').on('click', function () {
            $(this).parent().find('.session-filter-options').toggleClass('hidden');
        })

        $('.session-filter-options input').on('change', function () {
            set_filter_options(options, function () {
                $('html,body').animate({
                    scrollTop: $('.showing-subtitle').offset().top - 60
                }, 500);
            });

        })
    }

    function enable_expanding()
    {
         filter_offset = $('.session-filters').offset().top - 80;

        $( window ).scroll(function() {
            var top_screen = $(this).scrollTop(),
                bottom_screen = $(this).scrollTop() + $(window).height(),
                session_wrapper_offset = ($('.session-wrapper').offset().top + 80) + $('.session-wrapper').height(); 

            if (top_screen >= filter_offset && bottom_screen <= session_wrapper_offset) {
                $('.session-filters').css({
                    'position' : 'fixed',
                    'top' : '80px'
                });
            }
            else if (top_screen <= filter_offset) {
                $('.session-filters').css({
                    'position' : 'inherit',
                    'top' : ''
                });
            }
            else if (bottom_screen >= session_wrapper_offset) {
                $('.session-filters').css({
                    'position' : 'fixed',
                    'top' : -Math.abs(bottom_screen - session_wrapper_offset ) + 'px',
                    'height' : '100%'
                });
            }

        });
     
    }

    function enable_more_toggle()
    {
       
        $('body').on('click', '.more-toggle', function (e){
            e.preventDefault();
            var more_html = $(this).parent().next().html();
            $(this).parent().html(more_html);
        });
    }

    return {
        init : function (options) {
            enable_filtering(options);
            set_filter_options(options);
            enable_more_toggle();
        },
    }
}