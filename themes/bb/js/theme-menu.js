var desktop_breakpoint = 640;


if (typeof noScroll == 'undefined') {
    var main_menu = menuClass().init();
}

function menuClass()
{       

    var _timeout,
        main_window = $(window),
        main_menu_btn = $('.menu-button'),
        main_menu = $('.main-menu'),
        main_header = $('.main-header'),
        sub_menu = $('.main-menu .shape-expand');


    window.onresize = function()
    {
      clearTimeout(_timeout);
      _timeout = setTimeout(windowResizeComplete, 100);
    }


    function windowResizeComplete () 
    {
        main_menu.css({'height' : 'auto'});
        $('.main-menu li a.expandable, .sub-menu, .menu-logo, .main-header, .main-header .menu-register-btn').attr('style', '');
        triggerMenuScroll();
    }

    function mainMenuToggleClick () 
    {
         main_menu_btn.on('click', function(e) {
            e.preventDefault();
            $(this).toggleClass('active');
            toggleSVGClass(this, 'collapsed', ['shape-hamburger', 'shape-close']);
            main_menu.toggleClass('hide-for-small');
            $('.menu-register-btn, .menu-logo').toggleClass('hide-for-small');
            main_menu.height(main_window.height());
        });
    }

    function subMenuToggleClick ()
    {
        //submenu expand
        sub_menu.on('click touchstart', function(e) {

            if ($(window).width() > desktop_breakpoint) {
                return;
            }

            e.preventDefault();
            $(this).parent().next().toggleClass('hidden');

            //do an svg class toggle since svgs are not supported for jquery class manipulation
            toggleSVGClass($(this).parent(), 'collapsed');

            //if medium and up - set submenu height based on window
            if (main_window.width() >= desktop_breakpoint) {
                if ($(this).parent().next().height() > main_window.height() - main_header.height() * 2) {
                    $(this).parent().next().css({
                        'height': main_window.height() - main_header.height() * 2
                    })
                }
                main_header.prepend('<div class="overlay"></div>');

                //submenu close button collapse
                $('.close-btn, .overlay').on('click', function(e) {
                    e.preventDefault();
                    $('.sub-menu:not(.hidden)').toggleClass('hidden');
                    $('.overlay').remove();
                });

            }
        });
    }

    //handle the svg toggle independently 
    function toggleSVGClass(el, classname, swapshape) 
    {
        var curClasses = $('svg.icon', el).attr('class');

        //open
        if (curClasses.indexOf(classname) === -1) {
            curClasses += ' ' + classname;
            if (typeof swapshape !== 'undefined') {
                if (curClasses.indexOf(swapshape[1])) {
                    curClasses = curClasses.replace(swapshape[1], swapshape[0]);
                    $('svg use', el).attr('xlink:href', '#' + swapshape[0]);
                }
            }
        }

        //closed
        else {
            curClasses = curClasses.replace(' ' + classname, '');
            if (typeof swapshape !== 'undefined') {

                if (curClasses.indexOf(swapshape[0])) {
                    curClasses = curClasses.replace(swapshape[0], swapshape[1]);
                    $('svg use', el).attr('xlink:href', '#' + swapshape[1]);
                }
            }
        }
        //apply updated classes
        $('svg.icon', el).attr('class', curClasses);
    }


    function menuScroll() 
    {
       
        $(window).scroll(function(){
            triggerMenuScroll();
        });
    }

    function triggerMenuScroll()
    {
        var largeTop = 121,
        smallTop = 47;

        if ($(this).width() > desktop_breakpoint) {         
            largeMenuScroll(largeTop);
        }
        else {
            smallMenuScroll(smallTop);
        }
    }


    function largeMenuScroll(largeTop)
    {
        var inc = largeTop - $(this).scrollTop();
        
        if (inc >= largeTop - 5) {
            $('.main-menu li a.expandable').css({'opacity' : '1.0'});
            $('.main-header').css({'background' : 'rgba(0,0,0,0.25)'});
        }
        else if (inc > 0 && inc < largeTop - 5) {
            $('.main-menu li a.expandable').css({'opacity' : '0.'+Math.floor((pad(inc, 2)/10))*10, 'display' : 'block'});
        }
        else {
             $('.main-menu li a.expandable').css({'opacity' : '0', 'display' : 'none'});
             $('.main-header').css({'background' : 'rgba(0,0,0,0)'});
        }

        if(inc > 0 ){
            $('.sub-menu').css({'top' : inc+'px', 'background' : 'rgba(0,0,0,0.25)'});
            $('.menu-logo, .main-header .menu-register-btn').fadeOut('fast', function () {
                $(this).attr('style', '');
            })
        }
        else {
            $('.sub-menu').css({'background' : '#000', 'top' : '0px'});
            $('.menu-logo').css({'opacity' : '1', 'position' : 'absolute', 'left' : '0px',  'top' : '-53px', 'z-index' : '30'}).fadeIn('fast');
            $('.main-header .menu-register-btn').css({'position' : 'absolute',  'top' : '-54px', 'width' : 'auto', 'right' : '-45px', 'z-index' : '30'}).fadeIn('fast');
        }

        if (inc <= 51) {
            $('.main-header').css({'border-bottom-width' : '0px'});
        }
        else if (inc > 51) {
            $('.main-header').css({'border-bottom-width' : '1px'});
        }
    }

    function smallMenuScroll(smallTop)
    {
        var inc = smallTop - $(this).scrollTop();
                
        if(inc > 0){
            $('.main-header').css({'top' : inc+'px', 'background' : 'transparent'});
        }
        else {
            $('.main-header').css({'background' : '#000', 'top' : '0px'});
        }
    }

    function pad (str, max) {
      str = str.toString();
      return str.length < max ? pad("0" + str, max) : str;
    }

    return {
        init : function () {
            mainMenuToggleClick(); 
            subMenuToggleClick();
            menuScroll();
            triggerMenuScroll();
        }
    }
}


//button function 
$('a.button.scroll').on('click', function (e){
    e.preventDefault();
    $('html,body').animate({
      scrollTop: $('.registration-form-holder').offset().top - 100
    }, 1000);
})


//closed captions button
var cc_is_open = false;


$('body').on('click', '.closed-captions-btn', function (e) {

    e.preventDefault();

    if (!cc_is_open) {
        $('iframe.streamtext').slideDown('fast');
        $(this).html('Hide Closed Captions');
        cc_is_open = true;
    }
    else {
        $('iframe.streamtext').slideUp('fast');
        $(this).html('Show Closed Captions');
        cc_is_open = false;
    }
});

//form registration utm parameter additions
var defaultHiddenFieldNameValue = "";
function getQueryStringParamValue(strQStrParam) {
    var strURL = document.location.href;
    var strQStrParamValue = "";
    if (strURL.indexOf('?') != -1)
    {
        strQStrParamValue = strURL.substr(strURL.indexOf('?') + 1);
        if (strQStrParamValue.indexOf(strQStrParam) != -1)
        {
            strQStrParamValue = strQStrParamValue.substr(strQStrParamValue.indexOf(strQStrParam));
            strQStrParamValue = strQStrParamValue.substr(strQStrParamValue.indexOf('=') + 1);
            
            if (strQStrParamValue.indexOf('&') != -1)
                strQStrParamValue = strQStrParamValue.substr(0, strQStrParamValue.indexOf('&'));
            return strQStrParamValue;
        }
        else
        {
            strQStrParamValue = defaultHiddenFieldNameValue;
            return strQStrParamValue;
        }
    }
    else
    {
        strQStrParamValue = defaultHiddenFieldNameValue;
        return strQStrParamValue;
    }
}

//repeat for each field to populate
$('form.regform .source').attr('value', getQueryStringParamValue('utm_source'));
$('form.regform .medium').attr('value', getQueryStringParamValue('utm_medium'));
$('form.regform .term').attr('value', getQueryStringParamValue('utm_term'));
$('form.regform .content').attr('value', getQueryStringParamValue('utm_content'));
$('form.regform .campaign').attr('value', getQueryStringParamValue('utm_campaign'));  