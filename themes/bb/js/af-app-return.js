

/**

 */

return {
    user: function(options) {
        //users.init(options);
    },
    ajax: {
    	init: function (options) {
    		ajaxClass().init(options);
    	}
    },
    auth: {
    	init: function () {
    		authClass().init();
    	},
    	gate: function (options, callback) {
    		authClass().gate(options, callback);
    	},
    	login: function (options) {
    		authClass().login(options);
    	},
        chat: function (postid) {
            authClass().chat(postid);
        },
        poll: function (postid) {
            authClass().poll(postid);
        },
        vcpoll: function(randstring) {
            authClass().vcpoll(randstring);
        }
    }


}