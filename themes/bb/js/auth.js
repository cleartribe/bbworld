function authClass()
{       
    var user_meta = undefined,
        livestreamurl = '/bbworldlive/live-streaming/',
        is_chat = false;

    function init() {

        if (is_logged_in()) {

            //track the user view as an event
            var user_meta = get_user_meta();
            window.onload = function() {

                ga('gtm1.send', {
                  hitType: 'event',
                  eventCategory: 'bbwlive user log',
                  eventAction: window.location.href,
                  eventLabel: user_meta.id
                });
            }

            //set the logout button
            $('.top-header .top-register-btn, .menu-register-btn a').html('Log Out').addClass('top-logout-btn');

            //hide the reg stuff
            $('.sub-menu li:last-child, .reg_form, .loggedout').hide();

            //set the logout button click action
            $('body').on('click', '.top-logout-btn', function (e) {
                e.preventDefault();
                $.removeCookie('bbwluser', { path: '/' });
                var redirect = get_redirect();
                window.location.href = redirect;
            })

            var url = siteurl+'wp-content/themes/bb/quick/livestream_islive.txt';

            //check if its live streaming or not
            make_ajax_request(url, function (response) {
                if (response == '1') {
                    $('.sub-menu').prepend('<li><strong><a href="'+livestreamurl+'">Streaming Today</a></strong></li>');
                }
               
            })
        }

        
    }

    //this is used to redirect users away if they are not authenticated
    function gate (options, callback) {
        if (!is_logged_in()) {
  
            if (typeof options.ajaxdata.redirect_to == 'string') {
                redirect = get_redirect(options.ajaxdata.redirect_to)
            }
            else {
                redirect = get_redirect('');
            }
            window.location.href = redirect;
        }
        else {
            if (typeof callback == 'function') {
                callback();
            }
        }
    }
    

    function login (options) {
        
        //if the user is already logged in, redirect them to the right location
        if (is_logged_in()) {
            user_meta = get_user_meta();
            var redirect = get_redirect();
            window.location.href = redirect;
        }

        //when they click the login button
        $('#loginform').on('submit', function (e) {
            
            e.preventDefault();

            //get the serialized form data and merge it with options passed to this method
            var formdata = objectifyForm($(this).serializeArray());
            options.ajaxdata = mergeObjects(options.ajaxdata, formdata);

            options.ajaxdata.pwd = btoa(options.ajaxdata.pwd);

            var url = siteurl+'/wp-json/bbwlive/'+options.ajaxdata.action+'/'+options.ajaxdata.log+'/'+options.ajaxdata.pwd

            //call ajax to verify auth
            make_ajax_request(url, function (response) {

                if ((response == '"incorrect_password"') || (response == '"invalid_username"')) {
                    var error_message = get_error_message(response);
                    display_error_message(error_message);
                }

                //on success, set cookie, get user meta to determine redirect and then.... redirect
                else {

                    response = JSON.parse(response);

                    $.cookie('bbwluser', response.data.ID+"|"+response.data.user_login+"|"+response.data.user_type, { path: '/' });
                    user_meta = get_user_meta();

                    var redirect = get_redirect(options.ajaxdata.redirect_to);
                    //is this live now?
                    if (response.data.is_live == true && options.ajaxdata.redirect_to == undefined)
                        redirect = livestreamurl;

                    
                    window.location.href = redirect;
                }

            });

        })

    }

    function chat(postid) {

        var userdata = get_user_meta();
        var url = siteurl+'/wp-json/bbwlive/chat_init/'+postid+'/'+userdata.name+'/'+userdata.id

        make_ajax_request(url, function (response) {

            response = JSON.parse(response);

            //ok there is a chat window so we are live
            if (response != '') {
                is_chat = true;
                single_pollrefresher(postid)
            }

            $('.chat-wrapper').html(response);

        });
    }

    function poll(postid)
    {

        var url = siteurl+'wp-content/themes/bb/quick/livestream_archive.txt';

        make_ajax_request(url, function (response) {

            if (response !== $('.live-session-holder').html()) {
                $('.live-session-holder').html(response);
            }

        });

        setTimeout(function () { poll(postid) }, 20000);
    }

    function vcpoll(randstring)
    {

        var vcelem = $('.vc-live-session-holder.'+randstring);

        var year = vcelem.data('year');
        var sessions = vcelem.data('sessions');
        var url = siteurl+'wp-content/themes/bb/quick/livestream_vc_archive_'+year+'_'+sessions+'.txt';

        make_ajax_request(url, function (response) {

            if (response !== vcelem.html()) {
                vcelem.html(response);
            }

        });

        setTimeout(function () { vcpoll(randstring) }, 20000);
    }

    function single_pollrefresher(postid)
    {
  
        var url = siteurl+'wp-content/themes/bb/quick/livestream_single.txt';

        make_ajax_request(url, function (response) {

            if (response !== window.location.href && response !== false && is_chat == true) {
                window.location.href = response;
            }

        });

        setTimeout(function () { single_pollrefresher(postid) }, 20000);

    }

    //simple function for determining if the cookie is set
    function is_logged_in() {
        return $.cookie('bbwluser');
    }

    //take the cookie data and split it into a usable object
    function get_user_meta() {
        autharray = is_logged_in().split("|");
        user_meta = {
            'id' : autharray[0],
            'name' : autharray[1],
            'type' : autharray[2]
        }
        return user_meta;
    }

    //conditionals for where to redirect a user. this handles login and logout redirects
    function get_redirect(redirect) {

        //not logged in
        if (!is_logged_in() && redirect !== undefined) {
            return '/login/?redirect_to='+redirect;
        }
        else if (!is_logged_in()) {
            return '/login/';
        }

        //logged in
        if (typeof redirect !== "undefined") {
            return redirect;
        }
        else if (user_meta.type == 'Developer (July 25)') {
            return '/bbworldlive/';
        }
        else if (user_meta.type == 'Professional Education (July 25)') {
            return '/bbworldlive/professional-education/';
        }
        else if (user_meta.type == 'Corporate Training (July 12)') {
            return '/bbworldlive/professional-education/';
        }
        else if (user_meta.type == 'Blackboard Keynote (July 25)') {
            return '/bbworldlive/';
        }
        else if (user_meta.type == 'K-12 (July 26)') {
            return '/bbworldlive/k12/';
        }
        else if (user_meta.type == 'Higher Education (July 27)') {
            return '/bbworldlive/higher-education/';
        }
        else {
            return '/bbworldlive/';
        }

    }

    //this handles login error messages. can probably be reused for other error handling when the need arises. 
    function get_error_message(error) {
        if (error == '"invalid_username"' || error == '"incorrect_password"') {
            return '<p>Login Failed. Please Check Your Username and Password and Try Again.</p>';
        }
    }

    function display_error_message(error_message) {
        $('#loginform .error-holder').html(error_message);
    }


    function make_ajax_request (url, callback) {

        var timestamp = Date.now() / 1000 | 0;

        var iscallback = false;

        if (typeof callback == 'function')
            iscallback = true;

        $.get(url+'?cachebust='+timestamp, function(data) {

            if (iscallback)
                callback(data);
                 
        }, "text").fail(function() {

            if (iscallback)
                callback(false);

        }).always(function() {


        });
    }


    return {
        poll: function (postid) {
            poll(postid);
        },
        vcpoll: function (randstring) {
            vcpoll(randstring);
        },
        chat: function (postid) {
            chat(postid);
        },
        login : function (options) {
            login(options);
        },
        gate : function (options, callback) {
            gate(options, callback);
        },
        init : function () {
            init();
        }
    }
}