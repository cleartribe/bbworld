
/**
 * This is the closing file in a series of concatenated files
 */

/**
 * IMPORTANT!
 * Do not place anything above this closing brace.
 * af-app-return.js returns an object for AgoraApp()
 */
} // closes AgoraApp()

window.bbApp = BBApp();

})(window, document, undefined, $);


/**
 * PLACE YOUR FOOTER CODE BELOW HERE
 */