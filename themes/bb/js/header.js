
/**
 * This is the opening container for our global afApp() function
 * setting these parameters here, ensures they are the same an
 * unchanged by other third-party scripts  
 * 
 * @param  {object} window   
 * @param  {object} document 
 * @param  {object} undefined
 * @param  {object} $        
 * @return {object}          
 */
(function(window, document, undefined, $) {

/**
 * this is a general wrapper that never gets called
 * in the global scope.
 * The global scope references afApp(), which
 * is defined in the footer.js file
 */
function BBApp() {

//jquery wrapper

//Ga tracking
window.onload = function() {

    ga('create', 'UA-73901262-1', 'auto', 'bbTracker');
   
    $('form.attend').on('submit', function (e) {
    	ga('bbTracker.send', {
	      hitType: 'event',
	      eventCategory: 'BbWorld Why Attend Selection',
	      eventAction: $(this).serialize(),
	    });
    })

};



/**
 * The remaining code get's concatenated to the file
 * the last file is footer.js, which closes this function
 */