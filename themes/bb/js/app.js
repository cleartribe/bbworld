
/**
 * This is so we can use $ everywhere.
 * @type {object}
 */
$ = (typeof $ == 'undefined') ? jQuery : $;


/*!
 * jQuery Cookie Plugin v1.4.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2013 Klaus Hartl
 * Released under the MIT license
 */
!function(e){"function"==typeof define&&define.amd?define(["jquery"],e):e("object"==typeof exports?require("jquery"):jQuery)}(function(e){function n(e){return u.raw?e:encodeURIComponent(e)}function o(e){return u.raw?e:decodeURIComponent(e)}function i(e){return n(u.json?JSON.stringify(e):String(e))}function r(e){0===e.indexOf('"')&&(e=e.slice(1,-1).replace(/\\"/g,'"').replace(/\\\\/g,"\\"));try{return e=decodeURIComponent(e.replace(c," ")),u.json?JSON.parse(e):e}catch(n){}}function t(n,o){var i=u.raw?n:r(n);return e.isFunction(o)?o(i):i}var c=/\+/g,u=e.cookie=function(r,c,f){if(void 0!==c&&!e.isFunction(c)){if(f=e.extend({},u.defaults,f),"number"==typeof f.expires){var a=f.expires,d=f.expires=new Date;d.setTime(+d+864e5*a)}return document.cookie=[n(r),"=",i(c),f.expires?"; expires="+f.expires.toUTCString():"",f.path?"; path="+f.path:"",f.domain?"; domain="+f.domain:"",f.secure?"; secure":""].join("")}for(var p=r?void 0:{},s=document.cookie?document.cookie.split("; "):[],m=0,x=s.length;x>m;m++){var v=s[m].split("="),k=o(v.shift()),l=v.join("=");if(r&&r===k){p=t(l,c);break}r||void 0===(l=t(l))||(p[k]=l)}return p};u.defaults={},e.removeCookie=function(n,o){return void 0===e.cookie(n)?!1:(e.cookie(n,"",e.extend({},o,{expires:-1})),!e.cookie(n))}});


/**
 * get a url paramater
 * @return string if null, return empty string
 */
function getUrlParameter(e) {e=e.replace(/[\[]/,"\\[").replace(/[\]]/,"\\]");var n=new RegExp("[\\?&]"+e+"=([^&#]*)"),r="";if(typeof t==="undefined"){r=n.exec(location.search)}else{r=n.exec(t)}return r==null?"":decodeURIComponent(r[1].replace(/\+/g," "))};

/**
 * Domready. this can be used incase jquery isn't yet available
 * it's much smaller on more efficient the jquery.ready
 */
!function(a,ctx,b){typeof module!="undefined"?module.exports=b():typeof define=="function"&&typeof define.amd=="object"?define(b):ctx[a]=b()}("domready",this,function(a){function m(a){l=1;while(a=b.shift())a()}var b=[],c,d=!1,e=document,f=e.documentElement,g=f.doScroll,h="DOMContentLoaded",i="addEventListener",j="onreadystatechange",k="readyState",l=/^loade|c/.test(e[k]);return e[i]&&e[i](h,c=function(){e.removeEventListener(h,c,d),m()},d),g&&e.attachEvent(j,c=function(){/^c/.test(e[k])&&(e.detachEvent(j,c),m())}),a=g?function(c){self!=top?l?c():b.push(c):function(){try{f.doScroll("left")}catch(b){return setTimeout(function(){a(c)},50)}c()}()}:function(a){l?a():b.push(a)}});



/**
 * http://stackoverflow.com/a/46181/1334612
 * 
 * @param  {string} email
 * @return {boolean}
 */
function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}


function objectifyForm(formArray) {//serialize data function

  var returnArray = {};
  for (var i = 0; i < formArray.length; i++){
    returnArray[formArray[i]['name']] = formArray[i]['value'];
  }
  return returnArray;
}

function mergeObjects(obj1,obj2){
    var obj3 = {};
    for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
    for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
    return obj3;
}

/**
 * Get Unique values from an array
 * @param  {[type]} v [description]
 * @return {[type]}   [description]
 */
function getUniqueValues(v)
{
    return v.filter(function(item, i, ar){ return ar.indexOf(item) === i; });
}

function isInArray(value, array) {
    if (typeof array.indexOf == 'undefined') {
        return false;
    }
    return array.indexOf(value) > -1;
}




/**
 * This is the opening container for our global afApp() function
 * setting these parameters here, ensures they are the same an
 * unchanged by other third-party scripts  
 * 
 * @param  {object} window   
 * @param  {object} document 
 * @param  {object} undefined
 * @param  {object} $        
 * @return {object}          
 */
(function(window, document, undefined, $) {

/**
 * this is a general wrapper that never gets called
 * in the global scope.
 * The global scope references afApp(), which
 * is defined in the footer.js file
 */
function BBApp() {

//jquery wrapper

//Ga tracking
window.onload = function() {

    ga('create', 'UA-73901262-1', 'auto', 'bbTracker');
   
    $('form.attend').on('submit', function (e) {
    	ga('bbTracker.send', {
	      hitType: 'event',
	      eventCategory: 'BbWorld Why Attend Selection',
	      eventAction: $(this).serialize(),
	    });
    })

};



/**
 * The remaining code get's concatenated to the file
 * the last file is footer.js, which closes this function
 */var desktop_breakpoint = 640;


if (typeof noScroll == 'undefined') {
    var main_menu = menuClass().init();
}

function menuClass()
{       

    var _timeout,
        main_window = $(window),
        main_menu_btn = $('.menu-button'),
        main_menu = $('.main-menu'),
        main_header = $('.main-header'),
        sub_menu = $('.main-menu .shape-expand');


    window.onresize = function()
    {
      clearTimeout(_timeout);
      _timeout = setTimeout(windowResizeComplete, 100);
    }


    function windowResizeComplete () 
    {
        main_menu.css({'height' : 'auto'});
        $('.main-menu li a.expandable, .sub-menu, .menu-logo, .main-header, .main-header .menu-register-btn').attr('style', '');
        triggerMenuScroll();
    }

    function mainMenuToggleClick () 
    {
         main_menu_btn.on('click', function(e) {
            e.preventDefault();
            $(this).toggleClass('active');
            toggleSVGClass(this, 'collapsed', ['shape-hamburger', 'shape-close']);
            main_menu.toggleClass('hide-for-small');
            $('.menu-register-btn, .menu-logo').toggleClass('hide-for-small');
            main_menu.height(main_window.height());
        });
    }

    function subMenuToggleClick ()
    {
        //submenu expand
        sub_menu.on('click touchstart', function(e) {

            if ($(window).width() > desktop_breakpoint) {
                return;
            }

            e.preventDefault();
            $(this).parent().next().toggleClass('hidden');

            //do an svg class toggle since svgs are not supported for jquery class manipulation
            toggleSVGClass($(this).parent(), 'collapsed');

            //if medium and up - set submenu height based on window
            if (main_window.width() >= desktop_breakpoint) {
                if ($(this).parent().next().height() > main_window.height() - main_header.height() * 2) {
                    $(this).parent().next().css({
                        'height': main_window.height() - main_header.height() * 2
                    })
                }
                main_header.prepend('<div class="overlay"></div>');

                //submenu close button collapse
                $('.close-btn, .overlay').on('click', function(e) {
                    e.preventDefault();
                    $('.sub-menu:not(.hidden)').toggleClass('hidden');
                    $('.overlay').remove();
                });

            }
        });
    }

    //handle the svg toggle independently 
    function toggleSVGClass(el, classname, swapshape) 
    {
        var curClasses = $('svg.icon', el).attr('class');

        //open
        if (curClasses.indexOf(classname) === -1) {
            curClasses += ' ' + classname;
            if (typeof swapshape !== 'undefined') {
                if (curClasses.indexOf(swapshape[1])) {
                    curClasses = curClasses.replace(swapshape[1], swapshape[0]);
                    $('svg use', el).attr('xlink:href', '#' + swapshape[0]);
                }
            }
        }

        //closed
        else {
            curClasses = curClasses.replace(' ' + classname, '');
            if (typeof swapshape !== 'undefined') {

                if (curClasses.indexOf(swapshape[0])) {
                    curClasses = curClasses.replace(swapshape[0], swapshape[1]);
                    $('svg use', el).attr('xlink:href', '#' + swapshape[1]);
                }
            }
        }
        //apply updated classes
        $('svg.icon', el).attr('class', curClasses);
    }


    function menuScroll() 
    {
       
        $(window).scroll(function(){
            triggerMenuScroll();
        });
    }

    function triggerMenuScroll()
    {
        var largeTop = 121,
        smallTop = 47;

        if ($(this).width() > desktop_breakpoint) {         
            largeMenuScroll(largeTop);
        }
        else {
            smallMenuScroll(smallTop);
        }
    }


    function largeMenuScroll(largeTop)
    {
        var inc = largeTop - $(this).scrollTop();
        
        if (inc >= largeTop - 5) {
            $('.main-menu li a.expandable').css({'opacity' : '1.0'});
            $('.main-header').css({'background' : 'rgba(0,0,0,0.25)'});
        }
        else if (inc > 0 && inc < largeTop - 5) {
            $('.main-menu li a.expandable').css({'opacity' : '0.'+Math.floor((pad(inc, 2)/10))*10, 'display' : 'block'});
        }
        else {
             $('.main-menu li a.expandable').css({'opacity' : '0', 'display' : 'none'});
             $('.main-header').css({'background' : 'rgba(0,0,0,0)'});
        }

        if(inc > 0 ){
            $('.sub-menu').css({'top' : inc+'px', 'background' : 'rgba(0,0,0,0.25)'});
            $('.menu-logo, .main-header .menu-register-btn').fadeOut('fast', function () {
                $(this).attr('style', '');
            })
        }
        else {
            $('.sub-menu').css({'background' : '#000', 'top' : '0px'});
            $('.menu-logo').css({'opacity' : '1', 'position' : 'absolute', 'left' : '0px',  'top' : '-53px', 'z-index' : '30'}).fadeIn('fast');
            $('.main-header .menu-register-btn').css({'position' : 'absolute',  'top' : '-54px', 'width' : 'auto', 'right' : '-45px', 'z-index' : '30'}).fadeIn('fast');
        }

        if (inc <= 51) {
            $('.main-header').css({'border-bottom-width' : '0px'});
        }
        else if (inc > 51) {
            $('.main-header').css({'border-bottom-width' : '1px'});
        }
    }

    function smallMenuScroll(smallTop)
    {
        var inc = smallTop - $(this).scrollTop();
                
        if(inc > 0){
            $('.main-header').css({'top' : inc+'px', 'background' : 'transparent'});
        }
        else {
            $('.main-header').css({'background' : '#000', 'top' : '0px'});
        }
    }

    function pad (str, max) {
      str = str.toString();
      return str.length < max ? pad("0" + str, max) : str;
    }

    return {
        init : function () {
            mainMenuToggleClick(); 
            subMenuToggleClick();
            menuScroll();
            triggerMenuScroll();
        }
    }
}


//button function 
$('a.button.scroll').on('click', function (e){
    e.preventDefault();
    $('html,body').animate({
      scrollTop: $('.registration-form-holder').offset().top - 100
    }, 1000);
})


//closed captions button
var cc_is_open = false;


$('body').on('click', '.closed-captions-btn', function (e) {

    e.preventDefault();

    if (!cc_is_open) {
        $('iframe.streamtext').slideDown('fast');
        $(this).html('Hide Closed Captions');
        cc_is_open = true;
    }
    else {
        $('iframe.streamtext').slideUp('fast');
        $(this).html('Show Closed Captions');
        cc_is_open = false;
    }
});

//form registration utm parameter additions
var defaultHiddenFieldNameValue = "";
function getQueryStringParamValue(strQStrParam) {
    var strURL = document.location.href;
    var strQStrParamValue = "";
    if (strURL.indexOf('?') != -1)
    {
        strQStrParamValue = strURL.substr(strURL.indexOf('?') + 1);
        if (strQStrParamValue.indexOf(strQStrParam) != -1)
        {
            strQStrParamValue = strQStrParamValue.substr(strQStrParamValue.indexOf(strQStrParam));
            strQStrParamValue = strQStrParamValue.substr(strQStrParamValue.indexOf('=') + 1);
            
            if (strQStrParamValue.indexOf('&') != -1)
                strQStrParamValue = strQStrParamValue.substr(0, strQStrParamValue.indexOf('&'));
            return strQStrParamValue;
        }
        else
        {
            strQStrParamValue = defaultHiddenFieldNameValue;
            return strQStrParamValue;
        }
    }
    else
    {
        strQStrParamValue = defaultHiddenFieldNameValue;
        return strQStrParamValue;
    }
}

//repeat for each field to populate
$('form.regform .source').attr('value', getQueryStringParamValue('utm_source'));
$('form.regform .medium').attr('value', getQueryStringParamValue('utm_medium'));
$('form.regform .term').attr('value', getQueryStringParamValue('utm_term'));
$('form.regform .content').attr('value', getQueryStringParamValue('utm_content'));
$('form.regform .campaign').attr('value', getQueryStringParamValue('utm_campaign'));  function ajaxClass()
{       
    var trigger = false;

    function make_ajax_request (options) {

        //ajaxdata, callback, holder
        var results = '';

        $(options.holder).append('<div class="loader">LOADING SESSIONS</div>');

        $.post(siteurl+'/wp-admin/admin-ajax.php', options.ajaxdata, function(data) {
            $('body '+options.holder+' .loader').remove();
            results = $.parseJSON(data);            

        }).fail(function() {

            console.log('failed');

        }).always(function() {

            if(results.html == '')
                results.html = 'No Sessions Found. Please Try Another Filter';
            
            $(options.holder).append(results.html);
            
            if (results.props.maxpages == options.ajaxdata.pagenum || results.props.maxpages == 0)
            return;

            trigger = false;
            monitor_scroll(results, options);

        });
    }

    function monitor_scroll(results, options) {
        
        var loading_div = $('body '+options.holder);
        loading_offset = loading_div.offset().top + loading_div.height();

        $( window ).scroll(function() {
            var bottom_screen = $(this).scrollTop() + $(this).height();
            if (bottom_screen >= loading_offset && !trigger) {
                trigger = true;
                options.ajaxdata.pagenum += 1;
                make_ajax_request(options);         
            }
        })
    }

    function set_filter_options(options, callback) {

        var types = new Array(),
            markets = new Array(),
            dates = new Array(),
            times = new Array(),
            locations = new Array(),
            jobroles = new Array(),
            devcontracks = new Array(),
            bbworldtracks = new Array(),
            experience = new Array(),
            types_string = '',
            markets_string = '',
            dates_string = '',
            times_string = '',
            locations_string = '',
            jobroles_string = '',
            devcontracks_string = '',
            bbworldtracks_string = '',
            experience_string = '',
            subtitle_string = 'Showing All ',
            state_query = '?';


        $('.session-filter-options.types input:checked').each(function (e) {
            types.push($(this).val());
            types_string += encodeURIComponent($(this).data('name')) + 's';

            state_query += 'type[]='+$(this).data('name')+'&';

            if ($('.session-filter-options.types input:checked').length == 1 || e == $('.session-filter-options.types input:checked').length - 1) {
                types_string += ' ';
            }
            else {
                 types_string += ' & ';
            }

        })
        $('.session-filter-options.markets input:checked').each(function (e) {
            markets.push($(this).val());
            markets_string += $(this).data('name');

            state_query += 'market[]='+encodeURIComponent($(this).data('name'))+'&';

            if ($('.session-filter-options.markets input:checked').length == 1 || e == $('.session-filter-options.markets input:checked').length - 1) {
                markets_string += ' ';
            }
            else {
                markets_string += ' & ';
            }
        })
        $('.session-filter-options.dates input:checked').each(function (e) {
            dates.push($(this).val());
            dates_string += $(this).data('name');

            state_query += 'date[]='+encodeURIComponent($(this).data('name'))+'&';

            if ($('.session-filter-options.dates input:checked').length == 1 || e == $('.session-filter-options.dates input:checked').length - 1) {
                dates_string += ' ';
            }
            else {
                dates_string += ' & ';
            }
        })
        $('.session-filter-options.times input:checked').each(function (e) {
            times.push($(this).val());
            times_string += $(this).data('name');

            state_query += 'date[]='+encodeURIComponent($(this).data('name'))+'&';

            if ($('.session-filter-options.times input:checked').length == 1 || e == $('.session-filter-options.times input:checked').length - 1) {
                times_string += ' ';
            }
            else {
                times_string += ' & ';
            }
        })

        $('.session-filter-options.locations input:checked').each(function (e) {
            locations.push($(this).val());
            locations_string += $(this).data('name');

            state_query += 'date[]='+encodeURIComponent($(this).data('name'))+'&';

            if ($('.session-filter-options.locations input:checked').length == 1 || e == $('.session-filter-options.locations input:checked').length - 1) {
                locations_string += ' ';
            }
            else {
                locations_string += ' & ';
            }
        })

        $('.session-filter-options.jobroles input:checked').each(function (e) {
            jobroles.push($(this).val());
            jobroles_string += $(this).data('name');

            state_query += 'job[]='+encodeURIComponent($(this).data('name'))+'&';

            if ($('.session-filter-options.jobroles input:checked').length == 1 || e == $('.session-filter-options.jobroles input:checked').length - 1) {
                jobroles_string += ' ';
            }
            else {
                jobroles_string += ' & ';
            }
        })
        $('.session-filter-options.devcontracks input:checked').each(function (e) {
            devcontracks.push($(this).val());
            devcontracks_string += $(this).data('name');

            state_query += 'devcontrack[]='+encodeURIComponent($(this).data('name'))+'&';

            if ($('.session-filter-options.devcontracks input:checked').length == 1 || e == $('.session-filter-options.devcontracks input:checked').length - 1) {
                devcontracks_string += ' ';
            }
            else {
                devcontracks_string += ' & ';
            }
        })
        $('.session-filter-options.bbworldtracks input:checked').each(function (e) {
            bbworldtracks.push($(this).val());
            bbworldtracks_string += $(this).data('name');

            state_query += 'bbworldtrack[]='+encodeURIComponent($(this).data('name'))+'&';

            if ($('.session-filter-options.bbworldtracks input:checked').length == 1 || e == $('.session-filter-options.bbworldtracks input:checked').length - 1) {
                bbworldtracks_string += ' ';
            }
            else {
                bbworldtracks_string += ' & ';
            }
        })
        $('.session-filter-options.experience input:checked').each(function (e) {
            experience.push($(this).val());
            experience_string += $(this).data('name');

            state_query += 'exp[]='+encodeURIComponent($(this).data('name'))+'&';

            if ($('.session-filter-options.experience input:checked').length == 1 || e == $('.session-filter-options.experience input:checked').length - 1) {
                experience_string += ' ';
            }
            else {
                experience_string += ' & ';
            }
        })


        if (types_string == '' && markets_string == '' && dates_string == '' && times_string == '' && locations_string == '' && jobroles_string == '' && bbworldtracks_string == '' && experience_string == '' && options.template == 'bbworld') {
            subtitle_string = 'Showing All Sessions';
        }

        else {
            if (types_string !== '') {
                subtitle_string += types_string;
            }
            if (markets_string !== '') {
                subtitle_string += 'In '+markets_string;
            }
            if (dates_string !== '') {
                subtitle_string += 'On '+dates_string;
            }
            if (times_string !== '') {
                subtitle_string += 'At '+times_string;
            }
            if (locations_string !== '') {
                subtitle_string += 'In Room '+locations_string;
            }
            if (jobroles_string !== '') {
                subtitle_string += 'For '+jobroles_string;
            }
            if (devcontracks_string !== '') {
                subtitle_string += 'In The '+devcontracks_string+' Track ';
            }
            if (bbworldtracks_string !== '') {
                subtitle_string += 'In The "'+bbworldtracks_string+'" Theme ';
            }
            if (experience_string !== '') {
                subtitle_string += 'With '+experience_string+' Level';
            }
        }

        $('.showing-subtitle p').html(decodeURIComponent(subtitle_string));

        var stateObj = {};

        if (state_query != '?')
            history.replaceState(stateObj, "", state_query);


        options.ajaxdata.pagenum = 1;
        options.ajaxdata.template = options.template;
        options.ajaxdata.types = types;
        options.ajaxdata.markets = markets;
        options.ajaxdata.dates = dates;
        options.ajaxdata.times = times;
        options.ajaxdata.locations = locations;
        options.ajaxdata.jobroles = jobroles;
        options.ajaxdata.devcontracks = devcontracks;
        options.ajaxdata.bbworldtracks = bbworldtracks;
        options.ajaxdata.experience = experience;

        $(options.holder).html('');
        make_ajax_request(options);

        if (typeof callback == 'function')
            callback();

    }


    function enable_filtering(options) {

        $('.session-filter-title').on('click', function () {
            $(this).parent().find('.session-filter-options').toggleClass('hidden');
        })

        $('.session-filter-options input').on('change', function () {
            set_filter_options(options, function () {
                $('html,body').animate({
                    scrollTop: $('.showing-subtitle').offset().top - 60
                }, 500);
            });

        })
    }

    function enable_expanding()
    {
         filter_offset = $('.session-filters').offset().top - 80;

        $( window ).scroll(function() {
            var top_screen = $(this).scrollTop(),
                bottom_screen = $(this).scrollTop() + $(window).height(),
                session_wrapper_offset = ($('.session-wrapper').offset().top + 80) + $('.session-wrapper').height(); 

            if (top_screen >= filter_offset && bottom_screen <= session_wrapper_offset) {
                $('.session-filters').css({
                    'position' : 'fixed',
                    'top' : '80px'
                });
            }
            else if (top_screen <= filter_offset) {
                $('.session-filters').css({
                    'position' : 'inherit',
                    'top' : ''
                });
            }
            else if (bottom_screen >= session_wrapper_offset) {
                $('.session-filters').css({
                    'position' : 'fixed',
                    'top' : -Math.abs(bottom_screen - session_wrapper_offset ) + 'px',
                    'height' : '100%'
                });
            }

        });
     
    }

    function enable_more_toggle()
    {
       
        $('body').on('click', '.more-toggle', function (e){
            e.preventDefault();
            var more_html = $(this).parent().next().html();
            $(this).parent().html(more_html);
        });
    }

    return {
        init : function (options) {
            enable_filtering(options);
            set_filter_options(options);
            enable_more_toggle();
        },
    }
}function authClass()
{       
    var user_meta = undefined,
        livestreamurl = '/bbworldlive/live-streaming/',
        is_chat = false;

    function init() {

        if (is_logged_in()) {

            //track the user view as an event
            var user_meta = get_user_meta();
            window.onload = function() {

                ga('gtm1.send', {
                  hitType: 'event',
                  eventCategory: 'bbwlive user log',
                  eventAction: window.location.href,
                  eventLabel: user_meta.id
                });
            }

            //set the logout button
            $('.top-header .top-register-btn, .menu-register-btn a').html('Log Out').addClass('top-logout-btn');

            //hide the reg stuff
            $('.sub-menu li:last-child, .reg_form, .loggedout').hide();

            //set the logout button click action
            $('body').on('click', '.top-logout-btn', function (e) {
                e.preventDefault();
                $.removeCookie('bbwluser', { path: '/' });
                var redirect = get_redirect();
                window.location.href = redirect;
            })

            var url = siteurl+'wp-content/themes/bb/quick/livestream_islive.txt';

            //check if its live streaming or not
            make_ajax_request(url, function (response) {
                if (response == '1') {
                    $('.sub-menu').prepend('<li><strong><a href="'+livestreamurl+'">Streaming Today</a></strong></li>');
                }
               
            })
        }

        
    }

    //this is used to redirect users away if they are not authenticated
    function gate (options, callback) {
        if (!is_logged_in()) {
  
            if (typeof options.ajaxdata.redirect_to == 'string') {
                redirect = get_redirect(options.ajaxdata.redirect_to)
            }
            else {
                redirect = get_redirect('');
            }
            window.location.href = redirect;
        }
        else {
            if (typeof callback == 'function') {
                callback();
            }
        }
    }
    

    function login (options) {
        
        //if the user is already logged in, redirect them to the right location
        if (is_logged_in()) {
            user_meta = get_user_meta();
            var redirect = get_redirect();
            window.location.href = redirect;
        }

        //when they click the login button
        $('#loginform').on('submit', function (e) {
            
            e.preventDefault();

            //get the serialized form data and merge it with options passed to this method
            var formdata = objectifyForm($(this).serializeArray());
            options.ajaxdata = mergeObjects(options.ajaxdata, formdata);

            options.ajaxdata.pwd = btoa(options.ajaxdata.pwd);

            var url = siteurl+'/wp-json/bbwlive/'+options.ajaxdata.action+'/'+options.ajaxdata.log+'/'+options.ajaxdata.pwd

            //call ajax to verify auth
            make_ajax_request(url, function (response) {

                if ((response == '"incorrect_password"') || (response == '"invalid_username"')) {
                    var error_message = get_error_message(response);
                    display_error_message(error_message);
                }

                //on success, set cookie, get user meta to determine redirect and then.... redirect
                else {

                    response = JSON.parse(response);

                    $.cookie('bbwluser', response.data.ID+"|"+response.data.user_login+"|"+response.data.user_type, { path: '/' });
                    user_meta = get_user_meta();

                    var redirect = get_redirect(options.ajaxdata.redirect_to);
                    //is this live now?
                    if (response.data.is_live == true && options.ajaxdata.redirect_to == undefined)
                        redirect = livestreamurl;

                    
                    window.location.href = redirect;
                }

            });

        })

    }

    function chat(postid) {

        var userdata = get_user_meta();
        var url = siteurl+'/wp-json/bbwlive/chat_init/'+postid+'/'+userdata.name+'/'+userdata.id

        make_ajax_request(url, function (response) {

            response = JSON.parse(response);

            //ok there is a chat window so we are live
            if (response != '') {
                is_chat = true;
                single_pollrefresher(postid)
            }

            $('.chat-wrapper').html(response);

        });
    }

    function poll(postid)
    {

        var url = siteurl+'wp-content/themes/bb/quick/livestream_archive.txt';

        make_ajax_request(url, function (response) {

            if (response !== $('.live-session-holder').html()) {
                $('.live-session-holder').html(response);
            }

        });

        setTimeout(function () { poll(postid) }, 20000);
    }

    function vcpoll(randstring)
    {

        var vcelem = $('.vc-live-session-holder.'+randstring);

        var year = vcelem.data('year');
        var sessions = vcelem.data('sessions');
        var url = siteurl+'wp-content/themes/bb/quick/livestream_vc_archive_'+year+'_'+sessions+'.txt';

        make_ajax_request(url, function (response) {

            if (response !== vcelem.html()) {
                vcelem.html(response);
            }

        });

        setTimeout(function () { vcpoll(randstring) }, 20000);
    }

    function single_pollrefresher(postid)
    {
  
        var url = siteurl+'wp-content/themes/bb/quick/livestream_single.txt';

        make_ajax_request(url, function (response) {

            if (response !== window.location.href && response !== false && is_chat == true) {
                window.location.href = response;
            }

        });

        setTimeout(function () { single_pollrefresher(postid) }, 20000);

    }

    //simple function for determining if the cookie is set
    function is_logged_in() {
        return $.cookie('bbwluser');
    }

    //take the cookie data and split it into a usable object
    function get_user_meta() {
        autharray = is_logged_in().split("|");
        user_meta = {
            'id' : autharray[0],
            'name' : autharray[1],
            'type' : autharray[2]
        }
        return user_meta;
    }

    //conditionals for where to redirect a user. this handles login and logout redirects
    function get_redirect(redirect) {

        //not logged in
        if (!is_logged_in() && redirect !== undefined) {
            return '/login/?redirect_to='+redirect;
        }
        else if (!is_logged_in()) {
            return '/login/';
        }

        //logged in
        if (typeof redirect !== "undefined") {
            return redirect;
        }
        else if (user_meta.type == 'Developer (July 25)') {
            return '/bbworldlive/';
        }
        else if (user_meta.type == 'Professional Education (July 25)') {
            return '/bbworldlive/professional-education/';
        }
        else if (user_meta.type == 'Corporate Training (July 12)') {
            return '/bbworldlive/professional-education/';
        }
        else if (user_meta.type == 'Blackboard Keynote (July 25)') {
            return '/bbworldlive/';
        }
        else if (user_meta.type == 'K-12 (July 26)') {
            return '/bbworldlive/k12/';
        }
        else if (user_meta.type == 'Higher Education (July 27)') {
            return '/bbworldlive/higher-education/';
        }
        else {
            return '/bbworldlive/';
        }

    }

    //this handles login error messages. can probably be reused for other error handling when the need arises. 
    function get_error_message(error) {
        if (error == '"invalid_username"' || error == '"incorrect_password"') {
            return '<p>Login Failed. Please Check Your Username and Password and Try Again.</p>';
        }
    }

    function display_error_message(error_message) {
        $('#loginform .error-holder').html(error_message);
    }


    function make_ajax_request (url, callback) {

        var timestamp = Date.now() / 1000 | 0;

        var iscallback = false;

        if (typeof callback == 'function')
            iscallback = true;

        $.get(url+'?cachebust='+timestamp, function(data) {

            if (iscallback)
                callback(data);
                 
        }, "text").fail(function() {

            if (iscallback)
                callback(false);

        }).always(function() {


        });
    }


    return {
        poll: function (postid) {
            poll(postid);
        },
        vcpoll: function (randstring) {
            vcpoll(randstring);
        },
        chat: function (postid) {
            chat(postid);
        },
        login : function (options) {
            login(options);
        },
        gate : function (options, callback) {
            gate(options, callback);
        },
        init : function () {
            init();
        }
    }
}

/**

 */

return {
    user: function(options) {
        //users.init(options);
    },
    ajax: {
    	init: function (options) {
    		ajaxClass().init(options);
    	}
    },
    auth: {
    	init: function () {
    		authClass().init();
    	},
    	gate: function (options, callback) {
    		authClass().gate(options, callback);
    	},
    	login: function (options) {
    		authClass().login(options);
    	},
        chat: function (postid) {
            authClass().chat(postid);
        },
        poll: function (postid) {
            authClass().poll(postid);
        },
        vcpoll: function(randstring) {
            authClass().vcpoll(randstring);
        }
    }


}
/**
 * This is the closing file in a series of concatenated files
 */

/**
 * IMPORTANT!
 * Do not place anything above this closing brace.
 * af-app-return.js returns an object for AgoraApp()
 */
} // closes AgoraApp()

window.bbApp = BBApp();

})(window, document, undefined, $);


/**
 * PLACE YOUR FOOTER CODE BELOW HERE
 */