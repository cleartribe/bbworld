<?php
    /*
    Template Name: BbWorld Live Password
    */
    class BB_Password_Template {

        public function __construct() 
        {
            $this->hooks();
            $this->content();
        }

        public function hooks()
        {
            add_action( 'wp_footer', array($this, 'footer_scripts') );
            add_action( 'wp_head', array($this, 'header_scripts') );
        }

        public function header_scripts()
        {
       
            ?>
            <script>
                var currenturl = window.location.pathname,
                    ajaxdata = { redirect_to : currenturl },
                    siteurl = "<?php echo site_url('/'); ?>";
            </script>

            <?php
        }


        public function footer_scripts()
        {
            ?>
            <script>

                bbApp.auth.init();
        
            </script>
            <?php 
        }

        public function content()
        {
            global $post, $bb_theme;

            $post_slug = $post->post_name;
            $post_ancestry = get_post_ancestors($post->ID);
            $poster = '';

            if (!empty($post_ancestry)) {
                $poster = get_post($post_ancestry[0]);
                $post_slug = $poster->post_name;
            }
            else {
                $poster = $post;
            }

            $bb_theme->header_html($post_slug);
            $bb_theme->top_header();
            $bb_theme->main_header($poster->post_name);


            if ( have_posts() ) {
                while ( have_posts() ) {the_post(); ?>
                    
                    <?php $bb_theme->hero_content($post->ID); ?> 

                    <div class="row">
                        <div class="small-12 medium-6 medium-centered columns">

                            <div class="form-wrapper">

                                <?php
                                
                                    if (isset($_GET['errors'])) {
                                        if ($_GET['errors'] == 'invalidcombo') {
                                            ?>
                                            <h3>The email address you just entered wasn't valid.</h3>
                                            <p>Check your email address and try again.</p>
                                            <?php
                                        }
                                        if ($_GET['errors'] == 'invalid_email') {
                                            ?>
                                            <h3>The email address you just entered isn't in our system.</h3>
                                            <p><a href="/bbworldlive/registration/">Sign up here</a> to get access to the live stream, or try another email address below and we will try to send you a link to reset your password.</p>
                                            <?php
                                        }
                                    }

                                    else if (isset($_GET['checkemail'])) {
                                        if ($_GET['checkemail'] == 'confirm') {
                                            echo '<h3>Success! Please Check Your Email For Your Login Information.</h3>';
                                        }
                                    }
                                    else if (isset($_GET['login']) && $_GET['login'] == 'invalidkey') {
                                          echo '<h3>This link is expired. Please enter your email address below to recieve another link to reset your password.</h3>';
                                    }

                                    else {
                                        ?>
                                            <h3>Forgot Your Username or Password?</h3>
                                            <p>Enter your email address and we'll send you your username and a link you can use to pick a new password.</p>
                                        <?php 
                                    }

                                    
                                ?>
                              
                                <form id="lostpasswordform" action="<?php echo wp_lostpassword_url(); ?>" method="post">
                                    <p class="form-row">
                                        <label for="user_login"><?php _e( 'Enter Email Address', 'personalize-login' ); ?>
                                        <input type="text" required name="user_login" id="user_login">
                                    </p>
                             
                                    <p class="lostpassword-submit">
                                        <input type="submit" name="submit" class="button small dark lostpassword-button"
                                               value="<?php _e( 'Reset Password', 'personalize-login' ); ?>"/>
                                    </p>
                                </form>
                            </div>
                        </div>
                    </div>

                    <?php $bb_theme->bottom_cta($post->ID); 
                } 
            } 

            $bb_theme->footer_html(false);

        }

    }

    $bb_password_template = new BB_Password_Template;

?>