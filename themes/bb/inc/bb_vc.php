<?php 
class BB_VC {

	public function __construct() {
		$this->hooks();
	}

	public function hooks()
    {
        add_action( 'vc_before_init', array($this, 'register_vc_params'));
        add_action ( 'init', array($this, 'register_vc_shortcodes'));
    }
    public function register_vc_shortcodes()
    {
        add_shortcode( 'exhibitors', array($this, 'exhibitors') );
        add_shortcode( 'live_sessions', array($this, 'live_sessions') );
        add_shortcode( 'latest_news', array($this, 'latest_news') );
        add_shortcode( 'tagboard', array($this, 'tagboard_shortcode'));
    }
    public function register_vc_params()
    {

        vc_map( array(
           "name" => __("Exhibitors"),
           "base" => "exhibitors",
           "category" => __('Content'),
           "params" => array(
                array(
                    "type" => "dropdown",
                    "holder" => "",
                    "class" => "",
                    "heading" => __("Which Level Of Exhibitors To Pull"),
                    "param_name" => "exhibitors",
                    "value" => array ('none' => 'none', 'Diamond Sponsors' => 'diamond-sponsor', 'Ruby Sponsors' => 'ruby-sponsor', 'Platinum Sponsors' => 'platinum-sponsor', 'Gold Sponsors' => 'gold-sponsor', 'Silver Sponsors' => 'silver-sponsor', 'Bronze Sponsors' => 'bronze-sponsor', 'Expo Sponsors' => 'expo-sponsor'),
                    "description" => __("This will grab all the exhibitors from the exhibitors post type")
                ),
            )
        ) );

        vc_map( array(
           "name" => __("Live Sessions"),
           "base" => "live_sessions",
           "category" => __('Content'),
           "params" => array(
                array(
                    "type" => "dropdown",
                    "holder" => "",
                    "class" => "",
                    "heading" => __("Which Sessions To Build"),
                    "param_name" => "sessions",
                    "value" => array ('none', 'k12', 'highered', 'proed', 'developers', 'keynote'),
                    "description" => __("This will grab all the live sessions from the live sessions post type")
                ),
                array(
                    "type" => "dropdown",
                    "holder" => "",
                    "class" => "",
                    "heading" => __("Which Year?"),
                    "param_name" => "year",
                    "value" => array ('2016', '2017'),
                    "description" => __("This will grab narrow them down by year")
                ),
            )
        ) );

       vc_map( array(
           "name" => __("Latest News"),
           "base" => "latest_news",
           "category" => __('Content')
        ) );

        vc_map( array(
           "name" => __("tagboard"),
           "base" => "tagboard",
           "category" => __('Content'),
           "params" => array(
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __( "TagBoard ID" ),
                    "param_name" => "id",
                    "value" => __( "WhyWorkAtTagboard/146433" ),
                    "description" => __( "Enter In the tagboard id" )
                ),
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __( "Desktop Post Limit" ),
                    "param_name" => "postlimit",
                    "value" => __( "50" ),
                    "description" => __( "Enter How Many Posts Will Show For Desktop" )
                ),   
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __( "Mobile Post Limit" ),
                    "param_name" => "mobilelimit",
                    "value" => __( "10" ),
                    "description" => __( "Enter How Many Posts Will Show For Mobile" )
                ),                 
            )
        ) );

    }


    public function exhibitors ($atts)
    {
        global $bb_theme;

        extract( shortcode_atts( array(
            'exhibitors' => 'This is the title',
        ), $atts ) );


        if ($exhibitors == 'diamond-sponsor' || $exhibitors == 'ruby-sponsor') {
            $terms = array( $exhibitors );
            $class = "large";
        }
        else if ($exhibitors == 'platinum-sponsor' || $exhibitors == 'gold-sponsor') {
            $terms = array( $exhibitors );
            $class = "medium";
        }
        else if ($exhibitors == 'bronze-sponsor' || $exhibitors == 'silver-sponsor') {
            $terms = array( $exhibitors );
            $class = "small";
        }
        else {
            $terms = array( $exhibitors );
            $class = "tiny";
        }
       
        
        $args = array(
            'post_type' => 'exhibitor',
            'posts_per_page' => -1,
            'tax_query' => array(
                array(
                    'taxonomy' => 'types',
                    'field'    => 'slug',
                    'terms'    => $terms,
                )
            )
        );

        $query = new WP_Query( $args );

        $html = '<div class="exhibitor-wrapper '.$class.'"><div class="vc_row wpb_row vc_inner vc_row-fluid centered">';

        $inc = 1;
      
        $vc_col = 'centered-list-item '.$class;

        foreach ($query->posts as $key => $post) {
            $post_title = $post->post_title;
            $post_content = $post->post_content;
            $post_id = $post->ID;

            $post_url = $post->guid;
            $thumb_url = get_field('featuredImage', $post_id);
            $thumb_types = array('jpg', 'png', 'gif');


            //dont show it if there isnt a valid thumbnail
            if (!$thumb_url || is_array($thumb_url) || !in_array(pathinfo($thumb_url, PATHINFO_EXTENSION), $thumb_types))
                continue;

            $booth = get_field('booth', $post_id);
            $end_divs = '';

            $html .= '<div class="wpb_column vc_column_container '.$vc_col.'">';
                $html .= '<div class="exhibitor" style="background:url('.$thumb_url.') center center #fff no-repeat; border:1px solid #e1e1e1; background-size:50%;">';
                    $html .= '<div class="inner">';
                        $html .= '<div class="title"><h3>'.$post_title.'</h3></div>';
                        
                        if (is_string($booth)) {
                            $html .= '<div class="info">Booth '.$booth.'</div>';
                        }

                        $html .= '<div class="link">';
                            $html .= '<a href="'.$post_url.'" class="button small">View More</a>';
                        $html .= '</div>';

                    $html .= '</div>';
                $html .= '</div>';
            $html .= '</div>';

            $html .= $end_divs;

            $inc++;

        }
        
        $html .= '</div></div>';

        return $html;

    }


    public function live_sessions ($atts)
    {

        extract( shortcode_atts( array(
            'sessions' => 'none',
            'year' => '2016'
        ), $atts ) );

        $randomstring = $this->randomString();

        $html = '<script>jQuery(function() {bbApp.auth.vcpoll("'.$randomstring.'");});</script>';
        $html .= '<section data-year="'.$year.'" data-sessions="'.$sessions.'" class="vc-live-session-holder '.$randomstring.'"></section>';

        return $html;

    }

    public function randomString($type = 'alnum', $len = 8)
    {
        // default
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        switch($type)
        {
            case 'basic'    : return mt_rand();
            case 'alnum'    : $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; break;
            case 'numeric'  : $pool = '0123456789'; break;
            case 'nozero'   : $pool = '123456789'; break;
            case 'alpha'    : $pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; break;
            // don't include lower-case, don't include similar chars like 0 & O or I, L, and 1
            case 'non-confuse'    : $pool = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789'; break;
            case 'unique'   : return md5(uniqid(mt_rand()));
            case 'md5'      : return md5(uniqid(mt_rand()));
        }

        $str = '';
        for ($i=0; $i < $len; $i++)
        {
            $str .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
        }

        return $str;
    }


    public function latest_news ($atts)
    {
        global $bb_theme;
       
        $args = array(
            'post_type' => 'post',
        );

        $query = new WP_Query( $args );

        $html = '<div class="vc_row wpb_row vc_inner vc_row-fluid">';

        $inc = 1;

        if($query->found_posts == 1) {
            $vc_col = 'vc_col-sm-12';
        }
        else {
            $vc_col = 'vc_col-sm-4';
        }

        foreach ($query->posts as $key => $post) {

            $post_title = $post->post_title;
            $post_excerpt = $this->custom_excerpt($post->post_content, get_the_excerpt($post));
            $post_id = $post->ID;
            $post_url = get_permalink( $post );
            $post_date = get_the_date( 'F j, Y', $post_id );

            $end_divs = '';


            if($inc % 3 == 0) {$end_divs = '</div><div class="vc_row wpb_row vc_inner vc_row-fluid">';}

            $html .= '<div class="wpb_column vc_column_container '.$vc_col.'">';
                $html .= '<div style="padding:20px;">';
         
                    $html .= '<h3><a href="'.$post_url.'">'.$post_title.'</a></h3>';
                    $html .= '<p><strong>Posted On: '.$post_date.'</strong><br />';
                    
                    $html .= '<p>'.$post_excerpt.'</p>';

                $html .= '</div>';
            $html .= '</div>';

            $html .= $end_divs;

            $inc++;

        }
        
        $html .= '</div>';

        return $html;

    }

    function custom_excerpt($text, $excerpt)
    {
        if ($excerpt) return $excerpt;

        $text = strip_shortcodes( $text );

        $text = apply_filters('the_content', $text);
        $text = str_replace(']]>', ']]&gt;', $text);
        $text = strip_tags($text);
        $excerpt_length = apply_filters('excerpt_length', 55);
        $excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
        $words = preg_split("/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
        if ( count($words) > $excerpt_length ) {
                array_pop($words);
                $text = implode(' ', $words);
                $text = $text . $excerpt_more;
        } else {
                $text = implode(' ', $words);
        }

        return apply_filters('wp_trim_excerpt', $text, $raw_excerpt);
    }


    public function tagboard_shortcode($atts) {
       extract(shortcode_atts(array(
          'id' => "WhyWorkAtTagboard/146433",
          'postlimit' => "50",
          'mobilelimit' => "10",
          'darkmode' => false,
          'fixedheight' => false,
          'autoLoad' => false,
       ), $atts));
        return '<div id="tagboard-embed"></div>
                <script>var tagboardOptions = {tagboard:"'.$id.'",postCount: "'.$postlimit.'",mobilePostCount:"'.$mobilelimit.'",darkMode:"'.$darkmode.'",fixedHeight:"'.$fixedheight.'",autoLoad:"'.$autoLoad.'"};</script>
                <script src="https://tagboard.com/public/js/embed.js"></script>';
    }



} 