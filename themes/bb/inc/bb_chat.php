<?php 

	use Iflylabs\iFlyChat;

	require WP_CONTENT_DIR.'/vendor/autoload.php'; // include the autoload.php present in your vendor folder.

	const APP_ID = '38c8642f-7ecb-4ba7-b8c5-50f9f896783b';
	const API_KEY = 'WsNpxGpzpNus1Ucvu7vFHEyvD-7dtyl2UF1stG-gbR4W51105';

class BB_Chat {

	public function __construct() {

		$this->hooks();
	}

	public function hooks()
	{	
    	//ajax auth
		add_action( 'rest_api_init', array ($this, 'register_routes'));
	}

	public function register_routes()
    {
        register_rest_route( 'bbwlive', '/chat_init/(?P<postid>[\s\S]+)/(?P<username>[\s\S]+)/(?P<userid>[\s\S]+)', array(
            'methods' => WP_REST_Server::ALLMETHODS,
            'callback' => array($this, 'ajax_chat_init'),
        ) );

    }


	public function ajax_chat_init($request)
	{
		global $bb_theme;

		$is_live = get_field('is_live', 'option');
		$current_post_id = get_field('current_live_session', 'option'); 
		$postid = $request['postid'];
		$username = $request['username'];
		$userid = $request['userid'];

		$user = get_userdata( $userid );
		$username = $user->data->user_login;

		if (!$is_live || $current_post_id !== (int)$postid) {
			return '';
		}

		$chatroom_id = $this->get_chatroom_id($postid);

		$iflychat = new iFlyChat(APP_ID, API_KEY, array('SHOW_POPUP_CHAT' => false));
		$is_admin = false;

		$admins = array('Mark-VMODERATOR', 'Daniel-MODERATOR', 'Jace-MODERATOR', 'Sara-Moderator', 'Vicki-MODERATOR', 'Katie-Moderator', 'Tim-MODERATOR', 'Knight-MODERATOR', 'Naveen-MODERATOR', 'Jon-Admin', 'Liz-ADMIN', 'liz.conn', 'DebboraW-MODERATOR', 'tjtate');

		if (in_array($username, $admins)) {
			$is_admin = true;
		}

		if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
			$username = 'BbWorldUser'.rand ( 1 , 2000 );
		}

        $user = array(
          'user_name' => $username, // string(required)
          'user_id' => $userid, //string (required)
          'is_admin' => $is_admin, // boolean (optional)
        );

        $iflychat->setUser($user);
        $iflychat_code = $iflychat->getHtmlCode();


        $html = $iflychat_code;

        $html .= '<a href="#" class="closed-captions-btn button small">View Closed Captions</a>';

        $html .= '<iframe class="streamtext" style="clear:both; float:left; width:100%; display:none;" src="https://www.streamtext.net/player?event=BbWorldLive&amp;ff=Verdana,sans-serif&amp;fs=20&amp;fgc=ffffff&amp;bgc=000000&amp;spacing=2&amp;header=false&amp;controls=false&amp;footer=false&amp;chat=false" style="height: 150px; width: 100%;" scrolling="no" frameborder="0" marginheight="0px" marginwidth="0px"></iframe>';

        $html .= '<div class="iflychat-embed" data-room-id="'.$chatroom_id.'" data-height="600px" data-width="100%"></div>';

        return $html;
	}

	private function get_chatroom_id($postid)
	{

		$terms = wp_get_post_terms( $postid, 'area' );

		foreach ($terms as $term) {
			if ($term->name == 'highered') {
				return '1';
			}
			else if ($term->name == 'k12') {
				return '2';
			}
			else if ($term->name == 'proed') {
				return '3';
			}
			else if ($term->name == 'developers') {
				return '4';
			}
			else {
				return '1';
			}
		}
	}


} 