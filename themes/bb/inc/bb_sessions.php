<?php 
class BB_Sessions {


	public function __construct() {
		
		$this->hooks();
	}

	public function hooks()
	{	

		//ajax enablers
		add_action( 'wp_ajax_get_sessions', array( $this, 'ajax_get_posts' ) );
    	add_action( 'wp_ajax_nopriv_get_sessions', array( $this, 'ajax_get_posts' ) );
	}

	//ajax methods
	public function ajax_get_posts () {

		$session_args = array(
            'post_type' => array( 'session' )
        );

		$session_args['posts_per_page'] = 20;
		$session_args['paged'] = $_POST['pagenum'];
		$session_args['orderby'] = 'title';
		$session_args['order'] = 'ASC';

		$special_taxonomies = array();

		$is_bbworld = true;

		if (isset($_POST['template']) && $_POST['template'] == 'devcon') {
			$is_bbworld = false;
			$taxonomy = 'devcontracks';
			$taxonomy_terms = get_terms( $taxonomy, array(
			    'hide_empty' => 0,
			    'fields' => 'ids'
			) );

			// Use the new tax_query WP_Query argument (as of 3.1)
			array_push ($special_taxonomies, array (
	            'taxonomy' => $taxonomy,
	            'field' => 'id',
	            'terms' => $taxonomy_terms,
	        ));

	        if (isset($_POST['experience'])) {
				array_push ($special_taxonomies, array(
					'taxonomy'    => 'experience',
					'field'    => 'term_id',
					'terms'    => $_POST['experience'],
					'operator' => 'IN',
				));
			}
	
		}
		else {
			$taxonomy = 'devcontracks';
			$taxonomy_terms = get_terms( $taxonomy, array(
			    'hide_empty' => 0,
			    'fields' => 'ids'
			) );

			// Use the new tax_query WP_Query argument (as of 3.1)
			array_push ($special_taxonomies, array (
	            'taxonomy' => $taxonomy,
	            'field' => 'id',
	            'terms' => $taxonomy_terms,
	            'operator' => 'NOT IN',
	        ));
		}


		if (isset($_POST['types'])) {
			array_push ($special_taxonomies, array(
				'taxonomy'    => 'sessiontypes',
				'field'    => 'term_id',
				'terms'    => $_POST['types'],
				'operator' => 'IN',
			));
		}

		if (isset($_POST['dates'])) {
			array_push ($special_taxonomies, array(
				'taxonomy'    => 'dates',
				'field'    => 'term_id',
				'terms'    => $_POST['dates'],
				'operator' => 'IN',
			));
		}

		if (isset($_POST['times'])) {
			array_push ($special_taxonomies, array(
				'taxonomy'    => 'times',
				'field'    => 'term_id',
				'terms'    => $_POST['times'],
				'operator' => 'IN',
			));
		}

		/*if (isset($_POST['locations'])) {
			array_push ($special_taxonomies, array(
				'taxonomy'    => 'locations',
				'field'    => 'term_id',
				'terms'    => $_POST['locations'],
				'operator' => 'IN',
			));
		}*/

		if (isset($_POST['markets'])) {
			array_push ($special_taxonomies, array(
				'taxonomy'    => 'markets',
				'field'    => 'term_id',
				'terms'    => $_POST['markets'],
				'operator' => 'IN',
			));
		}

		if (isset($_POST['jobroles'])) {
			array_push ($special_taxonomies, array(
				'taxonomy'    => 'jobroles',
				'field'    => 'term_id',
				'terms'    => $_POST['jobroles'],
				'operator' => 'IN',
			));
		}

		if (isset($_POST['devcontracks'])) {
			array_push ($special_taxonomies, array(
				'taxonomy'    => 'devcontracks',
				'field'    => 'term_id',
				'terms'    => $_POST['devcontracks'],
				'operator' => 'IN',
			));
		}
		if (isset($_POST['bbworldtracks'])) {
			array_push ($special_taxonomies, array(
				'taxonomy'    => 'bbworldtracks',
				'field'    => 'term_id',
				'terms'    => $_POST['bbworldtracks'],
				'operator' => 'IN',
			));
		}

		if (!empty($special_taxonomies)) {
			$special_taxonomies['relation'] = 'AND';
			$session_args['tax_query'] = $special_taxonomies;
		}

        $sessions_query = new WP_Query( $session_args );

    	$html = '';

        foreach ($sessions_query->posts as $session) {
        
            $speaker_meta = get_post_meta( $session->ID, 'speaker_meta', true );
            if ($is_bbworld) {
            	$session_terms = wp_get_post_terms( $session->ID, array('types', 'markets', 'dates', 'times', 'bbworldtracks') );
            }
            else {
            	$session_terms = wp_get_post_terms( $session->ID, array('types', 'dates', 'times', 'experience', 'devcontracks') );
            }

            $html .= '<div class="session-single">';
                $html .= '<div class="session-title">';
                    $html .= '<h2>'.$session->post_title.'</h2>';
                $html .= '</div>';

                $html .= '<div class="session-taxonomies">';
                	$html .= '<ul class="inline-list">';

                		//for all priorities
                		foreach ($session_terms as $key => $session_term) {
                			$tax_class = "";
                			if ($session_term->taxonomy == 'dates') {
                				$tax_name = 'Date';
                				$tax_class = "highlight";
                				$html .= '<li><span class="pill '.$tax_class.'">'.$tax_name.': '.$session_term->name.'</span></li>';
                			}
                			else if ($session_term->taxonomy == 'times') {
                				$tax_name = 'Time';
                				$tax_class = "highlight";
                				$html .= '<li><span class="pill '.$tax_class.'">'.$tax_name.': '.$session_term->name.'</span></li>';
                			}
                			
                		}

                		//lesser priority
                		foreach ($session_terms as $key => $session_term) {
                			$tax_class = "";
                			if ($session_term->taxonomy == 'markets') {
                				$tax_name = 'Market';
                				$html .= '<li><span class="pill '.$tax_class.'">'.$tax_name.': '.$session_term->name.'</span></li>';
                			}
                			else if ($session_term->taxonomy == 'types') {
                				$tax_name = 'Type';
                				$html .= '<li><span class="pill '.$tax_class.'">'.$tax_name.': '.$session_term->name.'</span></li>';
                			}
                			else if ($session_term->taxonomy == 'devcontracks') {
                				$tax_name = 'Track';
                				$html .= '<li><span class="pill '.$tax_class.'">'.$tax_name.': '.$session_term->name.'</span></li>';
                			}
                			else if ($session_term->taxonomy == 'bbworldtracks') {
                				$tax_name = 'Theme';
                				$html .= '<li><span class="pill '.$tax_class.'">'.$tax_name.': '.$session_term->name.'</span></li>';
                			}
                			else if ($session_term->taxonomy == 'experience') {
                				$tax_name = 'Experience Level';
                				$html .= '<li><span class="pill '.$tax_class.'">'.$tax_name.': '.$session_term->name.'</span></li>';
                			}

                			
                		}
                	$html .= '</ul>';
                $html .= '</div> ';

                $html .= '<div class="session-meta post-meta">';
                    $html .= '<p>'.$speaker_meta.'</p>';
                $html .= '</div>';
                $html .= '<div class="session-description">';

                $session->post_content = utf8_decode($session->post_content);

                	$word_count = str_word_count(strip_tags($session->post_content, 0));
					if ($word_count > 55) {
						$html .= wp_trim_words($session->post_content, $num_words = 55);
						$html .= '<a class="more-toggle">Show More</a>';
					}
					else {
						$html .= $session->post_content;
					}
                   	
                $html .= '</div>';
             	$html .= '<div class="session-full no-display">';
                	$html .= wpautop($session->post_content);
                $html .= '</div>';
            $html .= '</div>';
           
        }

        //stash results
		$results = array(
			'props' => array(
				'perpage' => $sessions_query->query_vars['posts_per_page'],
				'totalnum' => $sessions_query->found_posts,
				'maxpages' => $sessions_query->max_num_pages,
				'curpage' => $sessions_query->query_vars['paged'],
			),
			'html' => $html
		);

		echo json_encode($results);
		exit;

	}

} 