<?php 
class BB_Lanyon {

	private $apitoken;
	
	public function __construct() {
		$this->apitoken = 'htCScP7l1e2wLGbYabcVMk1Zp0jffaUY';

		//add_action ('init', array($this, 'run_exhibitor_updater'));
		//add_action( 'init', array($this, 'run_session_updater'));
		//$this->schedule_session_updater();
		//$this->schedule_exhibitor_updater();
	}

	//this handles when to call the "run" for updating sessions
	//it will only do it once a day as a background process
	public function schedule_session_updater()
	{
		//check to see if its scheduled
		$timestamp = wp_next_scheduled( 'daily_session_updater' );

		//we need to schedule it if there isnt a timestamp present 
		if( $timestamp == false ){
			wp_schedule_event( time(), 'daily', 'daily_session_updater');	
		}
		//we need to make sure it can run when the time comes
		add_action( 'daily_session_updater', array($this, 'run_session_updater' ) );
	}

	public function schedule_exhibitor_updater()
	{
		//check to see if its scheduled
		$timestamp = wp_next_scheduled( 'daily_exhibitor_updater' );

		//we need to schedule it if there isnt a timestamp present 
		if( $timestamp == false ){
			wp_schedule_event( time(), 'daily', 'daily_exhibitor_updater');	
		}
		//we need to make sure it can run when the time comes
		add_action( 'daily_exhibitor_updater', array($this, 'run_exhibitor_updater' ) );
	}

	//this the main process to update the sessions
	public function run_session_updater()
	{

		//ok this is intense. lets set this up to do it correctly. 
		set_time_limit(240);
		ini_set('memory_limit','256M');

		//remove all current sessions in wordpress
		$this->delete_wordpress_sessions();

		//retreive all the session data from the lanyon api
		$sessions = $this->get_sessions_from_lanyon();

		//add all the sessions to wordpress
		$this->add_sessions_from_lanyon($sessions);

	}


	//this the main process to update the exhibitors
	public function run_exhibitor_updater()
	{

		//ok this is intense. lets set this up to do it correctly. 
		set_time_limit(240);
		ini_set('memory_limit','256M');

		//remove all current exhibitors in wordpress
		$this->delete_wordpress_exhibitors();

		//retreive all the exhibitor data from the lanyon api
		$exhibitors = $this->get_exhibitors_from_lanyon();

		//add all the exhibitors to wordpress
		$this->add_exhibitors_from_lanyon($exhibitors);

	}

	//get all session posts and remove them 1 by 1
	private function delete_wordpress_sessions()
	{

		$args = array(
			'post_status' => 'publish', 
			'post_type' => 'session', 
			'posts_per_page' => -1, 
		);

		$all_sessions = new WP_Query( $args );

		foreach ($all_sessions->posts as $session) {

			wp_delete_post( $session->ID, true );
		}
	}

	//get all exhibitor posts and remove them 1 by 1
	private function delete_wordpress_exhibitors()
	{

		$args = array(
			'post_status' => 'publish', 
			'post_type' => 'exhibitor', 
			'posts_per_page' => -1, 
		);

		$all_exhibitors = new WP_Query( $args );

		foreach ($all_exhibitors->posts as $exhibitor) {

			wp_delete_post( $exhibitor->ID, true );
		}
	}

	//actually add the session posts and assocaiated taxonomy refferences and metadata to wordpress
	private function add_sessions_from_lanyon($sessions)
	{
		foreach ($sessions as $key => $session) {

			//update speakers
			$speaker_meta = '';

			//if there are multiple speakers
			if (is_array($session->speaker) && $session->type !== 'Rocket Session') {
				$speaker_meta .= 'Presented By: ';
				foreach ($session->speaker as $keyz => $speaker) {
					$amp = ($keyz == count($session->speaker) - 1 ? '': '| ');
					$speaker_meta .= $this->speaker_html($speaker, $amp);
				}
			}
			else if ($session->type == 'Rocket Session') {
				$speaker_meta .= 'Multiple Presenters';
			}
			else if ($session->speaker != '') {
				$speaker_meta .= 'Presented By: ';
				$speaker_meta .= $this->speaker_html($session->speaker);
			}

			//set the wp terms to the session object
			$session = $this->set_session_terms($session);
		
			//An array of IDs of taxonomies we want this post to have.
			$markets = (is_string($session->{'AudienceMarket'}) ? (int) $session->{'AudienceMarket'} : $session->{'AudienceMarket'});
			$jobroles = (is_string($session->{'Top-Two-Role-Beneficiaries'}) ? (int) $session->{'Top-Two-Role-Beneficiaries'} : $session->{'Top-Two-Role-Beneficiaries'});
			$types = (is_string($session->type) ? (int) $session->type : $session->type);
			$dates = (isset($session->{'sessionTime'}->date) ? (int) $session->{'sessionTime'}->date : null);
			$times = (isset($session->{'sessionTime'}->time) ? (int) $session->{'sessionTime'}->time : null);
			$locations = (isset($session->{'sessionTime'}->room) ? (int) $session->{'sessionTime'}->room : null);
			$devcontracks = (isset($session->{'DevCon-Track'}) && is_string($session->{'DevCon-Track'}) ? (int) $session->{'DevCon-Track'} : $session->{'DevCon-Track'});
			$bbworldtracks = (isset($session->{'BbWorld-Program-Theme'}) && is_string($session->{'BbWorld-Program-Theme'}) ? (int) $session->{'BbWorld-Program-Theme'} : $session->{'BbWorld-Program-Theme'});
			$experience = (isset($session->{'DevCon-Experience-Level'}) && is_string($session->{'DevCon-Experience-Level'}) ? (int) $session->{'DevCon-Experience-Level'} : $session->{'DevCon-Experience-Level'});

			//assemble post data to be inserted
			$session_data = array(
			  'post_title'     => $session->title,
			  'post_content'   => $session->abstract,
			  'post_status'    => 'publish',
			  'post_type'      => 'session'
			); 

			//insert the post
			$session_wp_id = wp_insert_post( $session_data );

			//assign terms to newly added post
			wp_set_object_terms( $session_wp_id, $markets, 'markets' );
			wp_set_object_terms( $session_wp_id, $jobroles, 'jobroles' );
			wp_set_object_terms( $session_wp_id, $types, 'sessiontypes' );
			wp_set_object_terms( $session_wp_id, $dates, 'dates' );
			wp_set_object_terms( $session_wp_id, $times, 'times' );
			wp_set_object_terms( $session_wp_id, $locations, 'locations' );
			wp_set_object_terms( $session_wp_id, $experience, 'experience' );
			wp_set_object_terms( $session_wp_id, $devcontracks, 'devcontracks' );
			wp_set_object_terms( $session_wp_id, $bbworldtracks, 'bbworldtracks' );

			//update post meta
			update_post_meta($session_wp_id, 'lanyon_id', $session->sessionID);
			update_post_meta($session_wp_id, 'speaker_meta', $speaker_meta);
			
		}


	}

	//actually add the exhibitor posts and associated taxonomy references and metadata to wordpress
	private function add_exhibitors_from_lanyon($exhibitors)
	{
		foreach ($exhibitors as $key => $exhibitor) {


			//set the wp terms to the session object
			$exhibitor = $this->set_exhibitor_terms($exhibitor);

			//set the featured image
			$exhibitor = $this->set_exhibitor_featured_image($exhibitor);
		
			//An array of IDs of taxonomies we want this post to have.
			$types = (is_string($exhibitor->type) ? (int) $exhibitor->type : $exhibitor->type);


			if (is_object($exhibitor->description)) {
				$exhibitor->description = ' ';
			}

			//assemble post data to be inserted
			$exhibitor_data = array(
			  'post_title'     => $exhibitor->name,
			  'post_content'   => $exhibitor->description,
			  'post_status'    => 'publish',
			  'post_type'      => 'exhibitor'
			); 

			//insert the post
			$exhibitor_wp_id = wp_insert_post( $exhibitor_data );

			//assign terms to newly added post
			wp_set_object_terms( $exhibitor_wp_id, $types, 'types' );
		
			//update post meta
			update_post_meta($exhibitor_wp_id, 'exhibitorID', $exhibitor->exhibitorID);
			update_post_meta($exhibitor_wp_id, 'url', $exhibitor->url);
			update_post_meta($exhibitor_wp_id, 'email', $exhibitor->email);
			update_post_meta($exhibitor_wp_id, 'booth', $exhibitor->booth);

			if (isset($exhibitor->exhibitorFile) && is_object($exhibitor->exhibitorFile)) {
				$exhibitor->exhibitorFile = $exhibitor->exhibitorFile->url;
			}

			if (isset($exhibitor->exhibitorFile)){
				update_post_meta($exhibitor_wp_id, 'featuredImage', $exhibitor->exhibitorFile);
			}
			
		}

	}

	//check to see if the speaker 
	private function speaker_html($speaker, $amp = '') 
	{
		$speaker_meta = (isset($speaker->fullName) && !is_object($speaker->fullName) ? $speaker->fullName . ' ' : '');
		$speaker_meta .= (isset($speaker->jobTitle) && !is_object($speaker->jobTitle) ? $speaker->jobTitle . ' ' : '');
		$speaker_meta .= (isset($speaker->company) && !is_object($speaker->company) ? 'at ' . $speaker->company . ' ' . $amp : '');
		return $speaker_meta;
	}

	//if there are terms for a particular taxonomy that do not exist in wordpress
	//lets grab that term, insert into wordpress and make sure we update the lanyon session object 
	//so we can insert a term relationship at a later date
	private function set_session_terms($session)
	{	

		//market terms
		if (isset($session->{'AudienceMarket'})) {

			//there are a bunch
			if (is_array($session->{'AudienceMarket'})) {
				foreach ($session->{'AudienceMarket'} as $keyz => $session_market) {
					$term_id = term_exists( $session_market);
					if (!$term_id) {
						$term_id = wp_insert_term($session_market, 'markets');
						$term_id = $term_id['term_id'];
					}
					$session->{'AudienceMarket'}[$keyz] = (int) $term_id;
				}
			}

			//mmm just one
			else {
				$term_id = term_exists( $session->{'AudienceMarket'});
				if (!$term_id) {
					$term_id = wp_insert_term((string)$session->{'AudienceMarket'}, 'markets');
					$term_id = $term_id['term_id'];
				}
				$session->{'AudienceMarket'} = $term_id;
			
			}
			
		}


		//date terms
		if (isset($session->{'sessionTime'}->date) && !empty($session->{'sessionTime'}->date)) {
			
			//reformat date
			$old_date_timestamp = strtotime($session->{'sessionTime'}->date);
			$new_date = date('F j, l', $old_date_timestamp); 



			$term_id = term_exists( $new_date );
			if (!$term_id) {

				$term_id = wp_insert_term($new_date, 'dates');

				$term_id = $term_id['term_id'];

			}
			
			$session->{'sessionTime'}->date = $term_id;
		
			
		}

		//time terms
		if (isset($session->{'sessionTime'}->time) && !empty($session->{'sessionTime'}->time)) {
			

			//reformat time
			$time_in_12_hour_format = date("g:i a", strtotime($session->{'sessionTime'}->time));
			$new_time = $time_in_12_hour_format;


			$term_id = term_exists( $new_time );
			if (!$term_id) {

				$term_id = wp_insert_term($new_time, 'times');

				$term_id = $term_id['term_id'];

			}

			
			$session->{'sessionTime'}->time = $term_id;
		
			
		}

		//room #
		if (isset($session->{'sessionTime'}->room) && !empty($session->{'sessionTime'}->room)) {
			

			$term_id = term_exists( $session->{'sessionTime'}->room );
			if (!$term_id) {

				$term_id = wp_insert_term($session->{'sessionTime'}->room, 'locations');

				$term_id = $term_id['term_id'];

			}

			
			$session->{'sessionTime'}->room = $term_id;
					
		}


		//job role terms
		if (isset($session->{'Top-Two-Role-Beneficiaries'})) {

			//there are a bunch
			if (is_array($session->{'Top-Two-Role-Beneficiaries'})) {
				foreach ($session->{'Top-Two-Role-Beneficiaries'} as $keyz => $session_market) {
					$term_id = term_exists( $session_market);
					if (!$term_id) {
						$term_id = wp_insert_term($session_market, 'jobroles');
						$term_id = $term_id['term_id'];
					}
					$session->{'Top-Two-Role-Beneficiaries'}[$keyz] = (int) $term_id;
				}
			}

			//mmm just one
			else {
				$term_id = term_exists( $session->{'Top-Two-Role-Beneficiaries'});
				if (!$term_id) {
					$term_id = wp_insert_term($session->{'Top-Two-Role-Beneficiaries'}, 'jobroles');
					$term_id = $term_id['term_id'];
				}
				$session->{'Top-Two-Role-Beneficiaries'} = $term_id;
			}	
		}


		//devcon track terms
		if (isset($session->{'DevCon-Track'})) {

			//there are a bunch
			if (is_array($session->{'DevCon-Track'})) {
				foreach ($session->{'DevCon-Track'} as $keyz => $session_market) {
					$term_id = term_exists( $session_market, 'devcontracks' );
					$term_id = $term_id['term_id'];

					if (!$term_id) {
						$term_id = wp_insert_term($session_market, 'devcontracks');
						$term_id = $term_id['term_id'];
					}
					$session->{'DevCon-Track'}[$keyz] = (int) $term_id;
				}
			}

			//mmm just one
			else {
				$term_id = term_exists( $session->{'DevCon-Track'});

				if (!$term_id) {
					$term_id = wp_insert_term($session->{'DevCon-Track'}, 'devcontracks');
					$term_id = $term_id['term_id'];
				}
				$session->{'DevCon-Track'} = $term_id;
			}	
		}

		//bbworld track terms
		if (isset($session->{'BbWorld-Program-Theme'})) {

			//there are a bunch
			if (is_array($session->{'BbWorld-Program-Theme'})) {
				foreach ($session->{'BbWorld-Program-Theme'} as $keyz => $session_market) {

					$term_id = term_exists( $session_market, 'bbworldtracks');
					$term_id = $term_id['term_id'];

					if (!isset($term_id)) {
						$term_id = wp_insert_term( $session_market, 'bbworldtracks');
						$term_id = $term_id['term_id'];
					}
					$session->{'BbWorld-Program-Theme'}[$keyz] = (int) $term_id;
				}
			}

			//mmm just one
			else {
				$term_id = term_exists( $session->{'BbWorld-Program-Theme'}, 'bbworldtracks');
				$term_id = $term_id['term_id'];

				if (!isset($term_id)) {
					$term_id = wp_insert_term($session->{'BbWorld-Program-Theme'}, 'bbworldtracks');
					$term_id = $term_id['term_id'];
				}
				$session->{'BbWorld-Program-Theme'} = $term_id;
			}	
		}

		//experience level terms
		if (isset($session->{'DevCon-Experience-Level'})) {

			//there are a bunch
			if (is_array($session->{'DevCon-Experience-Level'})) {
				foreach ($session->{'DevCon-Experience-Level'} as $keyz => $session_market) {

					$term_id = term_exists( $session_market, 'experience');
					$term_id = $term_id['term_id'];

					if (!isset($term_id)) {
						$term_id = wp_insert_term( $session_market, 'experience');
						$term_id = $term_id['term_id'];
					}
					$session->{'DevCon-Experience-Level'}[$keyz] = (int) $term_id;
				}
			}

			//mmm just one
			else {
				$term_id = term_exists( $session->{'DevCon-Experience-Level'}, 'experience');
				$term_id = $term_id['term_id'];

				if (!isset($term_id)) {
					$term_id = wp_insert_term($session->{'DevCon-Experience-Level'}, 'experience');
					$term_id = $term_id['term_id'];
				}
				$session->{'DevCon-Experience-Level'} = $term_id;
			}	
		}

		


		//type terms
		if (isset($session->type)) {

			$term_id = term_exists( $session->type );
			if (!$term_id) {
				$term_id = wp_insert_term($session->type, 'sessiontypes');
				$term_id = $term_id['term_id'];
			}
			$session->type = $term_id;
			
		}


		return $session;
		
	}


	private function set_exhibitor_terms($exhibitor)
	{	

		//type terms
		if (isset($exhibitor->type)) {

			//there are a bunch
			if (is_object($exhibitor->type)) {
				foreach ($exhibitor->type as $keyz => $session_market) {

					$term_id = term_exists( $exhibitor->type, 'types');
					$term_id = $term_id['term_id'];

					if (!isset($term_id)) {
						$term_id = wp_insert_term( $exhibitor->type, 'types');
						$term_id = $term_id['term_id'];
					}
					$exhibitor->type[$keyz] = (int) $term_id;
				}
			}
			else {
				$term_id = term_exists( $exhibitor->type );
				if (!$term_id) {
					$term_id = wp_insert_term((string)$exhibitor->type, 'types');
					$term_id = $term_id['term_id'];
				}
				$exhibitor->type = $term_id;
			}
			
		}

		return $exhibitor;
	}

	private function set_exhibitor_featured_image($exhibitor)
	{

		if (isset($exhibitor->exhibitorFile)) {
			foreach ($exhibitor->exhibitorFile as $exhibitorFile) {
				if (isset($exhibitorFile->type) && $exhibitorFile->type == 'Logo')
					$exhibitor->exhibitorFile = $exhibitorFile->url;
			}
		}

		return $exhibitor;
	}


	//lets connect to the lanyon api to get the session data and format it so its workable
	public function get_sessions_from_lanyon()
	{
		$arg = array ( 'method' => 'POST', 'timeout' => 45 );
	
		//getting all session data
		$response = wp_remote_request ( 'https://bbwo8qlt.smarteventscloud.com/api/sessionData.do?apiToken='.$this->apitoken, $arg );
		
		//convert to workable json object
		$xml = json_decode(json_encode(simplexml_load_string($response['body']) ) );

		$final_sessions = array();

		//push PUBLISHED sessions only to array to work with. all others will be left behind
		foreach ($xml->session as $session) {
			if (is_string($session->published)) {
				//bullshit formatting because of what we get from lanyon
				$session->abstract = utf8_encode($session->abstract);
				$session->abstract = str_replace("ï¿½", "'", $session->abstract);
				array_push($final_sessions, $session);
			}
		}

		return $final_sessions;
	}

	//lets connect to the lanyon api to get the session data and format it so its workable
	public function get_exhibitors_from_lanyon()
	{
		$arg = array ( 'method' => 'POST', 'timeout' => 45 );

		//getting all session data
		$response = wp_remote_request ( 'https://bbwo8qlt.smarteventscloud.com/api/exhibitorData.do?apiToken='.$this->apitoken, $arg );

		//convert to workable json object
		$xml = json_decode(json_encode(simplexml_load_string($response['body']) ) );

		$final_exhibitors = array();

		//push PUBLISHED sessions only to array to work with. all others will be left behind
		foreach ($xml->exhibitor as $exhibitor) {
			if ($exhibitor->status == 'Approved') {
				array_push($final_exhibitors, $exhibitor);
			}
		}


		return $final_exhibitors;
	}


	public function upload_featured_image($post_id, $image_url) 
	{
		$upload_dir = wp_upload_dir();
		$image_data = file_get_contents($image_url);
		$filename = basename($image_url);

		$filename = str_replace('%2B', '', $filename);

		if(wp_mkdir_p($upload_dir['path']))
		    $file = $upload_dir['path'] . '/' . $filename;
		else
		    $file = $upload_dir['basedir'] . '/' . $filename;
		file_put_contents($file, $image_data);

		$wp_filetype = wp_check_filetype($filename, null );
		$attachment = array(
		    'post_mime_type' => $wp_filetype['type'],
		    'post_title' => sanitize_file_name($filename),
		    'post_content' => '',
		    'post_status' => 'inherit'
		);
		$attach_id = wp_insert_attachment( $attachment, $file, $post_id );
		require_once(ABSPATH . 'wp-admin/includes/image.php');
		$attach_data = wp_generate_attachment_metadata( $attach_id, $file );
		wp_update_attachment_metadata( $attach_id, $attach_data );

		set_post_thumbnail( $post_id, $attach_id );
	}

}


 