<?php 
class BB_CPT {

	public function __construct() {
		$this->hooks();
	}

	public function hooks () {
       add_action( 'init', array( $this, 'exhibitor' ) );
       add_action( 'init', array( $this, 'session' ) );
       add_action( 'init', array( $this, 'live_session' ) );
   }


   public function exhibitor( $atts ) {
        $labels = array(
            'name'                => _x( 'Exhibitors', 'Post Type General Name', 'text_domain' ),
            'singular_name'       => _x( 'Exhibitor', 'Post Type Singular Name', 'text_domain' ),
            'menu_name'           => __( 'Exhibitors', 'text_domain' ),
            'all_items'           => __( 'All Exhibitors', 'text_domain' ),
            'view_item'           => __( 'View Exhibitor', 'text_domain' ),
            'add_new_item'        => __( 'Add New Exhibitor', 'text_domain' ),
            'add_new'             => __( 'New Exhibitor', 'text_domain' ),
            'edit_item'           => __( 'Edit Exhibitor', 'text_domain' ),
            'update_item'         => __( 'Update Exhibitor', 'text_domain' ),
            'search_items'        => __( 'Search Exhibitors', 'text_domain' ),
            'not_found'           => __( 'No Exhibitor found', 'text_domain' ),
            'not_found_in_trash'  => __( 'No Exhibitors found in Trash', 'text_domain' ),
        );
        $args = array(
            'label'               => __( 'exhibitors', 'text_domain' ),
            'description'         => __( 'Exhibitors', 'text_domain' ),
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'revisions', 'thumbnail', 'custom-fields' ),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => false,
            'show_in_admin_bar'   => true,
            'menu_position'       => 8,
            'menu_icon'           => 'dashicons-id',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => true,
            'publicly_queryable'  => true,
            'capability_type'     => 'post',
            'rewrite'       => array( 'slug' => 'exhibitor' )
        );

        register_post_type( 'exhibitor', $args );

    }


    public function session( $atts ) {
        $labels = array(
            'name'                => _x( 'Sessions', 'Post Type General Name', 'text_domain' ),
            'singular_name'       => _x( 'Session', 'Post Type Singular Name', 'text_domain' ),
            'menu_name'           => __( 'Sessions', 'text_domain' ),
            'all_items'           => __( 'All Sessions', 'text_domain' ),
            'view_item'           => __( 'View Session', 'text_domain' ),
            'add_new_item'        => __( 'Add New Session', 'text_domain' ),
            'add_new'             => __( 'New Session', 'text_domain' ),
            'edit_item'           => __( 'Edit Session', 'text_domain' ),
            'update_item'         => __( 'Update Session', 'text_domain' ),
            'search_items'        => __( 'Search Sessions', 'text_domain' ),
            'not_found'           => __( 'No Session found', 'text_domain' ),
            'not_found_in_trash'  => __( 'No Sessions found in Trash', 'text_domain' ),
        );
        $args = array(
            'label'               => __( 'sessions', 'text_domain' ),
            'description'         => __( 'Sessions', 'text_domain' ),
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'revisions', 'thumbnail'  ),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => false,
            'show_in_admin_bar'   => true,
            'menu_position'       => 8,
            'menu_icon'           => 'dashicons-id',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => true,
            'publicly_queryable'  => true,
            'capability_type'     => 'post',
            'rewrite'       => array( 'slug' => 'session' )
        );

        register_post_type( 'session', $args );

    }

    public function live_session( $atts ) {


        $labels = array(
            'name'                => _x( 'Live Sessions', 'Post Type General Name', 'text_domain' ),
            'singular_name'       => _x( 'Live Session', 'Post Type Singular Name', 'text_domain' ),
            'menu_name'           => __( 'Live Sessions', 'text_domain' ),
            'all_items'           => __( 'All Live Sessions', 'text_domain' ),
            'view_item'           => __( 'View Live Session', 'text_domain' ),
            'add_new_item'        => __( 'Add New Live Session', 'text_domain' ),
            'add_new'             => __( 'New Live Session', 'text_domain' ),
            'edit_item'           => __( 'Edit Live Session', 'text_domain' ),
            'update_item'         => __( 'Update Live Session', 'text_domain' ),
            'search_items'        => __( 'Search Live Sessions', 'text_domain' ),
            'not_found'           => __( 'No Live Session found', 'text_domain' ),
            'not_found_in_trash'  => __( 'No Live Sessions found in Trash', 'text_domain' ),
        );
        $args = array(
            'label'               => __( 'livesessions', 'text_domain' ),
            'description'         => __( 'Live Sessions', 'text_domain' ),
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'revisions', 'thumbnail' ),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 8,
            'menu_icon'           => 'dashicons-id',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => true,
            'publicly_queryable'  => true,
            'capability_type'     => 'page',
            'rewrite'       => array( 'slug' => 'livesession' )
        );

        register_post_type( 'livesession', $args );

    }


}



