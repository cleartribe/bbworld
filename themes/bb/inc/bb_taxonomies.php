<?php 
class BB_Taxonomies {

	public function __construct() {
		$this->hooks();
	}

	public function hooks () {
        add_action( 'init', array($this, 'sessiontypes') );
        add_action( 'init', array($this, 'types') );
        add_action( 'init', array($this, 'dates') );
        add_action( 'init', array($this, 'times') );
        add_action( 'init', array($this, 'locations') );
        add_action( 'init', array($this, 'jobroles') );
        add_action( 'init', array($this, 'experience') );
        add_action( 'init', array($this, 'markets') );
        add_action( 'init', array($this, 'devcontracks') );
        add_action( 'init', array($this, 'bbworldtracks') );
        add_action( 'init', array($this, 'area') );
        add_action( 'init', array($this, 'year') );
    }

    public function types () {

        $labels = array(
            'name'              => _x( 'Types', 'taxonomy general name' ),
            'singular_name'     => _x( 'Type', 'taxonomy singular name' ),
            'search_items'      => __( 'Search Types' ),
            'all_items'         => __( 'All Types' ),
            'parent_item'       => __( 'Parent Type' ),
            'parent_item_colon' => __( 'Parent Type:' ),
            'edit_item'         => __( 'Edit Type' ),
            'update_item'       => __( 'Update Type' ),
            'add_new_item'      => __( 'Add New Type' ),
            'new_item_name'     => __( 'New Type Name' ),
            'menu_name'         => __( 'Types' ),
        );

        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'type' ),
        );

        register_taxonomy( 'types', array( 'exhibitor' ), $args );
    }


     public function sessiontypes () {

        $labels = array(
            'name'              => _x( 'Types', 'taxonomy general name' ),
            'singular_name'     => _x( 'Type', 'taxonomy singular name' ),
            'search_items'      => __( 'Search Types' ),
            'all_items'         => __( 'All Types' ),
            'parent_item'       => __( 'Parent Type' ),
            'parent_item_colon' => __( 'Parent Type:' ),
            'edit_item'         => __( 'Edit Type' ),
            'update_item'       => __( 'Update Type' ),
            'add_new_item'      => __( 'Add New Type' ),
            'new_item_name'     => __( 'New Type Name' ),
            'menu_name'         => __( 'Types' ),
        );

        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'type' ),
        );

        register_taxonomy( 'sessiontypes', array( 'session' ), $args );
    }

    public function jobroles () {

        $labels = array(
            'name'              => _x( 'Job Roles', 'taxonomy general name' ),
            'singular_name'     => _x( 'Job Role', 'taxonomy singular name' ),
            'search_items'      => __( 'Search Job Roles' ),
            'all_items'         => __( 'All Job Roles' ),
            'parent_item'       => __( 'Parent Job Role' ),
            'parent_item_colon' => __( 'Parent Job Role:' ),
            'edit_item'         => __( 'Edit Job Role' ),
            'update_item'       => __( 'Update Job Role' ),
            'add_new_item'      => __( 'Add New Job Role' ),
            'new_item_name'     => __( 'New Job Role Name' ),
            'menu_name'         => __( 'Job Roles' ),
        );

        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'job-role' ),
        );

        register_taxonomy( 'jobroles', array( 'session' ), $args );
    }

    public function experience () {

        $labels = array(
            'name'              => _x( 'Experience', 'taxonomy general name' ),
            'singular_name'     => _x( 'Experience', 'taxonomy singular name' ),
            'search_items'      => __( 'Search Experiences' ),
            'all_items'         => __( 'All Experiences' ),
            'parent_item'       => __( 'Parent Experience' ),
            'parent_item_colon' => __( 'Parent Experience:' ),
            'edit_item'         => __( 'Edit Experience' ),
            'update_item'       => __( 'Update Experience' ),
            'add_new_item'      => __( 'Add New Experience' ),
            'new_item_name'     => __( 'New Experience Name' ),
            'menu_name'         => __( 'Experiences' ),
        );

        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'experience' ),
        );

        register_taxonomy( 'experience', array( 'session' ), $args );
    }

    public function markets () {

        $labels = array(
            'name'              => _x( 'Markets', 'taxonomy general name' ),
            'singular_name'     => _x( 'Market', 'taxonomy singular name' ),
            'search_items'      => __( 'Search Markets' ),
            'all_items'         => __( 'All Markets' ),
            'parent_item'       => __( 'Parent Market' ),
            'parent_item_colon' => __( 'Parent Market:' ),
            'edit_item'         => __( 'Edit Market' ),
            'update_item'       => __( 'Update Market' ),
            'add_new_item'      => __( 'Add New Market' ),
            'new_item_name'     => __( 'New Market Name' ),
            'menu_name'         => __( 'Markets' ),
        );

        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'market' ),
        );

        register_taxonomy( 'markets', array( 'session' ), $args );

    }

    public function dates () {

        $labels = array(
            'name'              => _x( 'Dates', 'taxonomy general name' ),
            'singular_name'     => _x( 'Date', 'taxonomy singular name' ),
            'search_items'      => __( 'Search Dates' ),
            'all_items'         => __( 'All Dates' ),
            'parent_item'       => __( 'Parent Date' ),
            'parent_item_colon' => __( 'Parent Date:' ),
            'edit_item'         => __( 'Edit Date' ),
            'update_item'       => __( 'Update Date' ),
            'add_new_item'      => __( 'Add New Date' ),
            'new_item_name'     => __( 'New Date Name' ),
            'menu_name'         => __( 'Dates' ),
        );

        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'date' ),
        );

        register_taxonomy( 'dates', array( 'session' ), $args );

    }

    public function times () {

        $labels = array(
            'name'              => _x( 'Times', 'taxonomy general name' ),
            'singular_name'     => _x( 'Time', 'taxonomy singular name' ),
            'search_items'      => __( 'Search Times' ),
            'all_items'         => __( 'All Times' ),
            'parent_item'       => __( 'Parent Time' ),
            'parent_item_colon' => __( 'Parent Time:' ),
            'edit_item'         => __( 'Edit Time' ),
            'update_item'       => __( 'Update Time' ),
            'add_new_item'      => __( 'Add New Times' ),
            'new_item_name'     => __( 'New Time Name' ),
            'menu_name'         => __( 'Times' ),
        );

        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'time' ),
        );

        register_taxonomy( 'times', array( 'session' ), $args );

    }

    public function locations () {

        $labels = array(
            'name'              => _x( 'Locations', 'taxonomy general name' ),
            'singular_name'     => _x( 'Location', 'taxonomy singular name' ),
            'search_items'      => __( 'Search Locations' ),
            'all_items'         => __( 'All Locations' ),
            'parent_item'       => __( 'Parent Location' ),
            'parent_item_colon' => __( 'Parent Location:' ),
            'edit_item'         => __( 'Edit Location' ),
            'update_item'       => __( 'Update Location' ),
            'add_new_item'      => __( 'Add New Locations' ),
            'new_item_name'     => __( 'New Location Name' ),
            'menu_name'         => __( 'Locations' ),
        );

        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'location' ),
        );

        register_taxonomy( 'locations', array( 'session' ), $args );

    }

    public function devcontracks () {

        $labels = array(
            'name'              => _x( 'DevCon Tracks', 'taxonomy general name' ),
            'singular_name'     => _x( 'DevCon Track', 'taxonomy singular name' ),
            'search_items'      => __( 'Search DevCon Tracks' ),
            'all_items'         => __( 'All DevCon Tracks' ),
            'parent_item'       => __( 'Parent DevCon Track' ),
            'parent_item_colon' => __( 'Parent DevCon Track:' ),
            'edit_item'         => __( 'Edit DevCon Track' ),
            'update_item'       => __( 'Update DevCon Track' ),
            'add_new_item'      => __( 'Add New DevCon Track' ),
            'new_item_name'     => __( 'New DevCon Track Name' ),
            'menu_name'         => __( 'DevCon Tracks' ),
        );

        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'devcontrack' ),
        );

        register_taxonomy( 'devcontracks', array( 'session' ), $args );

    }

    public function bbworldtracks () {

        $labels = array(
            'name'              => _x( 'BbWorld Tracks', 'taxonomy general name' ),
            'singular_name'     => _x( 'BbWorld Track', 'taxonomy singular name' ),
            'search_items'      => __( 'Search BbWorld Tracks' ),
            'all_items'         => __( 'All BbWorld Tracks' ),
            'parent_item'       => __( 'Parent BbWorld Track' ),
            'parent_item_colon' => __( 'Parent BbWorld Track:' ),
            'edit_item'         => __( 'Edit BbWorld Track' ),
            'update_item'       => __( 'Update BbWorld Track' ),
            'add_new_item'      => __( 'Add New BbWorld Track' ),
            'new_item_name'     => __( 'New BbWorld Track Name' ),
            'menu_name'         => __( 'BbWorld Tracks' ),
        );

        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'bbworldtrack' ),
        );

        register_taxonomy( 'bbworldtracks', array( 'session' ), $args );

    }

    public function area () {

        $labels = array(
            'name'              => _x( 'Areas', 'taxonomy general name' ),
            'singular_name'     => _x( 'Area', 'taxonomy singular name' ),
            'search_items'      => __( 'Search Areas' ),
            'all_items'         => __( 'All Areas' ),
            'parent_item'       => __( 'Parent Area' ),
            'parent_item_colon' => __( 'Parent Area:' ),
            'edit_item'         => __( 'Edit Area' ),
            'update_item'       => __( 'Update Area' ),
            'add_new_item'      => __( 'Add New Area' ),
            'new_item_name'     => __( 'New Area Name' ),
            'menu_name'         => __( 'Areas' ),
        );

        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'area' ),
        );

        register_taxonomy( 'area', array( 'livesession' ), $args );
    }

    public function year () {

        $labels = array(
            'name'              => _x( 'Years', 'taxonomy general name' ),
            'singular_name'     => _x( 'Year', 'taxonomy singular name' ),
            'search_items'      => __( 'Search Years' ),
            'all_items'         => __( 'All Years' ),
            'parent_item'       => __( 'Parent Year' ),
            'parent_item_colon' => __( 'Parent Year:' ),
            'edit_item'         => __( 'Edit Year' ),
            'update_item'       => __( 'Update Year' ),
            'add_new_item'      => __( 'Add New Year' ),
            'new_item_name'     => __( 'New Year Name' ),
            'menu_name'         => __( 'Years' ),
        );

        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'year' ),
        );

        register_taxonomy( 'year', array( 'livesession' ), $args );
    }

}