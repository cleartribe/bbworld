<?php 
class BB_Defaults {

	public $is_live;

	public function __construct() {

		$this->hooks();
	}

	public function hooks()
	{	
		//core wordpress enablers
		add_theme_support( 'post-thumbnails' ); 
		add_image_size( 'featured-large', 310, 330, true ); // width, height, crop

		add_post_type_support( 'page', 'excerpt' ); 
		add_action('admin_enqueue_scripts', array($this, 'chromefix_inline_css'));
		
		//user enablers
		add_action( 'init', array($this, 'blockusers_init') );
		add_action('after_setup_theme', array($this, 'remove_admin_bar') );
        
    	//advanced custom field options
		acf_add_options_page('BbWorld Settings');
		acf_add_options_page('Live Streaming Settings');

		//hook on save post for options template update
		add_action('acf/save_post', array($this, 'update_live_session'), 20);
	}


	function update_live_session( $post_id ) {

		global $bb_auth;

		$this->is_live = get_field('is_live', 'option');

		if (isset($_POST['acf']['field_5751ba8746434']))
	    update_option( 'current_live_session_id', $_POST['acf']['field_5751ba8746434'] );

		$home_dir = get_home_path();

		//islive file generation
		$islive_file = $home_dir.'wp-content/themes/bb/quick/livestream_islive.txt';
        $islive_content = $this->ajax_is_live();
        file_put_contents($islive_file, $islive_content);

		//archive file generation
		$archive_file = $home_dir.'wp-content/themes/bb/quick/livestream_archive.txt';
        $archive_content = $this->ajax_archive_check_for_live_changes();
        file_put_contents($archive_file, $archive_content);

        //single file generation
        $single_file = $home_dir.'wp-content/themes/bb/quick/livestream_single.txt';
        $single_content = $this->ajax_single_check_for_live_changes();
        file_put_contents($single_file, $single_content);

        //generating vc static files
        $sessions = array('highered', 'proed', 'developers', 'k12', 'keynote');
        $years = array('2017', '2016');
        foreach ($years as $year) {
        	foreach ($sessions as $session) {
        		//single file generation
		        $vc_session_file = $home_dir.'wp-content/themes/bb/quick/livestream_vc_archive_'.$year.'_'.$session.'.txt';
		        $vc_session_content = $this->vc_live_sessions_grid($year, $session);
		        file_put_contents($vc_session_file, $vc_session_content);
        	}
        }
       
	}


	public function ajax_is_live () {

		return $this->is_live;
	}

	public function ajax_single_check_for_live_changes ()
	{

	    $current_post_id = get_field('current_live_session', 'option'); 
        return get_permalink( $current_post_id );
		

	}

	public function ajax_archive_check_for_live_changes ()
	{

        date_default_timezone_set('America/Chicago');

        $current_post_id = get_field('current_live_session', 'option'); 

        //before we go much further. is the livestream started?
        if (!$this->is_live)
            return '<section class="live-session-hero-block" style="background:none"><div class="row"><div class="columns small-12"><h3 class="centered">We are not streaming live sessions at this moment. Check back tomorrow for more exciting BbWorld LIVE content.</h3></div></div></section>';

    
        $args = array(
            'post_type' => 'livesession',
            'orderby'   => 'meta_value',
            'order'     => 'ASC',
            'meta_key'  => 'time',
            'posts_per_page' => '-1'
        );

        $query = new WP_Query( $args );

        $live_hero_html = '';
        $live_html_header = '<section class="live-session-blocks" style="margin-bottom:40px;"><div class="row"><div class="small-12 columns"><ul class="small-block-grid-1 medium-block-grid-3">';
        $live_session_html = '';

        $current_date = date("Ymd");
        $current_time = date('H:i:s');

        //TESTING ONLY
        $runtest = get_field('run_a_test', 'option'); 

        if ($runtest) {

            $current_date = date("Ymd", strtotime(get_field('testing_date', 'option')));
            $current_time = date('H:i:s', strtotime(get_field('testing_time', 'option')));

        }

        foreach ($query->posts as $key => $post) {
        

            $current_post['title'] = $post->post_title;
            $current_post['content'] = $post->post_content;
            $current_post['ID'] = $post->ID;
            $current_post['date'] = get_field('date', $post->ID);
            $current_post['time'] = get_field('time', $post->ID);
            $current_post['speaker'] = get_field('speaker_info', $post->ID);
            $current_post['url'] = get_permalink($post);
            $current_post['thumbid'] = get_post_thumbnail_id($post->ID);
            $current_post['formatted_datetime'] = date('F jS, g:i A', strtotime($current_post['date'] . ' ' . $current_post['time']));


            //if this one is live now, add the hero html content
            if ($current_post_id == $current_post['ID'] && $current_date == $current_post['date'] )   {
                $live_hero_html .= $this->live_session_hero_html($current_post);
            }

            else {
                //if its the right date and the time hasnt passed
                if ($current_date == $current_post['date'] && $current_time < $current_post['time']) {
                    $live_session_html .= $this->single_live_session_html($current_post);
                }
            }           
        }


        if ($live_session_html == '') {
            $live_session_html .= '<h3 class="centered">There are no remaining live sessions streaming today. please check back later</h3>';
        }
        else {
            $live_html_header .= '<h2 class="centered utility-head">Upcoming Sessions</h2>';
        }
        
        $live_session_html .= '</ul></div></div></section>';

        return $live_hero_html . ' ' .$live_html_header. ' ' .$live_session_html;

    }

    public function live_session_hero_html($current_post)
    {

        $thumb_url = $this->get_thumb_url($current_post);

        $live_hero_html = '<section class="live-session-hero-block"><div class="row"><div class="columns small-12">';
        $live_hero_html .= '<h2 class="centered utility-head">Live Now</h2>';
        $live_hero_html .= '</div></div>';
        $live_hero_html .= '<div class="row">';
        $live_hero_html .= '<div class="columns small-12 medium-4">';
            $live_hero_html .= '<p><a href="'.$current_post['url'].'"><img alt="'.$current_post['title'].'" src="'.$thumb_url.'"></a></p>';
            
        $live_hero_html .= '</div>';
        $live_hero_html .= '<div class="columns small-12 medium-8">';
            $live_hero_html .= '<h2><a href="'.$current_post['url'].'">'.$current_post['title'].'</a></h2>';
         
            $live_hero_html .= '<p><strong>'.$current_post['speaker'].'</strong></p>';
            $live_hero_html .= '<p>'.$current_post['content'].'</p>';
            $live_hero_html .= '<p><a class="button" href="'.$current_post['url'].'">Watch Live Now</a></p>';
            
        $live_hero_html .= '</div></div></section>';

        return $live_hero_html;

    }

    public function single_live_session_html($current_post)
    {
        $thumb_url = $this->get_thumb_url($current_post);
        
        $html = '<li>';
            $html .= '<div class="session">';
                $html .= '<p>';
                    $html .= '<img alt="'.$current_post['title'].'" style="width:100%;" src="'.$thumb_url.'">';
                    $html .= '<span style="width:100%; display:block; line-height:normal; padding:.46875rem .9375rem; margin:0px; font-size:.8125rem; text-transform:uppercase; text-align:center; font-weight:bold; background: #e1e1e1">Streaming '.$current_post['formatted_datetime'].' CT</span>';
                $html .= '</p>';
                $html .= '<h3>'.$current_post['title'].'</h3>';
                $html .= '<p><strong>'.$current_post['speaker'].'</strong></p>';
                $html .= '<p>'. $current_post['content'] .'</p>';
            $html .= '</div>';
        $html .= '</li>';

        return $html;
    }

    public function get_thumb_url($current_post)
    {
        $thumb_url_array = wp_get_attachment_image_src($current_post['thumbid'], 'featured-large', false);

        if (empty($thumb_url_array))
            $thumb_url = 'http://placehold.it/300x177';
        else {
            $thumb_url = $thumb_url_array[0];
        }

        return $thumb_url;
    }


    public function vc_live_sessions_grid($year, $sessions)
    {
 
        date_default_timezone_set('America/Chicago');

        $daily_args = array (
            'orderby'   => 'meta_value',
            'order'     => 'ASC',
            'meta_key'  => 'time',
        );
        $args = array(
            'post_type' => 'livesession',
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'area',
                    'terms' => array($sessions),
                    'field'    => 'slug',
                ),
                array(
                    'taxonomy' => 'year',
                    'terms' => array($year),
                    'field'    => 'slug',
                ),
            ),
        );

        $args = array_merge($args, $daily_args);
        $query = new WP_Query( $args );
        
        $runtest = get_field('run_a_test', 'option'); 
        $current_live_session = get_field('current_live_session', 'option'); 

        $live_hero_html = '';
        $html = '<div class="vc_row wpb_row vc_inner vc_row-fluid">';

        $inc = 1;

        if($query->found_posts == 1) {
            $vc_col = 'vc_col-sm-12';
        }
        else {
            $vc_col = 'vc_col-sm-4';
        }

        foreach ($query->posts as $key => $post) {
            
            $post_title = $post->post_title;
            $post_content = $post->post_content;
            $post_id = $post->ID;

            $post_date = get_field('date', $post_id);
            $post_time = get_field('time', $post_id);
            $formatted_date = date('F jS, g:i A', strtotime($post_date . ' ' . $post_time));

            $post_speaker_info = get_field('speaker_info', $post_id);
            $post_url = get_permalink($post);

            $post_is_archived = get_field('is_archived', $post_id);

            $thumb_id = get_post_thumbnail_id($post_id);

            $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'featured-large', false);

            if (empty($thumb_url_array))
                $thumb_url = 'http://placehold.it/300x177';
            else {
                $thumb_url = $thumb_url_array[0];
            }


            $end_divs = '';


            if($inc % 3 == 0) {$end_divs = '</div><div class="vc_row wpb_row vc_inner vc_row-fluid">';}

            $current_date = date("Ymd");
            $current_time = date('H:i:s');

            //TESTING

            if ($runtest) {
                $current_date = date("Ymd", strtotime(get_field('testing_date', 'option')));
                $current_time = date('H:i:s', strtotime(get_field('testing_time', 'option')));
            }


            if ( $current_live_session == $post_id && $this->is_live ) {
    
              	$live_hero_html .= '<section><div class="row"><div class="columns small-12">';
		        $live_hero_html .= '<h2 class="centered utility-head">Live Now</h2>';
		        $live_hero_html .= '</div></div>';
		        $live_hero_html .= '<div class="row">';
		        $live_hero_html .= '<div class="columns small-12 medium-4">';
		            $live_hero_html .= '<p><a href="'.$post_url.'"><img alt="'.$post_title.'" src="'.$thumb_url.'"></a></p>';
		            
		        $live_hero_html .= '</div>';
		        $live_hero_html .= '<div class="columns small-12 medium-8">';
		            $live_hero_html .= '<h2><a href="'.$post_url.'">'.$post_title.'</a></h2>';
		         
		            $live_hero_html .= '<p><strong>'.$post_speaker_info.'</strong></p>';
		            $live_hero_html .= '<p>'.$post_content.'</p>';
		            $live_hero_html .= '<p><a class="button" href="'.$post_url.'">Watch Live Now</a></p>';
		            
		        $live_hero_html .= '</div></div></section>';

            }
            else {

            
	            if (!$post_is_archived) {
	                $post_url_start = '';
	                $post_url_end = '';
	            } 

	            else if ($post_is_archived) {
	                $post_url_start = '<a href="'.$post_url.'">';
	                $post_url_end = '</a>';
	            }

	            else if (($current_live_session !== $post_id && $is_live)) { 
	                $post_url_start = '';
	                $post_url_end = '';
	            }

	            else {
	                $post_url_start = '<a href="'.$post_url.'">';
	                $post_url_end = '</a>';
	            }

	            $html .= '<div class="wpb_column vc_column_container '.$vc_col.'">';
	                $html .= '<div class="session" style="padding:20px;">';
	                    $html .= '<p>'.$post_url_start.'<img alt="'.$post_title.'" class="vc-session-thumb" src="'.$thumb_url.'">'.$post_url_end;
	                    $html .= '<br />';

	                    if ( $current_live_session == $post_id && $this->is_live ) {
	                        $html .= '<a href="'.$post_url.'" style="width:100%; margin-bottom:0px" class="button dark small">Watch Live Now!</a></p>';
	                    }

	                    else if ($current_date < $post_date || ($current_date == $post_date && $current_time < $post_time)) { 
	                        $html .= '<span style="width:100%; display:block; line-height:normal; padding:.46875rem .9375rem; margin:0px; font-size:.8125rem; text-transform:uppercase; text-align:center; font-weight:bold; background: #e1e1e1">Streaming '.$formatted_date.' CT</span></p>';
	                    }
	                   
	                    else if(!$post_is_archived) {
	                        $html .= '<span style="width:100%; display:block; line-height:normal; padding:.46875rem .9375rem; margin:0px; font-size:.8125rem; text-transform:uppercase; text-align:center; font-weight:bold; background: #e1e1e1">Recording available soon</span></p>';
	                    }

	                    else if ($post_is_archived) {
	                        $html .= '<a href="'.$post_url.'" style="width:100%; margin-bottom:0px" class="button dark small">Watch on demand</a>';
	                    }

	                    $html .= '<h3>'.$post_url_start.''.$post_title.''.$post_url_end.'</h3>';
	                    $html .= '<p style="font-weight:normal">'.$post_speaker_info.'</p>';

	                    
	                    
	                    $html .= '<p>'.$post_content .'</p>';
	                $html .= '</div>';
	            $html .= '</div>';

	            $html .= $end_divs;

	            $inc++;
	        }

        }
        
        $html = $live_hero_html.''.$html.'</div>';

        return $html;
    }

	public function chromefix_inline_css()
	{ 
		wp_add_inline_style( 'wp-admin', '#adminmenu { transform: translateZ(0); }' );
	}

	/*
	Block toolbar for everyone except admins
	*/
	public function remove_admin_bar() {
		if (!current_user_can('administrator') && !is_admin()) {
		  show_admin_bar(false);
		}
	}

	/*
	Block anyone other then admins from accessing admin
	*/
	public function blockusers_init() {
		if ( is_admin() && ! current_user_can( 'administrator' ) && ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
			wp_redirect( home_url() );
			exit;
		}
	}

} 