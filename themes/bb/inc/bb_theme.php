<?php 
class BB_Theme {

	public $theme_dir;
	public $post_slug;
	public $footer_html;
	public $top_sections;
	public $watch_name;

	public function __construct() {
		$this->theme_dir = site_url('/wp-content/themes/bb/');
		$this->top_sections = array('bbworld','devcon', 'bbworldlive');

		$this->hooks();
	}

	public function hooks()
	{	
		//navigation
		add_action( 'init', array($this, 'register_nav_menus' ));

		//check inbound links for utm parameters to save as cookie
		$this->utm_cookie_check();

	}

	public function register_nav_menus() {
		register_nav_menu('bbworld-menu',__( 'BbWorld Menu' ));
		register_nav_menu('devcon-menu',__( 'DevCon Menu' ));
		register_nav_menu('bbworldlive-menu',__( 'BbWorldLive Menu' ));
		register_nav_menu('loggedin-menu',__( 'Logged In Menu' ));
		register_nav_menu('for-menu',__( 'For Menu' ));
		register_nav_menu('footer-menu',__( 'Footer Menu' ));
	}


	public function utm_cookie_check()
	{
		$paramcookie = '?';
		
		if ($this->params_contain('utm_', $_REQUEST)) {

			foreach ($_REQUEST as $key => $value) {
				$paramcookie .= $key.'='.$value.'&';
			}

			setcookie( 'utmparams', $paramcookie, time() + (20 * 365 * 24 * 60 * 60), '/', $_SERVER['HTTP_HOST'], false );
		}

	}

	private function params_contain($check_string, $params)
	{
		foreach ($params as $key => $param) {
	        if (stripos($key, $check_string) !== FALSE) {
	            return true;
	        }
	    }
	}


	/*
	Header HTML for the entire theme
	*/
	public function header_html ($post_slug) {

		$this->post_slug = $post_slug;

	?>
	<!doctype html>
	<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
		<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Noto+Serif' rel='stylesheet' type='text/css'>
		<link rel="icon" href="<?php echo $this->theme_dir; ?>images/BbFav.png" />
		<link rel="stylesheet" id="js_composer_front-css" href="<?php echo $this->theme_dir; ?>stylesheets/app.css?ver=19" type="text/css" media="all">

		<title><?php wp_title(''); ?></title>
		<?php wp_head(); ?>
	</head>
	<body <?php body_class($this->post_slug); ?>>
		<!-- Google Tag Manager -->
		<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KZ2ZHJ"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-KZ2ZHJ');</script>
		<!-- End Google Tag Manager -->

		<!--google recaptcha-->
        <script src='https://www.google.com/recaptcha/api.js'></script>

		<div class="svg-def">
		<?php include_once(get_template_directory() . "/images/svg-defs.svg"); ?>
		</div>
		<?php 
	}


	/*
	Top Header HTML for the entire theme
	*/
	public function top_header() {
		?>

		<header class="top-header">
             <div class="row collapse">
                <div class="small-12 columns">

                	<?php echo $this->sign_up_link(); ?>

                    <ul class="top-menu">
                    	<li>FOR: </li> 
                        <?php 
                            wp_nav_menu( array (
                                'menu' => 'for-menu',
                                'container' => '',
                                'theme_location' => 'for-menu',
                                'items_wrap' =>  '%3$s'
                            ) );
                        ?>
                    </ul>
                    
                </div>
            </div>
        </header>

		<?php 
	}

	public function sign_up_link () {
		$classes = get_body_class($this->post_slug);

		$html = '';

		if (in_array('bbworldlive',$classes))  {
			$html = '<a href="/bbworldlive/login/" class="top-register-btn button right dark small hide-for-small" style="margin-left:3px;">Login</a>';
		}
		else {
			//$html = '<a href="/registration" class="top-register-btn button right dark small hide-for-small" style="margin-left:3px;">Register Now</a>';
		}
		

		return $html;

	}

	public function sign_up_link_menu () {
		$classes = get_body_class($this->post_slug);

		$html = '';

		if (in_array('bbworldlive',$classes))  {
			$html = '<p class="menu-register-btn"><a href="/bbworldlive/login/" class="button dark small">Login</a></p>';
		}
		else {
			//$html = '<p class="menu-register-btn"><a href="/registration/" class="button dark small">Register Now</a></p>';
		}
		

		return $html;

	}

	/*
	Main Header HTML for the entire theme
	*/
	public function main_header($post_name)
	{
		?>
			<header class="main-header">
	            <div class="row collapse">
	                <div class="small-12 columns">
	                    
	                    <a href="/<?php echo $post_name; ?>" class="menu-logo">
	                         <svg class="logo shape-logo-<?php echo $post_name; ?>">
	                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#shape-logo-<?php echo $post_name; ?>"></use>
	                        </svg>
	                    </a>

	                    <?php echo $this->sign_up_link_menu();?>

	                    <a class="menu-button">
	                        <svg viewBox="0 0 100 100" class="icon shape-hamburger collapsed">
	                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#shape-hamburger"></use>
	                        </svg>
	                    </a>

	                    <ul class="main-menu hide-for-small">

	                        <?php foreach ($this->top_sections as $top_section) { ?>
	                            
	                            <?php 

	                            $current = '';
	                            $hidden = 'hidden';
	                            $collapsed = 'collapsed';

	                            if ($post_name == $top_section) {
	                                $current = 'current';
	                                $hidden = '';
	                                $collapsed = '';
	                            }

	                            ?>

	                            <li>
	                                <a href="/<?php echo $top_section; ?>" class="<?php echo $current; ?> expandable">
	                                    <svg viewBox="0 0 100 100" class="icon shape-expand <?php echo $collapsed; ?>">
	                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#shape-expand"></use>
	                                    </svg>
	                                    <svg class="logo shape-logo-<?php echo $top_section; ?>">
	                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#shape-logo-<?php echo $top_section; ?>"></use>
	                                    </svg>
	                                </a>
	                                <ul class="sub-menu <?php echo $hidden; ?>">
	                                    
	                                    <?php 

	                                        wp_nav_menu( array (
	                                            'menu' => $top_section.'-menu',
	                                            'container' => '',
	                                            'theme_location' => $top_section.'-menu',
	                                            'items_wrap' =>  '%3$s'
	                                        ) );

	                                    ?>

	                                </ul>
	                            </li>

	                        <?php } ?>
	                      
	                    </ul>
	                                    
	                </div>
	            </div>
        	</header>
		<?php 
	}

	public function hero_content($post_id = NULL, $is_live = false, $is_announcement = false, $is_session = false, $custom_html = false)
	{
		$background_video = get_field( "background_video", $post_id );
		$background_image = get_field( "background_image", $post_id );
		$background_horizontal_placement = get_field( "background_horizontal_placement", $post_id );
		$background_vertical_placement = get_field( "background_vertical_placement", $post_id );

		if ($is_session) {
			$background_image = site_url('/wp-content/uploads/2016/02/BbW17_green-1000x500.jpg');
		}
		else {
			$background_image = (!empty($background_image) ? $background_image : $this->theme_dir.'images/home-bg.jpg');
		}
		
		$background_horizontal_placement = (!empty($background_horizontal_placement) ? $background_horizontal_placement : "center");
		$background_vertical_placement = (!empty($background_vertical_placement) ? $background_vertical_placement : "center");

		if (!isset($post_id)) {
			$post_title = 'Thanks For Attending BbWorld 2017';
		}
		else {
			$post_title = get_the_title($post_id);
		}


		?>
		<section class="hero-section" style="background:url(<?php echo $background_image; ?>) #000 no-repeat <?php echo $background_horizontal_placement; ?> <?php echo $background_vertical_placement; ?>; background-size:cover;">

				<?php if (!empty($background_video)) { 
					$this->get_background_video_html($background_video);
				} ?> 

                <div class="inner">
                    <div class="row">

                    <?php if (get_field( "hero_content" )) { ?>
                        <div class="small-12 columns incont">
                            <?php the_field( "hero_content", $post_id ); ?>
                        </div>
        			<?php } else { ?>
                        <div class="small-12 columns">
                        	<?php if ($is_live) { ?>
                        	<h3 style="text-align:center">Now Live Streaming</h3>
                        	<?php } ?>
                            <h1 style="text-align:center;"><?php echo $post_title; ?></h1>
                            <?php if ($custom_html) { ?>
                            <?php echo $custom_html; ?>
                            <?php } ?>
                        </div>

        			<?php } ?>

        	
        		    </div>
                </div>

        
        	</section> 

        <?php  
	}

	public function home_announcement () 
	{

		$home_announcement = get_field( 'home_announcement', 'option' );
    				
		if (!empty($home_announcement)) { ?>
			
			<div class="announce-panel">
			 	<div class="row">
			 		<div class="small-12 columns">
                		<?php echo $home_announcement; ?>
                	</div>
                </div>
            </div>

		<?php }
	}

	public function get_background_video_html($background_video) {
		?>
		<div id="muteYouTubeVideoPlayer" style="max-width: 1000%;margin-left: -20%; position: absolute; margin-top: -250px; width: 140%; height: 996.25px;"></div>
 
		<script async src="https://www.youtube.com/iframe_api"></script>
		<script>
		 function onYouTubeIframeAPIReady() {
		  var player;
		  player = new YT.Player('muteYouTubeVideoPlayer', {
		    videoId: '<?php echo $background_video; ?>', // YouTube Video ID
		    width: '140%',               // Player width (in px)
		    height: 316,              // Player height (in px)
		    playerVars: {
		      autoplay: 1,  
		      disablekb: 1,      // Auto-play the video on load
		      controls: 0,        // Show pause/play buttons in player
		      showinfo: 0,        // Hide the video title
		      modestbranding: 1,  // Hide the Youtube Logo
		      loop: 1,
		      playlist : '<?php echo $background_video; ?>',            // Run the video in a loop
		      fs: 0,              // Hide the full screen button
		      cc_load_policy: 0, // Hide closed captions
		      iv_load_policy: 3,  // Hide the Video Annotations
		      autohide: 0         // Hide video controls when playing
		    },
		    events: {
		      onReady: function(e) {
		        e.target.mute();
		      },
		     
		    }
		  });
		 }
		 
		</script>
		<?php 
	}


	public function bottom_cta ($post_id) {
		if (get_field( "show_bottom_cta", $post_id )) { ?>
                        
        <section class="bottom-cta-holder">
            <div class="row">
                <div class="small-12 medium-10 medium-centered columns">
                    <?php 
                        $global_cta = get_field('global_cta', 'option');
                        echo $global_cta;
                    ?>
                </div>
            </div>
        </section>

        <?php }
	}
	

	/*
	Footer HTML for the entire theme
	*/
	public function footer_html ($show_social = true) 
	{
		?>

	        <section class="bottom-form-holder dark-section">
	        	<div class="row">
	        		<div class="small-12 medium-8 columns">
	        		<h3>Sign up for updates</h3>
	        		<?php echo do_shortcode('[gravityform id="1" title="false" description="false"]'); ?>
	        		</div>
	        		<div class="small-12 medium-4 columns">
	        			<?php 
                            $global_social = get_field('global_social', 'option');
                            echo $global_social;
                        ?>
	        		</div>
	        	</div>
	        </section>

	        <?php $this->footer_nav(); ?>

			<?php $this->footer_scripts(); ?>
			
			
		<?php 
		$this->close_html();
	}

	public function footer_nav () 
	{
		?>
			<footer>
	            <div class="row">
	                <div class="small-12 columns">
	                    
	                    <ul class="inline-list right">
	                    	<li>© <?php echo date("Y") ?> Blackboard Inc.</li>
							<?php 

                                wp_nav_menu( array (
                                    'menu' => 'footer-menu',
                                    'container' => '',
                                    'theme_location' => 'footer-menu',
                                    'items_wrap' =>  '%3$s'
                                ) );

                            ?>
	                    </ul>
	                </div>
	            </div>
	        </footer>
		<?php 
	}

	public function footer_scripts() 
	{
		?>
			<script type='text/javascript' src='/wp-includes/js/jquery/jquery.js?ver=1.11.3'></script>
			<script type='text/javascript' src='<?php echo $this->theme_dir; ?>js/app.min.js?ver=18'></script>
			<?php wp_footer(); ?>
		<?php 
	}

	public function close_html() {
		?>
			</body>
			</html>
		<?php 
	}

	public function get_error_message( $error_code ) {
	    switch ( $error_code ) {
	        case 'empty_username':
			    return __( 'You need to enter your email address to continue.', 'personalize-login' );
			 
			case 'invalid_email':
			case 'invalidcombo':
			    return __( 'There are no users registered with this email address.', 'personalize-login' );

			case 'password_reset_mismatch':
				return __( 'The passwords you entered do not match' );

			case 'password_reset_empty':
				return __( 'You need to enter a new password' );
	 
	        default:
	            break;
	    }
	     
	    return __( 'An unknown error occurred. Please try again later.', 'personalize-login' );
    }

} 
