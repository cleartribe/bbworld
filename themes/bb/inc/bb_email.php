<?php 
class BB_Email {

	public function __construct() {
		$this->hooks();
	}

	public function hooks()
	{	

    	//override default php mailer to use our mailgun account instead
        //add_action('phpmailer_init', array($this, 'send_smtp_email'));
        add_filter( 'retrieve_password_message', array( $this, 'replace_retrieve_password_message' ), 10, 4 );

        //change FROM name of email
        add_filter( 'wp_mail_from_name', array($this, 'change_email_name'));
        add_filter( 'wp_mail_from',  array($this, 'change_email_address'));

	}

    /**
	 * Returns the message body for the password reset mail.
	 * Called through the retrieve_password_message filter.
	 *
	 * @param string  $message    Default mail message.
	 * @param string  $key        The activation key.
	 * @param string  $user_login The username for the user.
	 * @param WP_User $user_data  WP_User object.
	 *
	 * @return string   The mail message to send.
	 */
	public function replace_retrieve_password_message( $message, $key, $user_login, $user_data ) {
	    // Create new message
	    $msg  = __( 'Hello!', 'personalize-login' ) . "\r\n\r\n";
	    $msg .= "You asked us to reset your password for your BbWorld Live account with the username: ".$user_login." " . "\r\n\r\n";
	    $msg .= __( "If this was a mistake, or you didn't ask for a password reset, just ignore this email and nothing will happen.", 'personalize-login' ) . "\r\n\r\n";
	    $msg .= __( 'To reset your password, visit the following address:', 'personalize-login' ) . "\r\n\r\n";
	    $msg .= site_url( "wp-login.php?action=rp&key=$key&login=" . rawurlencode( $user_login ), 'login' ) . "\r\n\r\n";
	    $msg .= __( 'Thanks!', 'personalize-login' ) . "\r\n";
	 
	    return $msg;
	}

	//change email name to whateva
	public function change_email_name($name) {
		return 'BbWorld Live 2017';
	}

	//change email address to whateva
	public function change_email_address($address) {
		return 'bbworld@blackboard.com';
	}


} 