<?php 
class BB_Auth {

	public function __construct() {
		$this->hooks();
	}

	public function hooks()
	{	

        add_action( 'rest_api_init', array ($this, 'register_routes'));

	}

    public function register_routes()
    {
        register_rest_route( 'bbwlive', '/auth_user/(?P<log>[\s\S]+)/(?P<pwd>[\s\S]+)', array(
            'methods' => WP_REST_Server::ALLMETHODS,
            'callback' => array($this, 'ajax_auth_user'),
        ) );
   
    }

    public function ajax_auth_user( $request )
    {

        $is_live = get_field('is_live', 'option');

        $log = $request['log'];
        $pwd = base64_decode ($request['pwd']);

        $authdata = wp_authenticate($log, $pwd);

        if (is_wp_error($authdata)) {
            return $authdata->get_error_code();
        }

        else {
            
            //get the type of user
            $user_type = get_user_meta($authdata->data->ID, 'user_type', true);

            if ($user_type !== '')
                $authdata->data->user_type = $user_type;

            //check if we are live or not
            $authdata->data->is_live = $is_live;

            return $authdata;
        }
        
    }

} 