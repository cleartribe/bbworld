<?php 
class BB_Shortcodes {

	public function __construct() {
		$this->hooks();
	}

	public function hooks () {
		add_action( 'init', array($this, 'register_shortcodes' ));
	}

    public function register_shortcodes()
    {
        add_shortcode( 'button', array($this, 'button_shortcode' ) );
        add_shortcode( 'countdown', array($this, 'countdown_shortcode' ) );
        add_shortcode( 'registration_form', array ($this, 'registration') );
        add_shortcode( 'custom-password-reset-form', array( $this, 'render_password_reset_form' ) );
    }

    /**
     * A shortcode for rendering the form used to reset a user's password.
     *
     * @param  array   $attributes  Shortcode attributes.
     * @param  string  $content     The text content for shortcode. Not used.
     *
     * @return string  The shortcode output
     */


      // Add Shortcode
    public function button_shortcode( $atts ) {

        // Attributes
        $a = shortcode_atts(array(
          'title' => 'Click Here',
          'link' => '#',
          'target' => '_self',
          'class' => '',
          'utm' => '',
          ), $atts );


        if ($a['utm'] == true) {
            if (isset($_COOKIE["utmparams"])) {
                $a['utm'] = $_COOKIE["utmparams"];
            }
        }

        return '<a class="button '.$a['class'].'" target="'.$a['target'].'" href="'.$a['link'].''.$a['utm'].'">'.$a['title'].'</a>';
    }


    public function countdown_shortcode( $atts )
    {

        // Attributes
        $a = shortcode_atts(array(
          'date' => '1 month',
          ), $atts );

        date_default_timezone_set('America/Chicago');
        $event_date = date('D M d Y H:i:s O', strtotime($a['date']));


        $html = '<div id="clockdiv">';
        $html .= '<div>';
        $html .= '<span class="days"></span>';
        $html .= '<div class="smalltext">Days</div>';
        $html .= '</div>';
        $html .= '<div>';
        $html .= '<span class="hours"></span>';
        $html .= '<div class="smalltext">Hours</div>';
        $html .= '</div>';
        $html .= '<div>';
        $html .= '<span class="minutes"></span>';
        $html .= '<div class="smalltext">Minutes</div>';
        $html .= '</div>';
        $html .= '<div>';
        $html .= '<span class="seconds"></span>';
        $html .= '<div class="smalltext">Seconds</div>';
        $html .= '</div>';
        $html .= '</div>';


        $html .= '<script>';

        $html .= 'function getTimeRemaining(endtime) {';
        $html .= 'var t = Date.parse(endtime) - Date.parse(new Date());';
        $html .= 'var seconds = Math.floor((t / 1000) % 60);';
        $html .= 'var minutes = Math.floor((t / 1000 / 60) % 60);';
        $html .= 'var hours = Math.floor((t / (1000 * 60 * 60)) % 24);';
        $html .= 'var days = Math.floor(t / (1000 * 60 * 60 * 24));';
        $html .= 'return {';
        $html .= '"total": t,';
        $html .= '"days": days,';
        $html .= '"hours": hours,';
        $html .= '"minutes": minutes,';
        $html .= '"seconds": seconds';
        $html .= '};';
        $html .= '}';

        $html .= 'function initializeClock(id, endtime) {';
        $html .= 'var clock = document.getElementById(id);';

        $html .= 'var daysSpan = clock.querySelector(".days");';
        $html .= 'var hoursSpan = clock.querySelector(".hours");';
        $html .= 'var minutesSpan = clock.querySelector(".minutes");';
        $html .= 'var secondsSpan = clock.querySelector(".seconds");';

        $html .= 'function updateClock() {';
        $html .= 'var t = getTimeRemaining(endtime);';

        $html .= 'daysSpan.innerHTML = t.days;';
        $html .= 'hoursSpan.innerHTML = ("0" + t.hours).slice(-2);';
        $html .= 'minutesSpan.innerHTML = ("0" + t.minutes).slice(-2);';
        $html .= 'secondsSpan.innerHTML = ("0" + t.seconds).slice(-2);';

        $html .= 'if (t.total <= 0) {';
        $html .= 'clearInterval(timeinterval);';
        $html .= '}';
        $html .= '}';

        $html .= 'var timeinterval = setInterval(updateClock, 1000);';
        $html .= '}';

        $html .= 'var deadline = new Date("'.$event_date.'");';

        $html .= 'initializeClock("clockdiv", deadline);';

        $html .= '</script>';

        return $html;

    }

    //Registration Form
    //shortcode function for delivering the registration form in the templates
    public function registration ($atts) {

        // Attributes
        $a = shortcode_atts(array(
            'market' => false,
        ), $atts );

        //if initial page load, just load the form
        if (empty($_POST)) {

            $output = '<div id="thanks" style="padding-top:50px" class="reg_form">';
            $output .= $this->registration_form($a);
            $output .= '</div>';

            return $output;
        }

        $is_success = false;
        $output = '<div id="thanks" style="padding-top:50px" class="reg_form">';
        $error_message = $this->registration_errors();
        $customer_number = '';

        //if there are NOT initial error messages
        if ($error_message == '') {

            $userdata = array(
                'user_login'  =>  $_POST['singleLineText'],
                'user_pass' => $_POST['singleLineText2'], 
                'user_email' => $_POST['emailAddress'],
                'first_name' => $_POST['firstName'],
                'last_name' => $_POST['lastName'],
            );

            //try and insert the user record
            $customer_number = wp_insert_user( $userdata ) ;

            //On Error
            if( is_wp_error( $customer_number ) ) {
                $return_error = $customer_number->get_error_message();
                $error_message .= '<span class="label alert radius">' . $return_error . '</span><br /><br />';
            } 

            //On Success
            else {

                update_user_meta($customer_number, 'user_type', $_POST['radioButtons']);
                $is_success = true;

            }
        }
    
        //if there are error messages show the form
        if (!$is_success)  { 
            $error_message = '<div class="error_messages">'.$error_message.'</div>';
            $output .= $this->registration_form($a, $error_message);
        } 

        //good to send data to eloqua
        else {

            $this->post_to_eloqua();

            //TRACK IN GA
            $output .= "<style>body {display:none !important;}</style>";
            $output .= "<script>";
                $output .= "window.onload = function() {";
                    $output .= "ga('create', 'UA-73901262-1', 'auto', 'bbTracker');";
                    $output .= "ga('bbTracker.send', {";
                      $output .= "hitType: 'event',";
                      $output .= "eventCategory: 'BbWorld Live Signup',";
                      $output .= "eventAction: 'Success',";
                      $output .= "eventLabel: '".$_POST['radioButtons']."'";
                    $output .= "});";
                $output .= "};";

            $output .= 'window.location.replace("/login?registration=true");';

            $output .= "</script>";

        }

        $output .= '</div>';

        return $output;
    }


    //validate the registration form submission. 
    public function registration_errors () {

        $error_message = '';

        $is_robot = $this->is_robot(); 

        if ($is_robot)
          $error_message .= '<span class="label radius alert">Please Verify That You Are Not A Robot</span><br/>';

        if (trim($_POST['firstName']) == "")
          $error_message .= '<span class="label radius alert">You Need To Enter Your First Name</span><br/>';

        if (trim($_POST['lastName']) == "")
          $error_message .= '<span class="label radius alert">You Need To Enter Your Last Name</span><br/>';

        if (trim($_POST['emailAddress']) == "")
          $error_message .= '<span class="label radius alert">You Need To Enter Your Email Address</span><br/>';

        if (!filter_var($_POST['emailAddress'], FILTER_VALIDATE_EMAIL) && trim($_POST['emailAddress']) != "") 
          $error_message .= '<span class="label radius alert">The Email Address You Entered Is Not Valid.</span><br/>';

        if (trim($_POST['district1']) == "")
          $error_message .= '<span class="label radius alert">You Need To Enter Your District/Company/Organization</span><br/>';

        if (!isset($_POST['radioButtons']))
          $error_message .= '<span class="label radius alert">You Need To Select A Program</span><br/>';

        if (trim($_POST['singleLineText']) == "")
          $error_message .= '<span class="label radius alert">You Need To Enter A Username</span><br/>';

        if (trim($_POST['singleLineText2']) == "")
          $error_message .= '<span class="label radius alert">You Need To Enter A Password</span><br/>';

        if ($error_message != '')
            $error_message .= '<br />';

        return $error_message;
    }

    //check google recaptcha response
    public function is_robot () {
        // if submitted check response
        if ($_POST["g-recaptcha-response"] != '') {

            $secret = '6LdkDiAUAAAAAEB2akY6c5mcyqhzbhoRU5t2AzmW';
            $response = $_POST['g-recaptcha-response'];
            $remoteip = $_SERVER['REMOTE_ADDR'];

            $url = "https://www.google.com/recaptcha/api/siteverify";

            $post_data = http_build_query(
                array(
                    'secret' => $secret,
                    'response' => $response,
                )
            );  

            $arg = array ( 'method' => 'POST', 'timeout' => 45 );
    
            //getting all session data
            $response = wp_remote_request ( 'https://www.google.com/recaptcha/api/siteverify?'.$post_data, $arg );

            $resulting = json_decode($response['body'], true);

            if($resulting['success']) {
                return false;
            }
            else {
                return true;
            }
        }

        else {
            return true;
        }

    }

    //the actual html form
    public function registration_form ($a, $errors = '') {

        $output = '<div class="registration-form-holder">';
        
            $output .= '<div class="centered">';
                $output .= '<h2>Sign up to receive updates and a link to recorded sessions</h2>';
                $output .= '<p><strong>If you are a returning user from 2016, you will need to register for a new username and password. Please email <a href="mailto:bbworldlive@blackboard.com">bbworldlive@blackboard.com</a> with any questions.</strong></p>';
            $output .= '</div>';


            $output .= '<form method="post" name="BbWorld_Live_2017" action="#thanks" id="form3421" class="regform">';

                $output .= $errors;

                $output .= '<input value="BbWorld_Live_2017" type="hidden" name="elqFormName"  />';
                $output .= '<input value="2376" type="hidden" name="elqSiteId">';
                $output .= '<input name="elqCampaignId" type="hidden">';

                $output .= '<input class="source" name="source" value="" type="hidden">';
                $output .= '<input class="medium" name="medium" value="" type="hidden">';
                $output .= '<input class="term" name="term" value="" type="hidden">';
                $output .= '<input class="content" name="content" value="" type="hidden">';
                $output .= '<input class="campaign" name="campaign" value="" type="hidden">';

                $output .= '<h3>Enter your information:</h3>';

                $output .= '<input id="field0" placeholder="First Name" name="firstName" type="text" value="' . (isset($_POST['firstName']) ? $_POST['firstName'] : "") . '">';
                $output .= '<input id="field1" placeholder="Last Name" name="lastName" type="text" value="' . (isset($_POST['lastName']) ? $_POST['lastName'] : "") . '">';
                $output .= '<input id="field2" placeholder="Email" name="emailAddress" type="text" value="' . (isset($_POST['emailAddress']) ? $_POST['emailAddress'] : "") . '">';
                $output .= '<input id="field3" placeholder="District/Company/Organization" name="district1" type="text" value="' . (isset($_POST['district1']) ? $_POST['district1'] : "") . '">';
                $output .= ' <input id="field4" name="district1website" placeholder="District/Company/Organization Website" type="text" value="' . (isset($_POST['district1website']) ? $_POST['district1website'] : "") . '" />';

                    if ($a['market']) {
                        $output .= '<input name="radioButtons" type="hidden" value="'.$a['market'].'" class="input">';
                    }
                    else  {

                        $output .= '<h3>Choose your program:</h3>';
                   
                        $output .= '<input name="radioButtons" type="radio" value="Developer (July 25)" ' . (isset($_POST['radioButtons']) && $_POST['radioButtons'] == "Developer (July 25)" ? 'checked' : "") . ' class="input">';
                        $output .= '<span>Developer (July 25)</span>';
                        $output .= '<br />';
                    
                        $output .= '<input name="radioButtons" type="radio" value="Professional Education (July 25)" ' . (isset($_POST['radioButtons']) && $_POST['radioButtons'] == "Professional Education (July 25)" ? 'checked' : "") . ' class="input">';
                        $output .= '<span>Professional Education (July 25)</span>';
                        $output .= '<br />';
                    
                        $output .= '<input name="radioButtons" type="radio" value="Blackboard Keynote (July 25)" ' . (isset($_POST['radioButtons']) && $_POST['radioButtons'] == "Blackboard Keynote (July 25)" ? 'checked' : "") . ' class="input">';
                        $output .= '<span>Blackboard Keynote (July 25)</span>';
                        $output .= '<br />';
                                    
                        $output .= '<input name="radioButtons" type="radio" value="K-12 (July 26)" ' . (isset($_POST['radioButtons']) && $_POST['radioButtons'] == "K-12 (July 26)" ? 'checked' : "") . ' class="input">';
                        $output .= '<span>K-12 (July 26)</span>';
                        $output .= '<br />';

                        $output .= '<input name="radioButtons" type="radio" value="Higher Education (July 27)" ' . (isset($_POST['radioButtons']) && $_POST['radioButtons'] == "Higher Education (July 27)" ? 'checked' : "") . ' class="input">';
                        $output .= '<span>Higher Education (July 27)</span>';
                        $output .= '<br />';

                    }
               
                $output .= '<h3>Please create a username & password:</h3>';
                $output .= '<p>This is how you will log in to the live stream</p>';

                $output .= '<input id="field3" placeholder="Username" name="singleLineText" type="text" value="' . (isset($_POST['singleLineText']) ? $_POST['singleLineText'] : "") . '"/>';
                $output .= '<input id="field4" placeholder="Password" name="singleLineText2" type="password" value="' . (isset($_POST['singleLineText2']) ? $_POST['singleLineText2'] : "") . '"/>';
                $output .= '<input id="field5" placeholder="Are you in need of special assistance? If so, please specify:" name="singleLineText3" type="text" value="' . (isset($_POST['singleLineText3']) ? $_POST['singleLineText3'] : "") . '"/>';

              

                $output .= '<p><input name="BbWorldLiveOptIn" type="checkbox" value="on" checked  />';
                $output .= '&nbsp;By submitting this form you agree to receive emails from Blackboard and can unsubscribe at any time.</p>';

                $output .= '<div class="g-recaptcha" data-sitekey="6LdkDiAUAAAAAE_d5CzoAI1qTJsgQS0L7fTB6dBm"></div><br />';
                $output .= '<input class="button menu-register-btn dark btn-success" style="margin-top:10px" type="submit" value="Sign Up"/>';

            $output .= '</form>';

        $output .= '</div>';

        return $output;
    }

    public function post_to_eloqua () {

        $url = 'https://s2376.t.eloqua.com/e/f2';
        $myvars = 'elqFormName=' . $_POST['elqFormName'] . '&elqSiteId=' . $_POST['elqSiteId'] . '&source=' . $_POST['source']  . '&medium=' . $_POST['medium']  . '&term=' . $_POST['term'] . '&content=' . $_POST['content'] . '&campaign=' . $_POST['campaign'] . '&firstName=' . $_POST['firstName'] . '&lastName=' . $_POST['lastName'] . '&emailAddress=' . $_POST['emailAddress'] . '&district1=' . $_POST['district1'] . '&district1website=' . $_POST['district1website'] . '&radioButtons=' . $_POST['radioButtons'] . '&singleLineText=' . $_POST['singleLineText'] . '&singleLineText2=' . $_POST['singleLineText2'] . '&singleLineText3=' . $_POST['singleLineText3'] . '&BbWorldLiveOptIn=' . $_POST['BbWorldLiveOptIn']  . '&iPadProOptin=' . $_POST['iPadProOptin']  ;

        $ch = curl_init( $url );
        curl_setopt( $ch, CURLOPT_POST, 1);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $myvars);
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt( $ch, CURLOPT_HEADER, 0);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec( $ch );

    }


}
