<?php 
class BB_Login {

	public function __construct() {
		$this->hooks();
	}

	public function hooks()
	{	
    	//login
    	//add_action( 'wp_login_failed', array( $this, 'my_front_end_login_fail') ); 
    	add_action( 'login_form_lostpassword', array( $this, 'do_password_lost' ) );
    	//add_action( 'wp_authenticate' , array( $this, 'check_custom_authentication' ) );
    }

	
	//login failed
	public function my_front_end_login_fail( $username ) {
	   $referrer = $_SERVER['HTTP_REFERER'];  // where did the post submission come from?
	   // if there's a valid referrer, and it's not the default log-in screen
	   if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
	      wp_redirect( $referrer . '?login=failed' );  // let's append some information (login=failed) to the URL for the theme to use
	      exit;
	   }
	}



	/**
	 * Initiates password reset.
	 */
	public function do_password_lost() {
	    if ( 'POST' == $_SERVER['REQUEST_METHOD'] ) {
	        $errors = retrieve_password();
	        $redirect_url = site_url( '/bbworldlive/lost-password/' );
	        if ( is_wp_error( $errors ) ) {
	            // Errors found
	            $redirect_url = add_query_arg( 'errors', join( ',', $errors->get_error_codes() ), $redirect_url );
	        } else {
	            // Email sent
	            $redirect_url = add_query_arg( 'checkemail', 'confirm', $redirect_url );
	        }
	 
	        wp_redirect( $redirect_url );
	        exit;
	    }
	}


} 