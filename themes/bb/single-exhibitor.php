<?php

	global $post, $bb_theme;

    $post_slug = $post->post_name;
    $post_ancestry = get_post_ancestors($post->ID);

    $bb_theme->header_html($post_slug);
    $bb_theme->top_header();
    $bb_theme->main_header($post->post_name);

    $url = get_field('url', $post->ID);
    $booth = get_field('booth', $post->ID);
    $thumb_url = get_field('featuredImage', $post->ID);

    if (!is_string($booth)) {
        $booth = '';
    }
    else {
        $booth = '<h3 style="text-align:center">Come See Us At Booth '.$booth.'</h3>';
    }

    $url = get_field('url', $post->ID);
        	
    	if ( have_posts() ) {
    		while ( have_posts() ) {the_post(); ?>
                
                <?php $bb_theme->hero_content($post->ID, false, false, false, $booth); ?>    
                    
                    <section class="main-inner-content single">

                        <div class="row">
                            
                            <div class="small-12 medium-8 medium-centered columns">
                                
                                <p><img src="<?php echo $thumb_url; ?>" /></p>

                                <?php

                                the_content();
                                ?>

                                <?php if (is_string($url) && filter_var($url, FILTER_VALIDATE_URL) !== FALSE) {  ?>        
                                        <a target="_blank" href="<?php echo $url; ?>" class="button small">View <?php echo $post->post_title; ?> Website</a>&nbsp;
                                    <?php } ?>


                                <a class="button small" href="/bbworld/sponsors">View More Sponsors</a>
                            </div>

                        </div>

                    </section>

                <?php 

                $bb_theme->bottom_cta($post->ID); 

    		} 
    	} 

	$bb_theme->footer_html();

?>