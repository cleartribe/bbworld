<?php


class BB_Single_Live_Template {

    public function __construct() 
    {

        $this->hooks();
        $this->content();
    }

    public function hooks()
    {
        add_action( 'wp_footer', array($this, 'footer_scripts') );
        add_action( 'wp_head', array($this, 'header_scripts') );
    }

    public function header_scripts()
    {
   
        ?>
        <script>
            var currenturl = window.location.pathname,
                ajaxdata = { redirect_to : currenturl },
                siteurl = "<?php echo site_url('/'); ?>";
        </script>

        <?php
    }

    public function footer_scripts()
    {
        global $post;

        ?>
        <script>

            var options = {
                ajaxdata : ajaxdata,
            }
            
            bbApp.auth.gate(options, function () {
                bbApp.auth.init();
                bbApp.auth.chat('<?php echo $post->ID; ?>');
            });
            
            
        </script>
        <?php 
    }

    public function content() 
    {
        global $post, $bb_theme;

        $post_slug = $post->post_name;
        $post_ancestry = get_post_ancestors($post->ID);

        $bb_theme->header_html($post_slug);
        $bb_theme->top_header();
        $bb_theme->main_header('bbworldlive');
                
            if ( have_posts() ) {
                while ( have_posts() ) {the_post(); ?>

                    <?php 

                    $live_embed_code = get_field( "live_embed_code" ); 
                    $is_archived = get_field( "is_archived" );
                    $archived_embed_code = get_field("archived_embed_code"); 

                    ?>
                    
                    <?php $bb_theme->hero_content($post->ID, false, false, true); ?>    
                        
                        <section class="main-inner-content single">

                            <div class="row">
                                <div class="small-12 medium-12 medium-centered columns">
                                   
                                    <?php the_content();?>

                                    <p><strong>For the best experience use the following browsers: For Mac/PC - Chrome Version 59, Edge, Internet Explorer 11 or Safari 11. For mobile devices use Safari on iOS and Dolphin browser on Android.</strong></p>

                                    <?php 

                                        if ($is_archived) {
                                            if ($archived_embed_code != '') {
                                                echo '<iframe width="100%" height="400" frameborder="0" scrolling="auto" marginheight="0" marginwidth="0" src="'.$archived_embed_code.'?autoStart=true"></iframe>';
                                            }
                                            else {
                                                echo '<div class="panel"><h3>We will be posting on demand sessions soon. Please check back later.</h3></div>';
                                            }
                                        }
                                        else {
                                            if ($live_embed_code != '') {
                                                echo '<iframe width="100%" height="400" frameborder="0" scrolling="auto" marginheight="0" marginwidth="0" src="'.$live_embed_code.'?autoStart=true"></iframe>';
                                            }
                                            else {
                                                echo '<div class="panel"><h3>We will be posting on demand sessions soon. Please check back later.</h3></div>';
                                            }
                                        }

                                        echo '<div class="chat-wrapper"></div>';
                                        
                                    ?>                    

                                </div>
                            </div>

                        </section>

                    <?php 

                } 
            } 

        $bb_theme->footer_nav();
        $bb_theme->footer_scripts();
        $bb_theme->close_html();
    }

}

$bb_single_live_template = new BB_Single_Live_Template;

?>
