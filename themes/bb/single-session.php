<?php

	global $post, $bb_theme;

    $post_slug = $post->post_name;
    $post_ancestry = get_post_ancestors($post->ID);

    $bb_theme->header_html($post_slug);
    $bb_theme->top_header();
    $bb_theme->main_header($post->post_name);
        	
    	if ( have_posts() ) {
    		while ( have_posts() ) {the_post(); ?>
                
                <?php $bb_theme->hero_content($post->ID); ?>    
                    
                    <section class="main-inner-content single">

                        <div class="row">
                            <div class="small-12 medium-8 medium-centered columns">
                                <?php the_content();?>
                                <p><a href="/bbworld/sessions">View More Sessions</a></p>
                            </div>
                        </div>

                    </section>

                <?php 

                $bb_theme->bottom_cta($post->ID); 

    		} 
    	} 

	$bb_theme->footer_html();

?>