Need to do a couple things to database on deploy

1. assign all current live sessions to 2016 year taxonomy for future growth. 
2. toggle off gforms for eloqua plugin because of js conflict
3. change "time" custom field in live sessions to time picker (new in acf update)
4. ensure "current live sessions" field is set to return post id and not post object
5. set the "live-streaming" page to be a child of bbworld live and set the tempalte to bbworld live home template
6. bump js and css versions again prior to deploying
7. move all 2016 session embeds to archive embed


Phase 3 changes
1. optionally have fallback images for sessions?
2. recording available soon for sessions that have happened (done)
3. comming soon for sessions that are in the future (done)
4. chat moderation within the chatroom - are moderators distinct? (done)
5. one room per track (highered ex) (done)
6. disable single user chat (done)
7. message when things are done for the day. "Thanks for watching the live stream today. check back tomorrow for more sessions."
8. get with kinsta on staging environment
9. s3 get that inplace - jon to provide creds
10. close captions


